<?php
namespace App\Services;
use App\TempModel;
use Carbon\Carbon;

class TempUility {
    public static function makeTempEntry($json,$minutesToExpire = 20){
        $tempModel = TempModel::create([
            'json' => $json,
            'expires_at' => Carbon::now()->addMinutes($minutesToExpire)
        ]);
        return $tempModel->id;
    }
    public static function gc(){
        TempModel::where('expires_at','<',Carbon::now()->toDateTimeString())->delete();
    }
}
