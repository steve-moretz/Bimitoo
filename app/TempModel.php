<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\TempModel
 *
 * @property int $id
 * @property mixed $json
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $expires_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TempModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TempModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TempModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TempModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TempModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TempModel whereJson($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TempModel whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TempModel whereExpiresAt($value)
 */
class TempModel extends Model
{
    protected $guarded = [];
    protected $casts = [
        'json'=>'array'
    ];
}
