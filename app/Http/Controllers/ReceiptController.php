<?php

namespace App\Http\Controllers;

use App\Receipt;
use Illuminate\Http\Request;

class ReceiptController extends Controller
{
    public function index($page,$number){
        $user_id = auth()->user()->id;
        return [
            'items'=>Receipt::skip($page * $number )->take($number)->where('user_id',$user_id )->get(),
            'count'=>Receipt::where('user_id',$user_id)->count()
        ];
    }

    public function indexAdmin($page,$number){
        return [
            'items'=>Receipt::skip($page * $number )->take($number)->get(),
            'count'=>Receipt::count()
        ];
    }
}
