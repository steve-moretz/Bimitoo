<?php

namespace App\Http\Controllers;

class UserRoutesController extends Controller
{

    public function about_us(){
        return view('about-us');
    }

    public function index(){
        return view('welcome');
    }

    public function contact_us(){
        return view('contact-us');
    }

    public function post(){
        return view('post');
    }

    public function post_text(){
        return view('post-text');
    }

    public function rules(){
        return view('rules');
    }

    public function save(){
        return view('welcome');
    }

    //-----------------------------------------------------------------------------------------------------------------------------

    public function third_party_insurance(){
        return view('third_party_insurance');
    }

    public function third_party_insurance_compare(){
        return view('third_party_insurance-compare');
    }

    public function auto_body_insurance(){
        return view('auto-body-insurance');
    }

    public function auto_body_insurance_compare(){
        return view('auto-body-insurance-compare');
    }

    public function collaboration(){
        return view('collaboration');
    }

    public function collaboration_compare(){
        return view('collaboration-compare');
    }

    public function erth_insurance(){
        return view('erth-insurance');
    }

    public function erth_insurance_compare(){
        return view('erth-insurance-compare');
    }

    public function fire_insurance(){
        return view('fire-insurance');
    }

    public function fire_insurance_compare(){
        return view('fire-insurance-compare');
    }

    public function life_insurance(){
        return view('life-insurance');
    }

    public function life_insurance_compare(){
        return view('life-insurance-compare');
    }

    public function motor_third_party_insurance(){
        return view('motor-third-party-insurance');
    }

    public function motor_third_party_insurance_compare(){
        return view('motor-third-party-insurance-compare');
    }

    public function other_insurance(){
        return view('other-insurance');
    }

    public function other_insurance_compare(){
        return view('other-insurance-compare');
    }

    public function supplementary_health_insurance(){
        return view('supplementary-health-insurance');
    }

    public function supplementary_health_insurance_compare(){
        return view('supplementary-health-insurance-compare');
    }

    public function travel_insurance(){
        return view('travel-insurance');
    }

    public function travel_insurance_compare(){
        return view('travel-insurance-compare');
    }
}
