<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Reminder
 *
 * @property int $id
 * @property string $set_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Reminder newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Reminder newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Reminder query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Reminder whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Reminder whereSetAt($value)
 * @mixin \Eloquent
 * @property int $user_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Reminder whereUserId($value)
 */
class Reminder extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
