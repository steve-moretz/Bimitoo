<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Receipt
 *
 * @property int $id
 * @property int $user_id
 * @property int $price
 * @property int $duration
 * @property mixed $data
 * @property \Illuminate\Support\Carbon $expires_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Receipt newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Receipt newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Receipt query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Receipt whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Receipt whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Receipt whereDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Receipt whereExpiresAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Receipt whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Receipt wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Receipt whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Receipt whereUserId($value)
 * @mixin \Eloquent
 */
class Receipt extends Model
{
    protected $guarded = [];

    protected $casts = [
        'expires_at' => 'datetime',
        'data'=>'array'
    ];

    public static function Make($userId,$price,$duration,$data){
        static::create([
            'user_id'=>$userId,
            'price'=>$price,
            'duration'=>$duration,
            'data'=>$data,
            'expires_at'=>Carbon::now()->addDays($duration)
        ]);
    }
}
