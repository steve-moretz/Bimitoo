run:
php artisan migrate:refresh

add:
ADMIN_EMAIL=admin@gmail.com
to .env

checkout my .env.example for results

admin user pass : {
    user:admin
    pass:12345678
}

Reminder:{
    method : GET,
    url : /reminder/set,
    {
        date
    }
    example : http://bimitoo.test/reminder/set?date=2020/3/12
}

Buy example :{
    1.goto : http://bimitoo.test/auto-body-insurance-compare?AutoBrands=1006&AutoModels=1098&AutoUsingTypes=3&AutoProductionYears=2017&AutoLife=10&AutoPrice=100000&AutoVaredat=true&AutoInsuranceStatuses=1&AutoNoDisYear=32
    2.at the bottom of the page click the send button to submit the visible form
    3.On the next page you see some information,press the Buy link
    4.On the next page you will get the information that you sent.
    
    The file is shoppingdone.blade.php
}

User Profile : {
    file : profile-user.blade.php
    url : /profile
    get receipts : {
        method : 'GET',
        url : /user/receipts/{page}/{number}
        (
            page : starts from 0,
            number : number of items use 5 for example
        )
          
        example : /receipts/0/5
    }
}

Admin Profile : {
    file : profile-admin.blade.php
    url : /admin/profile
    get receipts : {
            method : 'GET',
            url : /admin/receipts/{page}/{number}
            (
                page : starts from 0,
                number : number of items use 5 for example
            )
                          
            example : /receipts/0/5
        }
}
