<?php 

 include('layout-page/header.php');

?>

        <!-- this-filter session -->
    <div class="content">

  <section>
    <div class="search-section">
        <div class="aboutus">
            <span>بی می تو </span>
                 مقایسه، مشاوره و خرید آنلاین بیمه  
        </div>          
        <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-filter" >
                        <li class="nav-item lastitem">
                            <a class="nav-link " data-toggle="tab" href="#part4">
                                <i class="fas fa-user-shield icon fa-1x">
                                </i>
                                <span>  بیمه عمر  </span>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
   <div class="tab-pane active fade " id="part4">
        <form id="wizard2" class="wizard slider-form">          
            <div class="wizard-content">
                <div id="step1" class="wizard-step"> 
                    <div class="col-sm-10 col-xs-12  content-step">
                                                    <div class="col-md-4 col-sm-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> روز تولد  :  </label>
                                                             <select class="slider-input form-control " id="">                                       
                                                                    <option value=""> انتخاب  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-sm-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label">    ماه تولد  : </label>
                                                            <select class="slider-input form-control " id="">
                                                              
                                                                   <option value=""> انتخاب  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-sm-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> سال تولد  : </label>
                                                            <select class="slider-input form-control " id="">
                                                              
                                                                   <option value=""> انتخاب   </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                    </div>
                    <div class="col-sm-2 col-xs-12 btns-step next-par">
                        <button type="button" class="wizard-btn wizard-next btn press-btn">
                            <i class="fas fa-caret-left"></i>
                            <span> بعد  </span>
                        </button>
                    </div>   
                </div>
                <div id="step2" class="wizard-step">
                    <div class="col-sm-2 col-xs-12 btns-step prev-par">
                        <button type="button" class="wizard-btn wizard-prev btn press-btn">
                            <i class="fas fa-caret-right"></i>
                            <span> قبلی  </span>
                        </button>
                    </div>   
                    <div class="col-sm-8 col-xs-12 content-step">
                                                    <div class="col-xl-8 col-xl-offset-2 col-lg-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> مدت قرارداد  :  </label>
                                                             <select class="slider-input form-control " id="">                                       
                                                                    <option value=""> انتخاب  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                    </div>
                    <div class="col-sm-2 col-xs-12  btns-step next-par">
                        <button type="button" class="wizard-btn wizard-next btn press-btn">
                            <i class="fas fa-caret-left"></i>
                            <span> بعد  </span>
                        </button>
                    </div>   
                </div>
                <div id="step3" class="wizard-step">
                    <div class="col-sm-2 col-xs-12 btns-step prev-par">
                        <button type="button" class="wizard-btn wizard-prev btn press-btn">
                            <i class="fas fa-caret-right"></i>
                            <span> قبلی  </span>
                        </button>
                    </div>   
                    <div class="col-sm-8 col-xs-12 content-step">
                                                    <div class="col-xl-8 col-xl-offset-2 col-lg-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> توان پرداخت ماهانه : </label>
                                                            <input type="text" class="form-control slider-input" name="" placeholder="توان پرداخت ماهانه ">
                                                        </div>
                                                    </div>
                    </div>
                    <div class="col-sm-2 col-xs-12 btns-step next-par reg-par">
                        <button type="button" class="wizard-finish wizard-reg btn bimi-btns press-btn">
                            <span> مقایسه  </span>
                        </button>
                    </div>   
                </div>
            </div>
            <div class="wizard-header">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="wizard-step-indicator"><a href="#!">
                        <span class="badge slider-counter">1</span>
                    </a></li>
                    <li role="presentation" class="wizard-step-indicator"><a href="#!">
                        <span class="badge slider-counter">2</span>
                    </a></li>
                    <li role="presentation" class="wizard-step-indicator"><a href="#!">
                        <span class="badge slider-counter">3</span>
                    </a></li>
                </ul>
                <div class="slider-line"></div>
            </div> 
        </form>                                                                                            
    </div>    
                    </div>
   
    </div>
 </section>
</div>
    <!-- content session -->
    <section id="third-party-insurance-content">
        <div class="container">

            <!-- explanatin tab -->
            <div class="row">
                <div class="col-md-12">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs justify-content-center">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#explain">

                                معرفی بیمه عمر
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " data-toggle="tab" href="#question">
                                سوالات متداول
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#counciler">
                                درخواست مشاوره
                            </a>
                        </li>

                    </ul>
                    <!-- tab-content -->
                    <div class="tab-content shadow">
                        <!-- explanatin tab -->
                        <div class="tab-pane container active" id="explain">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="third-title">
                                            <h4> بیمه عمر چیست؟</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                بیمه عمر که بیمه آتیه ساز نیز نامیده می شود، یکی از انواع بیمه
                                                اشخاص می باشد؛ و از آن می توان به عنوان یک پس انداز
                                                طلایی برای آینده یادکرد.

                                                <br>

                                                این بیمه یک سرمایه گذاری با سود تضمینی است و هدف آن تامین آتیه و کاهش
                                                دغدغه و نگرانی افراد در طول زندگی می باشد.

                                                <br>

                                                به طور دقیق تر افراد با پرداخت مبلغ معینی به عنوان حق بیمه، علاوه
                                                بر سرمایه گذاری با سود تضمینی برای آتیه خود و
                                                استفاده از پوشش های متنوع و مفید در زمان حیات و در طول مدت اعتبار بیمه
                                                نامه،امکان
                                                استفاده از پوشش های مخصوص در
                                                مقابل خطر فوت و بعد از آن را نیز خواهند داشت.

                                                <br>

                                                بیمه عمر یک پشتوانه مالی بسیار محکم و قوی در دوران پیری، بازنشستگی و
                                                بعداز
                                                فوت برای بازماندگان محسوب می شود.

                                                <br>

                                                این بیمه دارای رشته های گوناگونی مانند بیمه های تمام
                                                عمر، بیمه عمر زمانی، بیمه های مختلط عمر و پس انداز، انواع
                                                طرح های آتیه برای کودکان، بیمه های مستمری، بیمه های
                                                بازنشستگی و بیمه های عمر گروهی می باشد.
                                            </p>
                                        </div>
                                        <div class="third-title">
                                            <h4>تعهد شرکت ‌های بیمه دربرابر بیمه‌گذاران بیمه عمر</h4>
                                        </div>
                                        <div class="thired-content">
                                            <p class="paragraph">
                                                تعهد شرکت های بیمه در این رشته از بیمه، پرداخت اندوخته به اضافه سود
                                                مشترک در منافع، به صورت یکجا و یا مستمری از محل
                                                اندوخته، در صورت حیات بیمه شده و در پایان مدت بیمه نامه، خواهدبود.

                                                <br>

                                                علاوه بر این بیمه گذار می تواند در طول مدت بیمه نامه از مزایا
                                                و پوشش هایی مانند دریافت وام، پرداخت هزینه های پزشکی
                                                و... که به طور دقیق تر به آن خواهیم پرداخت، بهره مند شود.

                                                <br>

                                                درصورت فوت نیز شرکت بیمه موردنظر موظف به پرداخت سرمایه بیمه عمر به
                                                اضافه
                                                اندوخته تشکیل شده تا زمان فوت ، به استفاده
                                                کننده قانونی و ذکر شده در فرم قرارداد می باشد.
                                            </p>
                                        </div>
                                        <div class="third-title">
                                            <h4> بیمه عمر سامان</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                شرکت بیمه سامان یکی از ارائه دهندگان بیمه عمر می باشد و طبق
                                                سالنامه آماری صنعت بیمه، این شرکت با صدور
                                                <b class="red">۷۷۱۵۷</b>
                                                بیمه نامه بیمه عمر در سال
                                                <b class="red">۹۶</b>
                                                رتبه بیست و یکم را در بین شرکت های دیگر کسب کرده است.

                                                <br>

                                                این شرکت علاوه بر ارائه پوشش های اصلی مانند پوشش های نقص عضو
                                                و ازکارافتادگی، بیماری های خاص و...به تازگی پوشش های
                                                تامین هزینه های بیماری های خاص و سرطان تا سقف
                                                <span class="red">۱.۵</span>
                                                میلیارد تومان، تامین هزینه های بیمارستانی تا سقف
                                                <b class="red">۲۰</b>
                                                میلیون تومان،
                                                جبران خسارت های وارده به ساختمان ها و تاسیسات در مقابل آتش سوزی،
                                                صاعقه، انفجار، سیل، سرقت و مسئولیت مالی تا سقف
                                                <b class="red">۲۰۰</b>
                                                میلیون تومان و پوشش جدید سرقت تلفن همراه، تبلت، لپ تاپ تا سقف
                                                <b class="20">۲۰</b>
                                                میلیون تومان به بیمه نامه خود افزوده است.
                                            </p>
                                        </div>
                                        <div class="third-title">
                                            <h4> بیمه عمر پاسارگاد</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                طبق سالنامه آماری منتشرشده توسط بیمه مرکزی، شرکت بیمه پاسارگاد با
                                                تولید
                                                <span class="red">۷۶۵۷۵۷</span>
                                                بیمه نامه بیمه عمر درسال
                                                <span class="red">۹۶</span>
                                                ، رتبه نوزدهم را در بین شرکت های دیگر داراست.

                                                <br>

                                                بیمه عمر پاسارگاد در دو رشته بیمه عمر و تأمین آتیه پاسارگاد و بیمه
                                                عمر
                                                پاسارگاد به شرط فوت ارائه می شود.

                                                <br>

                                                از جمله پوشش های ویژه ای که شرکت بیمه پاسارگاد به بیمه گذاران
                                                خود ارائه می دهد، پوشش آتش سوزی منزل مسکونی فرد
                                                بیمه گذار تا سقف
                                                <span class="red">۵۰۰</span>
                                                میلیون تومان سرمایه است. همچنین ضریب بیمه عمر این شرکت تا سقف
                                                <span class="red">۳۶۰</span>
                                                می باشد.
                                            </p>
                                        </div>
                                        <div class="third-title">
                                            <h4> بیمه عمر ایران</h4>
                                        </div>
                                        <div class="third-content">
                                            <div class="paragraph">
                                                شرکت بیمه ایران نیز با صدور
                                                <span class="red">۲۹۰۶۲۶</span>
                                                بیمه نامه بیمه عمر، رتبه اول را در بین تمامی شرکت های بیمه دارد.

                                                <br>

                                                بیمه عمر انفرادی و گروهی در شرکت بیمه ایران شامل طرح های زیر می
                                                باشد:

                                                <br>

                                                بیمه عمر مانده بدهکار انفرادی

                                                <br>

                                                بیمه عمر زمانی انفرادی

                                                <br>

                                                بیمه تمام عمر انفرادی

                                                <br>

                                                بیمه تأمین آتیه فرزندان

                                                <br>

                                                بیمه تامین هزینه جهیزیه

                                                <br>

                                                بیمه کارکنان دولت

                                                <br>

                                                بیمه جامع زندگی

                                                <br>

                                                جدیدترین و کاربردی ترین طرح بیمه عمر شرکت ایران طرح "بیمه مان" می
                                                باشد.

                                                <br>

                                                ویژگی هایی که باعث شده است این طرح بیمه عمر بسیار پرمخاطب بشود موار
                                                زیر
                                                می باشد:

                                                <ul>
                                                    <li>
                                                        این طرح عاری از محدودیت های سنی و سطح درآمدی شخص خریدار بیمه
                                                        عمر
                                                        می باشد.
                                                    </li>
                                                    <li>
                                                        این طرح ۸۵ درصد سود حاصل از سرمایه گذاری را علاوه بر سود علی
                                                        الحساب تضمینی، در اختیار شخص بیمه گذار قرار
                                                        می دهد.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="third-title">
                                            <h4> چرا به بیمه عمر و سرمایه گذاری نیاز داریم؟</h4>
                                        </div>
                                        <div class="third-content">
                                            <div class="paragraph">
                                                بیمه عمر درواقع راهکاری برای مقابله با این مشکلات مالی و اقتصادی است که
                                                ممکن
                                                است گریبان گیر هر فرد شود. افراد
                                                دوراندیش و آینده نگر با هر توان مالی، با پس انداز مبلغی به صورت
                                                ماهیانه، سه ماهه، شش ماهه و یا سالانه تحت عنوان
                                                حق بیمه، علاوه بر یک اندوخته مالی مطمئن برای آینده خود، از امکانات و
                                                پوشش های بیمه عمر نیز برخوردار خواهند شد.
                                            </div>
                                        </div>
                                        <div class="third-title">
                                            <h4> پوشش ها و شرایط بیمه عمر</h4>
                                        </div>
                                        <div class="third-content">

                                            <p class="paragraph">
                                                بیمه عمر و سرمایه گذاری دارای پوشش های متنوع و
                                                شرایط بسیاری است. در این مبحث به برخی از این پوشش ها و شرایط اشاره
                                                می کنیم:
                                            </p>

                                            <h6 class="sub-heading">دریافت وام</h6>
                                            <p class="paragraph">
                                                دریافت وام یکی از بهترین مزایا و تسهیلات بیمه عمر می باشد.

                                                <br>

                                                بیمه گذاران با پرداخت حق بیمه در طول مدت بیمه نامه،اندوخته ای
                                                برای خود شکل می دهند که به آن سود نیز تعلق می گیرد و
                                                می توانند بدون هیچ تشریفات اداری و نیاز به بازخرید بیمه نامه، تا
                                                قبل از پایان قرارداد از محل همین اندوخته وام دریافت
                                                کنند.
                                            </p>
                                            <br>

                                            <h6 class="sub-heading">شرایط و میزان پرداخت وام بیمه عمر</h6>
                                            <div class="paragraph">
                                                <ul>
                                                    <li>
                                                        می توان تا سقف ۹۰ درصد ارزش بازخریدی اندوخته بیمه نامه را،
                                                        به
                                                        صورت وام دریافت کرد.
                                                    </li>
                                                    <li>
                                                        نرخ تسهیلات وام ۴ درصد بیش از نرخ سود محاسباتی برای تشکیل
                                                        سرمایه
                                                        خواهد بود.
                                                    </li>
                                                    <li>
                                                        بیمه گذار دارای شرایط وام می تواند از اندوخته خود برداشت
                                                        کند و به صندوق خود بازگرداند اما به همان مقدار
                                                        برداشت از اندوخته او کسر می گردد.
                                                    </li>
                                                </ul>
                                            </div>
                                            <br>

                                            <h6 class="sub-heading">پوشش بیماری های خاص</h6>
                                            <p class="paragraph">
                                                انتخاب پوشش های مناسب برای بیمه عمر از اهمیت بسیاری برخوردار است؛
                                                در صورتی که بیمه شده در طول مدت بیمه نامه به
                                                هرگونه عارضه جسمی و یا اختلال در عملکرد طبیعی اعضا بدن که در اثر
                                                بروز
                                                سرطان،
                                                سکته قلبی، سکته مغزی، جراحی عروق قلبی
                                                (کرونر) و پیوند اعضای اصلی بدن (مطابق تشخیص پزشک معتمد شرکت بیمه)
                                                دچار
                                                شده
                                                باشد، تحت پوشش امراض خاص قرار می گیرد.

                                                <br>

                                                البته این پوشش مشروط بر ابتلای بیمه شده به این بیماری ها قبل از سن
                                                ۶۰ سالگی و بعد از عقد قرارداد بیمه عمر می باشد.
                                            </p>
                                            <br>

                                            <h6 class="sub-heading">پوشش نقص عضو و از کار افتادگی</h6>
                                            <p class="paragraph">
                                                در صورت بروز حادثه و ازکار افتادگی کامل و دائم، شخص بیمه شده از
                                                پرداخت
                                                حق بیمه معاف می شود و یا در صورت نقص عضو وی ،
                                                می تواند
                                                <span class="red">۵۰٪</span>
                                                سرمایه فوت خود را دریافت کند؛ تصور کنید سرمایه فوت فردی
                                                <span class="red">۳۰۰</span>
                                                میلیون تومان می باشد، در صورت نقص عضو،
                                                <span class="red">۱۵۰</span>
                                                میلیون تومان به ایشان پرداخت خواهدشد.

                                                <br>

                                                اگر حس امنیت و آرامش برای شما اهمیت دارد استفاده از پوشش نقص عضو و
                                                ازکارافتادگی بیمه عمر می تواند تا حد زیادی
                                                دغدغه های فکری شما را کم کند.
                                            </p>
                                            <br>

                                            <h6 class="sub-heading">پوشش های فوت طبیعی و فوت بر اثر حادثه</h6>
                                            <p class="paragraph">
                                                اشخاص با پرداخت اولین قسط از بیمه نامه خود مشمول این پوشش می شوند
                                                و در برابر هرگونه خطر احتمالی و خارج از کنترل،
                                                بیمه خواهندشد. افراد با خرید بیمه عمر و برخورداری از این پوشش، دیگر
                                                نگران
                                                آینده ی خانواده ی خود در صورت بروز حادثه و
                                                فوت، نخواهندبود.
                                            </p>
                                            <br>

                                            <h6 class="sub-heading">پوشش تکمیلی هزینه های پزشکی</h6>
                                            <p class="paragraph">
                                                تامین هزینه های پزشکی و درمانی مندرج شده در صورت حساب درمانی شخص
                                                بیمه شده، در صورتی که این پوشش در قرارداد وی ذکر
                                                شده باشد، توسط شرکت بیمه می باشد.

                                                <br>

                                                مطابق ماده ۱۳۶ قانون مالیات های مستقیم، سرمایه بیمه عمر و
                                                همچنین اندوخته حاصل از سرمایه گذاری های
                                                صورت گرفته،
                                                تماما از پرداخت مالیات معاف می باشند.
                                            </p>
                                            <br>

                                            <h6 class="sub-heading">مشخص کردن ذی نفع قرارداد بیمه</h6>
                                            <p class="paragraph">
                                                از دیگر مزایا و شرایط بیمه عمر می توان به این نکته اشاره کرد که،
                                                بیمه گذار حق دارد ذی نفع قرارداد را خود مشخص کند و
                                                در صورت تمایل هر سال آن را تغییر دهد.
                                            </p>
                                            <br>

                                            <h6 class="sub-heading">انعطاف پذیری</h6>
                                            <p class="paragraph">
                                                شخص می تواند شرایط بیمه نامه خود را از حیث نحوه ی پرداخت،
                                                میزان مبلغ پرداختی و پرداخت حق بیمه در بازه های زمانی
                                                دلخواه مانند هر ماه، هر سه ماه، هر شش ماه و سالی یکبار تعیین کند.
                                            </p>
                                            <br>

                                            <h6 class="sub-heading">ضمانت نامه</h6>
                                            <p class="paragraph">
                                                بیمه نامه عمر و سرمایه گذاری به تدریج و با تشکیل
                                                اندوخته سرمایه گذاری تبدیل به یک ضمانت نامه معتبر
                                                مانند اسناد
                                                مالی و اعتباری در معاملات خواهدشد.
                                            </p>
                                        </div>
                                        <div class="third-title">
                                            <h4> تفاوت های سرمایه گذاری در بانک و خرید بیمه عمر</h4>
                                        </div>
                                        <div class="third-content">
                                            <div class="paragraph">
                                                یکی از بهترین روش های سرمایه گذاری بلندمدت، خرید بیمه عمر می باشد.؛
                                                و در مقایسه با سرمایه گذاری در بانک دارای
                                                مزایای بسیاری است.

                                                <br>

                                                در این بخش قصد داریم تا تفاوت های خرید بیمه عمر و سپرده گذاری
                                                در بانک را به اختصار شرح دهیم:

                                                <ul>
                                                    <li>
                                                        اگر قصد شما سرمایه گذاری کوتاه مدت است، سپرده گذاری
                                                        در بانک ها به نظر جذاب تر می آید، اما باید در ابتدا
                                                        مبلغ
                                                        بسیاری را در بانک سپرده گذاری کنید تا سود قابل توجهی را
                                                        بدست بیاورید. ولی در مورد خرید بیمه عمر ، کافی ست
                                                        تنها یک حق بیمه را پرداخت کنید تا تحت پوشش بیمه ای نیز
                                                        قرار بگیرید؛ در صورتی که بانک فاقد این پوشش می باشد.
                                                    </li>
                                                    <li>
                                                        در حساب ها و سپرده گذاری های بلندمدت بانکی،
                                                        امکان برداشت پول قبل از اتمام قرارداد وجود ندارد، اما در
                                                        بیمه
                                                        عمر امکان برداشت اندوخته (وام بدون ضامن) طی مدت قرارداد وجود
                                                        دارد.
                                                    </li>
                                                    <li>
                                                        یکی از تفاوت های قابل توجهی که بین سپرده گذاری در
                                                        بانک و خرید بیمه عمر مطرح است، متغیر و ثابت بودن نرخ سود
                                                        پرداختی به اندوخته شما است. در مورد سپرده گذاری در بانک ها،
                                                        نرخ سود بانکی تحت شرایط متفاوت متغیر است، اما در
                                                        مورد بیمه عمر نرخ سود تا پایان، روندی مشخص و ثابت دارد و
                                                        تغییری
                                                        نمی
                                                        کند.
                                                    </li>
                                                    <li>
                                                        تفاوت عمده دیگر در حق برداشت طلبکاران است. در صورتی که پس
                                                        اندازکننده،
                                                        یک سپرده در بانک داشته باشد، طبق
                                                        قانون، در صورت از کار افتادگی او و بروز مشکلات دیگر،
                                                        طلبکاران می
                                                        توانند
                                                        از محل سپرده های بانکی، طلب خود را
                                                        وصول کنند. در بیمه عمر چنین برداشتی امکان پذیر نیست.
                                                    </li>
                                                    <li>
                                                        یک تفاوت مهم دیگر نیز وجود دارد، تا زمانی که روند پس انداز
                                                        در
                                                        بانک ها ادامه و یا سرمایه ها در اختیار آن ها
                                                        قرار دارد، به پس انداز کننده سود پرداخت می کنند و در
                                                        صورت از کارافتادگی کلی پس اندازکننده و کاهش درآمد او،
                                                        روند پس انداز کردن متوقف می شود؛ اما بیمه عمر از آنجایی که
                                                        علاوه بر پس انداز و سرمایه گذاری جنبه پوشش دهی
                                                        ریسک هم دارد در صورت از کارافتادگی دائم و کلی، بیمه شده از
                                                        پرداخت حق بیمه معاف می شود و شرکت بیمه ملزم به
                                                        اجرای تعهدات مندرج در بیمه نامه است.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="third-title">
                                            <h4> محاسبه بیمه عمر</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                نرخ بیمه عمر مقدار مشخصی ندارد و با توجه به عواملی از جمله سن،
                                                جنسیت،
                                                وضعیت
                                                جهانی، تاریخچه سلامت خانواده، شغل و
                                                حرفه، انتخاب پوشش های متفاوت و تعیین میزان حق بیمه پرداختی به
                                                دلخواه شخص بیمه گذار، قابل تعیین می باشد.
                                            </p>
                                        </div>
                                        <div class="third-title">
                                            <h4> فسخ قرارداد بیمه عمر</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                بیمه گر می تواند در هر زمان که بخواهد این بیمه نامه را فسخ
                                                کند و در تاریخ فسخ می تواند ارزش باز خریدی بیمه نامه خود
                                                را دریافت کند.
                                            </p>
                                        </div>
                                        <div class="third-title">
                                            <h4> هماهنگی بیمه عمر با تورم</h4>
                                        </div>
                                        <div class="third-content">
                                            <div class="paragraph">
                                                سپرده های بانکی با گذشت زمان و در اثر تورم ارزش کمتری پیدا
                                                می کنند، به دلیل همین ویژگی هماهنگی با تورم در
                                                بیمه نامه عمر ایجاد شده است که به شیوه های زیر ممکن می باشد:

                                                <ul>
                                                    <li>
                                                        اولین راهکار افزایش حق بیمه متناسب با درصد تورم بین
                                                        ۵ تا ۲۰ درصد هر سال نسبت به سال قبل و متناسب با افزایش
                                                        سرمایه فوت و اندوخته می باشد.
                                                    </li>
                                                    <li>
                                                        راهکار بعدی افزایش حق بیمه با درصدی بیشتر از درصد
                                                        افزایش سرمایه فوت است.
                                                    </li>
                                                    <li>
                                                        افزایش حق بیمه و ثابت نگه داشتن سرمایه فوت نیز
                                                        راهکار دیگری برای هماهنگی با تورم می باشد.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="third-title">
                                            <h4> بهترین بیمه عمر</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                در انتخاب بهترین بیمه عمر هدف بیمه گذار از خرید این بیمه بسیار مهم
                                                می
                                                باشد.افراد
                                                بیمه گذار می توانند هدف های متفاوتی
                                                ازجمله اهمیت پوشش های این بیمه به تنهایی، سرمایه گذاری بلندمدت از
                                                طریق خرید بیمه عمر، نحوه و پرداخت خسارت به موقع
                                                هر شرکت بیمه، میزان توانگری مالی شرکت های بیمه و ... داشته باشند و
                                                به
                                                طبع بهترین بیمه عمر با توجه به هدف هر فرد،
                                                متفاوت می باشد.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end explanatin tab -->

                        <!-- question tab-->
                        <div class="tab-pane container fade" id="question">
                            <div class="accordion" id="accordionExample">
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link" type="button" data-toggle="collapse"
                                                    data-target="#question1" aria-expanded="true"
                                                    aria-controls="question1">
                                                مناسب‌ترین مدل بیمه عمر چیست؟
                                            </button>
                                        </h2>
                                    </div>

                                    <div id="question1" class="collapse show" aria-labelledby="headingOne"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph"><p dir="RTL">عمر و سرمایه گذاری به شکل
                                                    مرکب،مناسب ترین گزینه برای کسانی است که با نگرش درست نگران آینده خود
                                                    و خانواده خود هستند. بطور کلی این بیمه نامه چهار محور اساسی زیر
                                                    را تحت پوشش قرار می دهد: </p>
                                                <ul>
                                                    <li> پس انداز</li>
                                                    <li> عمر و حوادث</li>
                                                    <li> از کار افتادگی</li>
                                                    <li>وام و مزایای خاص</li>
                                                </ul>
                                                <p></p></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingTwo">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question2" aria-expanded="false"
                                                    aria-controls="question2">
                                                مهمترین پوشش‌های اضافی بیمه عمر کدام است؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question2" class="collapse" aria-labelledby="headingTwo"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph"><p dir="RTL"><strong>پوشش بیمه فوت در اثر
                                                        حادثه</strong>:
                                                    در صورت فوت بیمه شده در اثر حادثه، سرمایه فوت بیمه عمر می تواند
                                                    تا سه برابر افزایش یابد. <br> <strong>پوشش از کار افتادگی</strong>:
                                                    این
                                                    پوشش به افرادی که شاغل هستند تعلق می گیرد. در صورت از کار
                                                    افتادگی
                                                    دائم و کامل بیمه شده (که حداقل تا یکسال ادامه داشته باشد) تا
                                                    پیش از
                                                    سن ۶۰ سالگی، شرکت بیمه، حق بیمه عمر را تا پایان مدت قید شده در
                                                    قرارداد بیمه پرداخت می نماید . <br> <strong>پوشش بیماری های
                                                        خاص </strong>:این طرح بیمه شده را در برابر هزینه های
                                                    بیماری های سکته قلبی، سکته مغزی، سرطان، پیوند اعضا اصلی بدن و
                                                    جراحی
                                                    قلب باز (کرونر قلبی) پوشش می دهد. </p></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingThree">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question3" aria-expanded="false"
                                                    aria-controls="question3">
                                                برای گرفت وام چه ضمانت‌هایی لازم است و مدت زمان وام و بازپرداخت آن چگونه
                                                است؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question3" class="collapse" aria-labelledby="headingThree"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph"><p dir="RTL"> شرایط دریافت وام در شرکت های بیمه
                                                    مختلف متفاوت است و در برخی از آنها برای دریافت وام باید حداقل سه ماه
                                                    از
                                                    مدت بیمه نامه گذشته و حداقل موجودی ۲۰۰ هزار تومان باشد که و
                                                    برخی از
                                                    شرکت ها این مدت حداقل ۲ سال است.<br> در این صورت بیمه گذار
                                                    بدون هیچ تشریفات اداری، فقط با ارائه بیمه نامه و کارت شناسایی
                                                    وام
                                                    را دریافت می نماید. باز پرداخت وام می تواند حداکثر تا ۳۶
                                                    ماه
                                                    باشد و همچنین می توان بعد از اتمام هر وام بلافاصله درخواست وام
                                                    جدید
                                                    نمود. </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingFour">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question4" aria-expanded="false"
                                                    aria-controls="question4">

                                                در صورت تاخیر در پرداخت اقساط بیمه عمر چگونه عمل می‌شود؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question4" class="collapse" aria-labelledby="headingFour"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph"><p dir="RTL"> چنانچه بیمه گذار بیش از یک ماه از
                                                    پرداخت حق بیمه خودداری نماید از پایان یک ماه دیگر تحت پوشش بیمه
                                                    نمی باشد.<br>
                                                    چنانچه خودداری از پرداخت حق بیمه قبل از شش ماه باشد، بدون
                                                    استرداد
                                                    هیچ مبلغی و با اخطار کتبی بیمه نامه باطل خواهد شد و اگر بعد از
                                                    شش
                                                    ماه و پرداخت شش قسط چنین اتفاقی بیافتد در صورت فوت ارزش باز خریدی و
                                                    یا
                                                    ارزش سرمایه مخفف (هر کدام بیشتر باشد) به بازماندگان پرداخت خواهد شد
                                                    و در
                                                    صورت حیات، ارزش باز خریدی بیمه نامه پرداخت خواهد شد.<br> اما
                                                    اگر
                                                    بیمه گذار قبل از اتمام آن سال اقدام به پرداخت اقساط معوق و
                                                    هزینه
                                                    دیر کرد نماید بلافاصله و بدون هیچ تشریفاتی شرایط بیمه نامه به
                                                    شرایط
                                                    قبلی باز خواهد گشت و درصورتی که این اتفاق بعد از یکسال بیافتد ادامه
                                                    بیمه نامه
                                                    منوط به آزمایشات پزشکی و تایید پزشک خواهد شد . </p></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingFive">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question5" aria-expanded="false"
                                                    aria-controls="question5">
                                                اقساط بیمه عمر چگونه پرداخت می‌شود؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question5" class="collapse" aria-labelledby="headingFive"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph"><p dir="RTL"> تعیین نحوه پرداخت در شرکت های
                                                    مختلف
                                                    متفاوت است. قبلا دفترچه ای برای پرداخت به بیمه گذار داده
                                                    میشد
                                                    تا در موعد مقرر به پرداخت اقساط اقدام نماید. اما در حال حاضر برگه ای
                                                    حاوی تاریخ سررسید پرداخت اقساط و شناسه پرداخت به بیمه گذار
                                                    تحویل
                                                    داده می شود. در روش های مدرن تر پرداخت اقساط از طریق
                                                    پرداخت اینترنتی و یادآوری پرداخت اقساط در سایت بی می توـ صورت
                                                    میگیرد. </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingSix">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question6" aria-expanded="false"
                                                    aria-controls="question6">

                                                آیا می‌توان شرایط بیمه‌نامه را تغییر داد؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question6" class="collapse" aria-labelledby="headingSix"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                بله؛ در طول مدت قرارداد بیمه عمر و در سررسید هر یک از سنوات بیمه ای،
                                                می‌توان
                                                شرایط بیمه‌نامه را تغییر داد.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingSeven">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question7" aria-expanded="false"
                                                    aria-controls="question7">

                                                آیا بیمه‌های عمر معاف از مالیات است؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question7" class="collapse" aria-labelledby="headingSeven"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                بله؛ مطابق ماده ۱۳۶ قانون مالیات‌های مستقیم، سرمایه بیمه عمر و نیز
                                                اندوخته
                                                حاصل از ‌سرمایه‌گذاری‌های صورت گرفته، به کل از پرداخت مالیات معاف هستند.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingEight">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question8" aria-expanded="false"
                                                    aria-controls="question8">

                                                در صورت انحلال و ورشکستگی شرکت بیمه، سرمایه مشتری چه می‌شود؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question8" class="collapse" aria-labelledby="headingEight"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph"><p dir="RTL"><strong>طبق ماده ۶۰ </strong>از قانون
                                                    تاسیس
                                                    بیمه مرکزی ایران، مصوبه سال ۱۳۵۰، اموال مؤسسات بیمه همچنین ودایع
                                                    مذکور
                                                    در مواد ۳۶ و ۴۶ تضمین حقوق و مطالبات بیمه گذاران، بیمه
                                                    شدگان و
                                                    صاحبان حقوق آنان است و در صورت انحلال یا ورشکستگى مؤسسه بیمه بیمه گذاران
                                                    و بیمه شدگان و صاحبان حقوق آنان نسبت به سایر بستانکاران حق تقدم
                                                    دارند. در میان رشته هاى مختلف بیمه حق تقدم با بیمه عمر است.<br>
                                                    مؤسسات بیمه نمی توانند بدون موافقت قبلى بیمه مرکزى ایران اموال
                                                    خود
                                                    را صلح حقوق نمایند و یا به رهن واگذار کنند و یا موضوع هر نوع معامله
                                                    با
                                                    حق استرداد قرار دهند.<br>دفاتر اسناد رسمى موظفند هنگام انجام این
                                                    قبیل
                                                    معاملات موافقتنامه بیمه مرکزى ایران را مطالبه و مفاد آن  را در
                                                    سند
                                                    منعکس کنند.<br> <strong>طبق ماده ۳۶ </strong>مؤسسات بیمه ایرانى با
                                                    سرمایه حداقل یک صد میلیون ریال تشکیل مى شود که باید ۵۰ درصد
                                                    آن
                                                    به صورت نقدی پرداخت شده باشد میزان ودیعه اى که عندالاقتضا براى
                                                    هر یک
                                                    از رشته هاى بیمه در نظر گرفته خواهد شد در آیین نامه اى
                                                    که از
                                                    طرف بیمه مرکزى ایران تهیه و به تصویب شوراى عالى بیمه می رسد
                                                    تعیین
                                                    خواهد شد.<br> <strong>طبق ماده ۴۶ </strong>مؤسسات بیمه خارجى باید
                                                    طبق
                                                    آیین نامه اى که به پیشنهاد بیمه مرکزى ایران به تصویب شوراى
                                                    عالى
                                                    بیمه مى رسد مبلغى براى هر یک از دو رشته بیمه هاى زندگى و
                                                    سایر
                                                    انواع بیمه نزد بیمه مرکزى ایران تودیع  نمایند. مبلغ  این
                                                    ودیعه
                                                    در هریک از دو موردمذکور از پانصد هزار دلار یا معادل آن از ارزهاى
                                                    مورد
                                                    قبول بانک مرکزى ایران  کمترنخواهد بود.<br> هر یک از
                                                    مؤسسات
                                                    بیمه خارجى باید درآمدهاى خود را سال  به  سال به ودیعه مزبور
                                                    اضافه کند تا در هر مورد مبلغ  ودیعه حداقل به دو برابر مبلغ مصوب
                                                    شوراى عالى بیمه برسد. <br> افزایش ودیعه مازاد بر مبالغ فوق اختیارى
                                                    است.
                                                </p></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingNine">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question9" aria-expanded="false"
                                                    aria-controls="question9">

                                                موارد استثنا در بیمه‌نامه عمر چیست؟
                                            </button>

                                        </h2>
                                    </div>
                                    <div id="question9" class="collapse" aria-labelledby="headingNine"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph"><p dir="RTL"></p>
                                                <ul>
                                                    <li>خطر جنگ و یا هر نوع حادثه دیگری که جنبه نظامی داشتته باشد .</li>
                                                    <li>مسابقات سرعت و اکتشاف و پروازهای اکروباتی.</li>
                                                    <li>خودکشی قبل سه سال و ۳۶ قسط کامل مگر اینکه ثابت شود خودکشی غیر
                                                        ارادی
                                                        بوده.
                                                    </li>
                                                    <li>در صورتی که استفاده کننده یا بیمه گذار مسبب مرگ
                                                        بیمه شده
                                                        باشند.
                                                    </li>
                                                </ul>
                                                <p></p></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading11">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question11" aria-expanded="false"
                                                    aria-controls="question11">
                                                مزایای بیمه عمر نسبت به نظام بانکی چیست؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question11" class="collapse" aria-labelledby="heading11"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph"><p dir="RTL"> در نظام بانکی با تغییر سیاست های
                                                    بانک
                                                    مرکزی سود بانکی تغییر می کند، اما در بیمه عمر و سرمایه گذاری
                                                    سود متعلقه تحت هیچ شرایطی کاهش نمی یابد و همچنین در صورت تشکیل
                                                    منافع افزایش می یابد .<br> بیمه عمر و سرمایه گذاری، پس
                                                    اندازی
                                                    منظم و البته اجباری ست که پس انداز بانکی چنین خاصیتی ندارد
                                                    .<br> در
                                                    یافت وام از طریق بیمه عمر در عرض بیست دقیقه و بدون هیچ تشریفاتی صورت
                                                    می گیرد،
                                                    و بیمه نامه تنها ضامن بازپرداخت وام است. اما در نظام بانکی برای
                                                    دریافت وام تشریفات و بروکراسی اداری بسیاری را باید طی کرد و چندین
                                                    ضامن
                                                    با شرایط مختلف نیاز دارد. </p></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading12">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question12" aria-expanded="false"
                                                    aria-controls="question12">
                                                شرایط وام بیمه عمر چیست؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question12" class="collapse" aria-labelledby="heading12"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph"><p dir="RTL"> در بیمه های عمر بیمه گذار می تواند
                                                    طبق شرایط زیر وام دریافت نماید: </p>
                                                <ul>
                                                    <li>وام تا سقف ۹۰ درصد ارزش بازخریدی بیمه نامه عمر در پایان ماه
                                                        ششم
                                                        به بیمه گذار پرداخت می شود.
                                                    </li>
                                                    <li>باز پرداخت وام دریافت شده حداکثر طی ۳۶ قسط و سه سال است.</li>
                                                    <li>سود تعلق گرفته به وام بیمه عمر حداکثر ۴ درصد بیش از سود تضمینی
                                                        است
                                                        که به اندوخته این بیمه نامه تعلق می گیرد.
                                                    </li>
                                                </ul>
                                                <p></p></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading13">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question13" aria-expanded="false"
                                                    aria-controls="question13">
                                                در صورت دریافت وام آیا پوشش‌های بیمه ای تحت تاثیر قرار می‌گیرد؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question13" class="collapse" aria-labelledby="heading13"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                خیر ، استفاده از وام بیمه عمر تاثیری بر پوشش‌های این بیمه‌نامه نمی‌گذارد
                                                و
                                                پوشش‌ها در طی مدت بازپرداخت اقساط وام ادامه خواهد داشت.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading14">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question14" aria-expanded="false"
                                                    aria-controls="question14">
                                                آیا برای دریافت وام نیاز به ضامن، چک و یا سند است؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question14" class="collapse" aria-labelledby="heading14"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                خیر، از آنجایی که پرداخت وام از اعتبار صندوق بیمه‌نامه خود بیمه‌گذار
                                                تهیه
                                                می‌شود، نیاز به هیچ تضمینی ندارد و بدون هیچگونه تشریفاتی طی چند روزه
                                                کاری به
                                                حساب بیمه‌گذار واریز می‌شود. </P>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading15">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question15" aria-expanded="false"
                                                    aria-controls="question15">
                                                آیا اشخاص بیمار هم می‌توانند تحت پوشش بیمه عمر قرار بگیرند؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question15" class="collapse" aria-labelledby="heading15"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                <div class="paragraph">
                                            <p dir="RTL"> در مورد برخی از بیماری ها با نظر پزشک و دریافت حق بیمه
                                                اضافی قابل بیمه شدن است، اما بیماری های لاعلاج قابل بیمه شدن
                                                نیستند.<br>در عین حال برای اینکه بیمه گر و بیمه گذار با مشکل
                                                مواجه
                                                نشوند بهتر است پرسش نامه با دقت مطالعه و تکمیل گردد، و یا از
                                                مشاوران
                                                بی می توـ در زمینه بیمه عمر کمک بگیرید. </p></div>
                                        </p>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading16">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question16" aria-expanded="false"
                                                    aria-controls="question16">
                                                شرایط بازنشستگی بیمه عمر چه تفاوتی با حقوق بازنشستگی تأمین اجتماعی دارد؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question16" class="collapse" aria-labelledby="heading16"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph"><p dir="RTL"> در مدت ۲۰ سال حقوق دریافتی از بیمه عمر
                                                    بیش
                                                    از حقوق دریافتی با سابقه ۳۰ ساله تأمین اجتماعی است.<br>جمع حق بیمه
                                                    پرداختی ۲۰ ساله بیمه عمر بسیار کمتر از ۳۰ سال تأمین اجتماعی است.<br>نیاز
                                                    به اشتغال در محل یا تایید بازرس تامین اجتماعی نیست، و برای خانم های
                                                    خانه دار یا کودکان و نوزادان بسیار مناسب است. </p></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading17">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question17" aria-expanded="false"
                                                    aria-controls="question17">
                                                چه مدارکی برای پوشش از کار افتادگی لازم است؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question17" class="collapse" aria-labelledby="heading17"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph">
                                                <ul>
                                                    <li>مدارک تاییدیه پزشکی قانونی و مراجع ذیصلاح</li>
                                                    <li>نظریه و مدارک مورد درخواست پزشک معتمد بیمه گر (این مدارک در
                                                        شرکت ها مختلف و بسته به نوع آسیب متفاوت است)
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading18">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question18" aria-expanded="false"
                                                    aria-controls="question18">
                                                مدارک لازم برای دریافت سرمایه را نام ببرید؟ چیست؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question18" class="collapse" aria-labelledby="heading18"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph">
                                                <ul>
                                                    <li>بیمه نامه و ضمایم آن</li>
                                                    <li>قبض رسید حق بیمه های پرداختی</li>
                                                    <li>کارت شناسایی و کپی آن</li>
                                                </ul>
                                                برای پرداخت سرمایه فوت، استفاده کننده یا بیمه گذار در اولین فرصت و
                                                حداکثر تا یک ماه، به صورت کتبی دلایل فوت را به اطلاع بیمه گر برساند
                                                و
                                                علاوه بر مدارک ذکر شده مدارک زیر را برای رسیدگی در اختیار بیمه گر
                                                قرار
                                                دهد:<br>
                                                <ul>
                                                    <li>گواهی رسمی فوت.</li>
                                                    <li>گواهی مشروح آخرین پزشک معالج که در آن علت بیماری شرح داده شده
                                                        باشد
                                                    </li>
                                                    <li>در صورت فوت بر اثر حادثه، گزارش حادثه که توسط مراجع ذیصلاح
                                                        تهیه شده باشد.
                                                    </li>
                                                    <li>در صورتی که استفاده کنندگان مشخص نشده باشند گواهی
                                                        انحصار
                                                        وراثت.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading19">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question19" aria-expanded="false"
                                                    aria-controls="question19">
                                                آیا بیمه عمر از نظر شرعی اشکالی ندارد؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question19" class="collapse" aria-labelledby="heading19"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph">یکی از ابهاماتی که معمولا در مورد بیمه های عمر و
                                                اندوخته ساز مورد سوال قرار می گیرد، مباحث شرعی آن است در این بخش
                                                پاسخ
                                                حضرت آیت الله خامنه ای را در مورد حکم بیمه عمر را
                                                می بینیم.<br>
                                                سؤال: بیمه عمر چه حکمى دارد؟<br> پاسخ: شرعاً مانعى ندارد.<br> منبع: سایت
                                                حضرت آیت الله خامنه ای
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading20">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question20" aria-expanded="false"
                                                    aria-controls="question20">
                                                پرداخت سرمایه و یا مستمری به چه شکل صورت می‌گیرد؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question20" class="collapse" aria-labelledby="heading20"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph">پرداخت سرمایه در شرکت های بیمه مختلف متفاوت است،
                                                اما
                                                به صورت کلی، پرداخت سرمایه بیمه نامه عمر و سرمایه گذاری
                                                به
                                                انقضای بیمه نامه (فوت بیمه شده و یا پایان مدت بیمه نامه)
                                                بستگی دارد به دو صورت زیر صورت می گیرد:<br> ۱. فوت بیمه شده طی
                                                مدت
                                                بیمه نامه.<br> ۲. حیات بیمه شده در انتهای مدت بیمه نامه.
                                                <br>اندوخته
                                                ناشی از حق بیمه پرداختی در انقضای بیمه نامه به یکی از حالت های
                                                زیر قابل پرداخت است:<br>
                                                <ul>
                                                    <li>به صورت یکجا</li>
                                                    <li>به صورت پرداخت مستمری تضمینی با مدت معین</li>
                                                    <li>به صورت پرداخت مستمری مادام العمر (به شرط حیات)</li>
                                                    <li>به صورت پرداخت مستمری مادام العمر با دوره تضمین</li>
                                                    <li>به صورت پرداخت مستمری به شرط حیات با مدت معین</li>
                                                    <li>به صورت پرداخت مستمری به شرط حیات با مدت معین و دوره تضمین</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading21">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question21" aria-expanded="false"
                                                    aria-controls="question21">
                                                ذی‌نفع در بیمه عمر کیست؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question21" class="collapse" aria-labelledby="heading21"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                شخص یا اشخاص حقیقی و حقوقی و دارای رابطه منطقی با بیمه‌شده هستند که نام
                                                آن‌ها در بیمه‌نامه ذکر شده و به عبارتی منافع مادی حاصل از بیمه‌نامه برای
                                                آنان پیش بینی شده است.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading22">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question22" aria-expanded="false"
                                                    aria-controls="question22">
                                                سرمایه فوت بیمه عمر چیست؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question22" class="collapse" aria-labelledby="heading22"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                عبارت است از مبلغ توافق شده ای که بیمه‌گر مکلف است در صورت فوت بیمه‌شده
                                                در
                                                هر سال بیمه‌ای، مطابق جدول پیش بینی تعهدات بیمه‌گر و بیمه‌گذار به
                                                ذی‌نفعان
                                                بیمه‌نامه پرداخت نماید.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading23">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question23" aria-expanded="false"
                                                    aria-controls="question23">

                                                ارزش بازخریدی بیمه عمر چیست؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question23" class="collapse" aria-labelledby="heading23"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                در صورتی که بیمه‌گذار قبل از پایان مدت قرارداد تمایلی به ادامه بیمه‌نامه
                                                نداشته باشد، بیمه‌گر مکلف است ارزش بازخرید بیمه‌نامه را طبق شرایط
                                                بیمه‌نامه
                                                و ضمائم آن محاسبه و پرداخت نماید. </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading24">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question24" aria-expanded="false"
                                                    aria-controls="question24">
                                                بیمه عمر معلق چیست؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question24" class="collapse" aria-labelledby="heading24"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                به بیمه‌نامه‌ای گفته می‌شود که حق‌بیمه آن در سررسید تعیین شده پرداخت
                                                نشده و
                                                مبلغ اندوخته ‌سرمایه‌گذاری بیمه‌نامه صفر باشد.
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading25">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question25" aria-expanded="false"
                                                    aria-controls="question25">
                                                غرامت فوت به چه کسی پرداخت می‌شود؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question25" class="collapse" aria-labelledby="heading25"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph"><p dir="RTL"> بیمه گذار موظف است درهنگام تکمیل
                                                    فرم
                                                    پیشنهاد، ذینفع/ذینفعان را در دو حالت فوت و حیات بیمه شده تعیین
                                                    نماید؛ در صورت مشخص نبودن استفاده کنندگان سرمایه بیمه نامه به
                                                    نسبت
                                                    مساوی به وراث قانونی بیمه شده پرداخت خواهد شد.<br>توجه داشته
                                                    باشید،
                                                    در صورت حیات بیمه شده صرفا یک نفر (بیمه گذار یا بیمه شده)
                                                    به
                                                    عنوان ذی نفع تعیین می گردد و در صورت فوت می توان حداکثر ۶
                                                    نفر
                                                    با سهم ها و اولویت های متفاوت به عنوان ذی نفع تعیین
                                                    نمود.
                                                </p></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading26">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question26" aria-expanded="false"
                                                    aria-controls="question26">
                                                وراث از نظر قانون به چند دسته تقسیم می‌شوند؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question26" class="collapse" aria-labelledby="heading26"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph"><p dir="RTL"> وراث از نظر قانون به سه طبقه تقسیم می
                                                    شوند:<br>
                                                </p>
                                                <ul>
                                                    <li>طبقه اول که عبارتند از: پدر، مادر، زن، شوهر، اولاد و اولاد اولاد
                                                    </li>
                                                    <li>طبقه دوم که عبارتند از: اجداد، برادر، خواهر و اولاد آن ها
                                                    </li>
                                                    <li>طبقه سوم که عبارتند از: عمو، عمه، دائی، خاله و اولاد
                                                        آن ها
                                                    </li>
                                                </ul>
                                                <p></p></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading27">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question27" aria-expanded="false"
                                                    aria-controls="question27">
                                                پوشش معافیت از پرداخت حق‌بیمه در صورت از کارافتادگی به چه شخصی تعلق
                                                می‌گیرد؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question27" class="collapse" aria-labelledby="heading27"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                این پوشش تنها به یک نفر، بیمه‌گذار (در صورت ازکارافتادگی ناشی از حادثه)
                                                یا
                                                بیمه‌شده (در صورت ازکارافتادگی ناشی از حادثه یا بیماری) تعلق می‌گیرد،
                                                البته
                                                این پوشش شرط سنی دارد و در شرکت‌های بیمه مختلف متفاوت است.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading28">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question28" aria-expanded="false"
                                                    aria-controls="question28">
                                                آیا یک شخص می‌تواند چند بیمه عمر از شرکت‌های مختلف بیمه‌ای خریداری کند؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question28" class="collapse" aria-labelledby="heading28"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                بله؛ خرید چندین بیمه عمر از شرکت‌های مختلف بیمه، مشروط بر اینکه مجموع
                                                سرمایه‌های خریداری شده از ۵.۰۰۰.۰۰۰.۰۰۰ ریال تجاوز نکند امکان پذیر است.
                                                بنابرین بیمه‌شده باید اطلاعات بیمه‌نامه‌های قبلی خریداری شده را در
                                                اختیار
                                                شرکت بیمه جدید قرار دهد.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading29">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question29" aria-expanded="false"
                                                    aria-controls="question29">
                                                در صورت ارائه اطلاعات نادرست در پرسش‌نامه چه اتفاقی رخ خواهد داد؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question29" class="collapse" aria-labelledby="heading29"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph"><p dir="RTL"></p>
                                                <ul>
                                                    <li>در صورتی که اطلاعات نادرست به صورت عمدی توسط بیمه گذار یا
                                                        بیمه شده
                                                        درفرم پیشنهاد ارائهشود، بیمه نامه باطل خواهد شد و حق بیمه
                                                        پرداختی قابل استرداد نخواهد بود.
                                                    </li>
                                                    <li>در صورتی که این اظهارات از روی سهو و ناآگاهی باشد، اندوخته بیمه
                                                        نامه
                                                        پرداخت و در صورت بروز خسارت بیمه گر می تواند از پرداخت
                                                        تمام و یا بخشی از تعهدات خود، خودداری نماید.
                                                    </li>
                                                </ul>
                                                <p></p></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading30">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question30" aria-expanded="false"
                                                    aria-controls="question30">
                                                در بیمه عمر راه‌های مقابله با تورم و کاهش ارزش پول و سرمایه در آینده
                                                چیست؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question30" class="collapse" aria-labelledby="heading30"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph"><p dir="RTL"></p>
                                                <ul>
                                                    <li>به منظور کاهش اثرات تورم در بیمه نامه های عمر و سرمایه گذاری
                                                        توصیه می شود برای حق بیمه پرداختی و سرمایه فوت، ضریب
                                                        تعدیل
                                                        مناسب و مطابق با شرایط مالی بیمه گذار تعیین گردد.
                                                    </li>
                                                    <li>ضریب تعدیل حق بیمه می تواند ۰، ۵، ۱۰، ۱۵، ۲۰ درصد
                                                        انتخاب
                                                        شود.
                                                    </li>
                                                    <li>ضریب تعدیل سرمایه فوت می تواند ۰، ۵، ۱۰، ۱۵، ۲۰ درصد انتخاب
                                                        شود.
                                                    </li>
                                                    <li>برای اثرگذاری هر چه بهتر، لازم است ضریبتعدیل حق بیمه همواره
                                                        بزرگتر یا مساوی ضریبتعدیل سرمایه فوت انتخاب شود.
                                                    </li>
                                                    <li>توجه داشته باشید این ضرایب نیز در شرکت های بیمه مختلف
                                                        متفاوت
                                                        است.
                                                    </li>
                                                </ul>
                                                <p></p></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading31">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question31" aria-expanded="false"
                                                    aria-controls="question31">
                                                آیا بیمه عمر علاوه بر سرمایه‌گذاری بلندمدت، مزایای دیگری هم دارد؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question31" class="collapse" aria-labelledby="heading31"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                بیمه عمر شامل پوشش‌هایی از جمله: پرداخت وام بدون ضامن، پوشش خطر فوت در
                                                اثر
                                                حادثه، پوشش خطر طبیعی، پوشش بیماری های خاص (شامل: سکته قلبی، سکته مغزی،
                                                انواع سرطان ها، پیوند اعضا و جراحی قلب و عروق)، پوشش نقص عضو، پوشش
                                                ازکارافتادگی و پرداخت هزینه های پزشکی می‌باشد.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading32">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question32" aria-expanded="false"
                                                    aria-controls="question32">


                                                پس از پایان مدت بیمه‌نامه عمر، بازنشستگی به چه صورت می‌باشد؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question32" class="collapse" aria-labelledby="heading32"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                پس از پایانپس از پایان طول مدت بیمه‌نامه، بیمه‌گذار می‌تواند کل پرداختی
                                                خود
                                                را به صورت یکجا و یا به صورت مستمری دریافت کند. طول مدت بیمه‌نامه،
                                                بیمه‌گذار
                                                می‌تواند کل پرداختی خود را به صورت یکجا و یا به صورت مستمری دریافت کند.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading33">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question33" aria-expanded="false"
                                                    aria-controls="question33">
                                                در صورت ورشکستگی شرکت بیمه، پرداخت اندوخته بیمه‌گذار و عمل به تعهدات
                                                شرکت بیمه بر عهده‌ی چه کسی خواهدبود؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question33" class="collapse" aria-labelledby="heading33"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                تمام شرکت‌های بیمه توسط بیمه مرکزی ضمانت می‌شوند و طبق ماده ۳۲ بیمه
                                                مرکزی درصورت ورشكستگي بيمه‌گر، بيمه‌گذاران نسبت به ساير طلبكاران حق تقدم
                                                دارند و بين معاملات مختلف بيمه در درجه اول، حق تقدم با معاملات بيمه عمر
                                                است.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading34">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question34" aria-expanded="false"
                                                    aria-controls="question34">
                                                دوره‌های پرداختی حق‌بیمه عمر چگونه است؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question34" class="collapse" aria-labelledby="heading34"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                حق‌بیمه عمر می‌تواند در دوره‌های ماهانه، سه ماهه، شش ماهه و یا حتی
                                                سالانه پرداخت شود.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading35">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question35" aria-expanded="false"
                                                    aria-controls="question35">
                                                بهترین بیمه عمر از نظر مردم کدام است؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question35" class="collapse" aria-labelledby="heading35"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                برای افرادی ممکن است بیمه عمر با پوشش‌های بیشتر، بهترین بیمه عمر باشد و
                                                برای برخی دیگر انتخاب کمترین میزان پوشش و زیاد کردن میزان اندوخته اهمیت
                                                بیشتری دارد، پس انتخاب بهترین بیمه عمر به هدف هر فرد بستگی دارد. </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading36">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question36" aria-expanded="false"
                                                    aria-controls="question36">
                                                آیا میتوانیم تعداد سال های سرمایه‌گذاری در بیمه عمر را تغییر دهیم؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question36" class="collapse" aria-labelledby="heading36"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                یکی از ویژگی های بیمه عمر انعطاف‌پذیری آن است و بیمه‌گذار می‌تواند میزان
                                                حق‌بیمه، بازه ی زمانی پرداخت حق‌بیمه و تعداد سال‌های بیمه‌نامه خود را
                                                تغییر دهد. </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading37">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question37" aria-expanded="false"
                                                    aria-controls="question37">
                                                سرمایه‌گذاری در بیمه عمر بهتر است یا در بانک؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question37" class="collapse" aria-labelledby="heading37"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph"><p dir="RTL">براساس معیارهای زیر، سرمایه گذاری
                                                    در خرید بیمه عمر برای افرادی که هدف آن ها از خرید بیمه عمر صرفا
                                                    سرمایه گذاری است نسبت به سرمایه گذاری در بانک، بهتر
                                                    است:</p>
                                                <ul>
                                                    <li dir="RTL">بدون نیاز به سپرده گذاری مبلغ اولیه و تنها با
                                                        پرداخت یک قسط از بیمه، وارد روند سرمایه گذاری می شوند.
                                                    </li>
                                                    <li dir="RTL">روند نرخ سود در سرمایه گذاری بیمه عمر بر خلاف
                                                        بانک ها ثابت می باشد.
                                                    </li>
                                                    <li dir="RTL">برخلاف سرمایه گذاری در بانک، طبق قوانین بیمه در
                                                        صورت بروز هرگونه مشکل برای دارنده بیمه عمر، طلبکارانِ وی اجازه ی
                                                        تصرف بر اندوخته ی او را ندارند.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading38">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question38" aria-expanded="false"
                                                    aria-controls="question38">
                                                مقدار حق‌بیمه عمر چقدر می‌باشد؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question38" class="collapse" aria-labelledby="heading38"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                میزان حق‌بیمه قابل پرداخت در هر شرکت بیمه متفاوت می‌باشد و با توجه به
                                                میزان توان مالی افراد می‌تواند متفاوت باشد.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading39">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question39" aria-expanded="false"
                                                    aria-controls="question39">
                                                آیا می‌توان خدمات ارائه‌دهنده این رشته از بیمه را در بین چند شرکت مقایسه
                                                کرد؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question39" class="collapse" aria-labelledby="heading39"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                <div class="paragraph">
                                            <p dir="RTL">سایت بی می تو این امکان را برای شما ایجاد کرده است.</p>
                                            <p dir="RTL">با مراجعه به سایت بی می تو، فعالیتِ چند شرکت بیمه را در این
                                                رشته از
                                                بیمه می توانید مقایسه کنید و از خدمات مشاوره ای کارشناسان بی می تو
                                                برای انتخاب بهتر کمک بگیرید.</p></div>
                                        </p>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading40">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                    data-target="#question40" aria-expanded="false"
                                                    aria-controls="question40">
                                                سود بیمه عمر چقد می‌باشد؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question40" class="collapse" aria-labelledby="heading40"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph"><p dir="RTL">سود بیمه عمر به دوصورت تضمینی و مشارکتی
                                                    محاسبه می شود. میزان سود تضمینی در سال اول ۱۶ درصد و براي دو
                                                    سال بعد ۱۳ درصد و براي مدت اضافه بر چهار سال اول آن ۱۰ درصد می
                                                    باشد.</p>
                                                <p dir="RTL">سود مشارکتی نیز متغیر است.</p></div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                    </div>
                </div>
                <!-- end question tab-->
                <!-- counciler tab-->
                <div class="tab-pane container fade" id="counciler">

                    <section class="form-elegant">

                        <div class="third-title">
                            <h4>درخواست مشاوره</h4>
                        </div>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">


                                <form>
                                    <div class="form-group col-md-8">
                                        <label for="nameInput">نام و نام خانوادگی</label>
                                        <input type="text" class="form-control none-border" id="nameInput"
                                               placeholder="">
                                    </div>

                                    <div class="form-group col-md-8">
                                        <label for="phoneInput">شماره تماس </label>
                                        <input type="text" class="form-control none-border" id="phoneInput"
                                               placeholder="">
                                    </div>
                                    <div class="form-group col-md-8">
                                        <label for="description">متن درخواست مشاوره</label>
                                        <textarea id="description" class="form-control none-border"
                                                  style="float: right;">

                                            </textarea>
                                    </div>

                                    <div class="col-md-8  btn ">
                                        <div class="compare-btn btn ">
                                            <a href="#">ارسال درخواست</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </section>

                </div>
                <!--  end counciler tab -->
            </div>
            <!--  end tab-content  -->
        </div>
        </div>
        </div>
        </div>

    </section>
    <!-- end  content session -->

<?php

 include('layout-page/footer.php');

 ?>

<script type="text/javascript">

        $(".top-nav-menu .nav-item").removeClass("active");
        $(".top-nav-menu #m4").addClass("active");

</script>

</body>

</html>
