/* ------------------------------------------------------ in first  -----------------------------------------------------------------*/

//to save send object data
var SendObj;

/* get from api and prev page and check are empty or not*/
GetInfoFromApiPrev();

/* ------------------------------------------------------ in changes  -----------------------------------------------------------------*

/* check empty */
$("#wizard-compare8 .compare-input").change(function(){

    let el=$(this);

    let NameInObj=el.attr("NameInObj");
    let Newval=el.val();

    checkEmpty(el);

    let NewCount=CountEmptyBox();

    SendObj[NameInObj]=Newval;
    PostData(SendObj,NewCount);

});

/* get brand of selected device */
$("#Device-select").change(function () {

    let SelDev=$(this).val();

    if (SelDev !="00"){
        GetDevBrand(SelDev);
    }

});

/* get model of selected brand device */
$("#DeviceBrand-select").change(function () {

    let DevBrand=$(this).val();

    if (DevBrand !="00"){
        GetDevModel(DevBrand);
    }

});

/* -------------------------------------------------------------- my functions -----------------------------------------*/

/* get information from api for insurance form */
function  GetInfoFromApiPrev() {

    /* get list of selects box from api */

    const data = new FormData();
    data.append('_token', '{{csrf_token()}}');


    let Devices=[];
    let DevicesFran=[];

    (async () =>{

        try {
            const response = await fn1('api/insurance/insurance/electronic-equipment/basic-data', 'GET', data, {});
            const res = await response.json();

            Devices=res.Devices;
            DevicesFran=res.Franchises;

        }
        catch (e) {
            console.log(e);
        }


        /* get selected option from prev page */
        let searchParams = new URLSearchParams(window.location.search);
        let SDevice= searchParams.get('Device');
        let SDeviceBrand= searchParams.get('DeviceBrand');
        let SDeviceModel= searchParams.get('DeviceModel');
        let SDeviceFran= searchParams.get('DeviceFran');
        let SDevicePrice= searchParams.get('DevicePrice');


        /* put select box lists and selected option-check empty or not - check count az empty or not */
        let element;
        element=$("#Device-select");
        AddInput(element,Devices);
        AddSelcted(element,SDevice);
        checkEmpty(element);
        if (SDevice !="00"){
            GetDevBrand(SDevice);
        }
        element=$("#DeviceBrand-select");
        AddSelcted(element,SDeviceBrand);
        checkEmpty(element);
        if (SDeviceBrand !="00"){
            GetDevModel(SDeviceBrand);
        }
        element=$("#DeviceModel-select");
        AddSelcted(element,SDeviceModel);
        checkEmpty(element);
        element=$("#DeviceFran-select");
        AddInput(element,DevicesFran);
        AddSelcted(element,SDeviceFran);
        checkEmpty(element);
        element=$("#DevicePrice-input");
        AddSelcted(element,SDevicePrice);
        checkEmpty(element);


        /* obj to post */

        let DeviceId=parseInt(SDevice);
        let BrandId=parseInt(SDeviceBrand);
        let ModelId=parseInt(SDeviceModel);
        let FranchiseId=parseInt(SDeviceFran);
        let Price=parseInt(SDevicePrice);


        SendObj={ DeviceId:DeviceId,BrandId:BrandId,ModelId:ModelId,FranchiseId:FranchiseId,Price:Price,
            _token : '{{csrf_token()}}'
        };


        /* count neccessary empty fileds */
        let emptyCount=CountEmptyBox();

        PostData(SendObj,emptyCount);


    })();

}
/* put arrays of api in select inputs */
function AddInput(el,arr) {

    let child="";

    for (let i=0;i<arr.length;i++) {

        child=child+"<option value="+arr[i].Id +">"+arr[i].Title +"</option>";

    }

    el.children().remove();
    let FirstOp="<option value='00'> انتخاب کنید  </option>";
    el.append(FirstOp);
    el.append(child);
}
/* put selected to selected option in element */
function AddSelcted(el,sel) {

    el.find("option[value="+sel+"]").attr("selected","selected");
    el.val(sel);
}
/* check empty fields */
function checkEmpty(el){

    let sv=el.val();

    //no select-empty
    if (sv == "00" ) {
        //show error
        el.parent().addClass("empty-box");
    }
    // select-full
    else {
        //hide error
        el.parent().removeClass("empty-box");
    }

}
/* count empty box */
function CountEmptyBox() {

    let i=0;
    $("#wizard-compare8").find(".empty-box.req").each(function () {
        i=i+1;
    });
    return i;
}
/* post data to api */
function PostData(obj,emptyCount){

    //is empty
    if (emptyCount > 0 ) {


        $(".result-list").children().remove();

        let str="<div  class='result-row empty-result'> <span class='fa fa-exclamation-triangle empty-alert'> </span><span> توجه! برای نمایش بسته های پیشنهادی بیمه ، اطلاعات مورد نیاز را وارد نمایید</span></div>";

        setTimeout(function () {
            $(".result-list").append(str);
        },200);
    }
    //is full
    else {

        (async () =>{

            try {
                const response = await fn1('api/insurance/insurance/electronic-equipment/inquiry', 'POST', obj, {});
                const res = await response.json();

                var msg=res.Message;
                var ResArr=res.Inquiries;
                var CompArr=res.Companies;
                var DurArr=res.Durations;

                $(".result-list").children().remove();

                //has error message
                if (msg !=null) {

                    let str="<div  class='result-row empty-result'> <span class='fa fa-exclamation-triangle empty-alert'> </span><span>"+ msg +"</span></div>";
                    setTimeout(function () {
                        $(".result-list").append(str);
                    },200);
                }

                //no error message
                else{

                    if (ResArr.length==0) {

                        let str="<div  class='result-row empty-result'> <span class='fa fa-exclamation-triangle empty-alert'> </span><span> هیچ نتیجه ای برای این بیمه وجود ندارد</span></div>";
                        setTimeout(function () {
                            $(".result-list").append(str);
                        },200);
                    }
                    else {
                        loading(2000);

                        for (let i=0;i<ResArr.length;i++) {

                            var UniqueId=ResArr[i].UniqueId;

                            var TarhTitle=ResArr[i].Title;
                            var TarhSubTitle=ResArr[i].SubTitle;

                            var FirstAmount=ResArr[i].CashPrice.FirstAmount;
                            var FinalAmount=ResArr[i].CashPrice.FinalAmount;

                            var dis=false;

                            if (FirstAmount != FinalAmount ) {
                                dis=true;
                            }

                            var HasInstallments=ResArr[i].HasInstallments;


                            var CompanyId=ResArr[i].CompanyId;
                            var DurationId=ResArr[i].DurationId;

                            for (let j=0;j<CompArr.length;j++){
                                if ( CompArr[j].Id==CompanyId )  {
                                    var CompTitlt=CompArr[j].Title;
                                    var CompLogo=CompArr[j].LogoURL;
                                    var CompStar=CompArr[j].FinancialStrength;
                                    var CompBranch=CompArr[j].CompensationBranchCount;
                                }
                            }

                            for (let j=0;j<DurArr.length;j++){
                                if ( DurArr[j].Id==DurationId )  {
                                    var DurTitlt=DurArr[j].Title;
                                }
                            }


                            let  str="<div class='result-row' id="+UniqueId+">";

                            str=str+"<div class='result-row-box'>";


                            str=str+ "<div class='company-logo'><img src="+CompLogo+" alt="+CompTitlt+"></div>";

                            str=str+ "<div class='company-desc'><div class='desc-col'><span> سطح توانگری مالی </span>";
                            for (let k=0;k<CompStar;k++){
                                str=str+" <span class='fa fa-star star'></span>";
                            }
                            str=str+ "</div><div class='desc-col'><span class='branchCount'>"+CompBranch+"</span><span> شعبه پرداخت خسارت </span></div></div>";

                            str=str+ "<div class='company-dur'>"+DurTitlt+"</div>" ;

                            str=str+ "<div class='company-money'>";
                            if (dis) {
                                str=str+"<div class='company-col discount'>"+FirstAmount+"</div>" ;
                            }
                            str=str+"<div class='company-col'>"+FinalAmount+"</div>" ;
                            str=str+ "<p class='rial'> ریال </p></div>";

                            str=str+ "<div class='pay-box'> <button class='btn'> خرید نقدی </button>" ;
                            if (HasInstallments !=null) {
                                str=str+ "<button class='btn aghsat'> اقساطی </button>" ;
                            }
                            str=str+ "</div>";

                            str=str+ "</div>";

                            str=str+ "</div>";

                            setTimeout(function () {
                                $(".result-list").append(str);
                            },200);

                        }
                    }
                }

            }
            catch (e) {
                console.log(e);
            }

        })();
    }

}
/* for loading*/
function loading(time) {

    $(".loading-box").removeClass("dn");
    $(".loading-box .loading-sq").addClass("rotate-class");

    setTimeout(function () {
        $(".loading-box").addClass("dn");
        $(".loading-box .loading-sq").removeClass("rotate-class");
    },time);

}
/* get brand of selected device */
function GetDevBrand(DevId) {

    const data = new FormData();
    data.append('_token', '{{csrf_token()}}');

    let address='api/insurance/insurance/electronic-equipment/brand/'+DevId;

    let DevBrands=[];

    (async () =>{

        try {
            const response = await fn1(address, 'GET', data, {});
            const res = await response.json();

            DevBrands=res.Brands;
        }
        catch (e) {
            console.log(e);
        }

        let element;
        element=$("#DeviceBrand-select");
        AddInput(element,DevBrands);

    })();

}
/* get model of selected brand device */
function GetDevModel(DevBraId) {

    const data = new FormData();
    data.append('_token', '{{csrf_token()}}');

    let address='api/insurance/insurance/electronic-equipment/model/'+DevBraId;

    let DevModels=[];

    (async () =>{

        try {
            const response = await fn1(address, 'GET', data, {});
            const res = await response.json();

            DevModels=res.Models;
        }
        catch (e) {
            console.log(e);
        }

        let element;
        element=$("#DeviceModel-select");
        AddInput(element,DevModels);

    })();

}

