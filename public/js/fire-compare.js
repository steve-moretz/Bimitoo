/* ------------------------------------------------------ in first  -----------------------------------------------------------------*/

//to save send object data
var SendObj;

/* get from api and prev page and check are empty or not*/
GetInfoFromApiPrev();

/* ------------------------------------------------------ in changes  -----------------------------------------------------------------*/

/* check empty */
$("#wizard-compare7 .compare-input").change(function(){

    let el=$(this);

    let NameInObj=el.attr("NameInObj");
    let Newval=el.val();

    checkEmpty(el);

    let NewCount=CountEmptyBox();

    SendObj[NameInObj]=Newval;
    PostData(SendObj,NewCount);

});

/* check estate is apartemant or no */
$("#EstateTypes-select").change(function () {

    let v=$(this).val();

    //is apartemant
    if (v=="3") {

        $(".unit-condition").removeClass("dn");
        $(".unit-condition .form-group").addClass("req");

    }
    else {

        $(".unit-condition").addClass("dn");
        $(".unit-condition .form-group").removeClass("req");
    }

});

/* -------------------------------------------------------------- my functions -----------------------------------------*/

/* get information from api for insurance form */
function  GetInfoFromApiPrev() {

    /* get list of selects box from api */

    const data = new FormData();
    data.append('_token', '{{csrf_token()}}');

    let EstateTypes=[];
    let StructureTypes=[];
    let AreaUnitPrices=[];
    let Coverages=[];

    (async () =>{

        try {
            const response = await fn1('api/insurance/insurance/fire/basic-data', 'GET', data, {});
            const res = await response.json();
            EstateTypes=res.EstateTypes;
            StructureTypes=res.StructureTypes;
            AreaUnitPrices=res.AreaUnitPrices;
            Coverages=res.Coverages;
        }
        catch (e) {
            console.log(e);
        }

        /* get selected option from prev page */
        let searchParams = new URLSearchParams(window.location.search);
        let SEstateTypes= searchParams.get('EstateTypes');
        let SStructureTypes= searchParams.get('StructureTypes');
        let SUnit= searchParams.get('Unit');
        let SAreaUnitPrices= searchParams.get('AreaUnitPrices');
        let SMeter= searchParams.get('Meter');
        let SCost= searchParams.get('Cost');


        /* put select box lists and selected option-check empty or not - check count az empty or not */
        let element;
        element=$("#EstateTypes-select");
        AddInput(element,EstateTypes);
        AddSelcted(element,SEstateTypes);
        checkEmpty(element);

        let v=SEstateTypes;

        if (v=="3") {

            $(".unit-condition").removeClass("dn");
            $(".unit-condition .form-group").addClass("req");

        }
        else {

           $(".unit-condition").addClass("dn");
            $(".unit-condition .form-group").removeClass("req");
        }

        element=$("#StructureTypes-select");
        AddInput(element,StructureTypes);
        AddSelcted(element,SStructureTypes);
        checkEmpty(element);
        element=$("#Unit-input");
        AddSelcted(element,SUnit);
        checkEmpty(element);
        element=$("#AreaUnitPrices-select");
        AddInput(element,AreaUnitPrices);
        AddSelcted(element,SAreaUnitPrices);
        checkEmpty(element);
        element=$("#Meter-input");
        AddSelcted(element,SMeter);
        checkEmpty(element);
        element=$("#HomeCost-input");
        AddSelcted(element,SCost);
        checkEmpty(element);
        element=$("#Coverages-box");
        AddCheckbox(element,Coverages);
        checkEmpty(element);


        /* obj to post */

        let EstateTypeId=parseInt(SEstateTypes);
        let ApartmentUnitCount=parseInt(SUnit);
        let StructureTypeId=parseInt(SStructureTypes);
        let TotalArea=parseInt(SMeter);
        let AppliancesValue=parseInt(SCost);
        let AreaUnitPriceId=parseInt(SAreaUnitPrices);
        let CoverageIds=[];


        SendObj={ EstateTypeId:EstateTypeId,ApartmentUnitCount:ApartmentUnitCount,StructureTypeId:StructureTypeId,TotalArea:TotalArea,AppliancesValue:AppliancesValue,
            AreaUnitPriceId:AreaUnitPriceId ,CoverageIds:CoverageIds ,

            _token : '{{csrf_token()}}'
        };


        /* count neccessary empty fileds */
        let emptyCount=CountEmptyBox();

        PostData(SendObj,emptyCount);


    })();

}
/* put arrays of api in select inputs */
function AddInput(el,arr) {

    let child="";

    for (let i=0;i<arr.length;i++) {

        child=child+"<option value="+arr[i].Id +">"+arr[i].Title +"</option>";

    }

    el.children().remove();
    let FirstOp="<option value='00'> انتخاب کنید  </option>";
    el.append(FirstOp);
    el.append(child);
}
/* put arrays of checkbox in select inputs */
function AddCheckbox(el,arr) {

    let child="";

    for (let i=0;i<arr.length;i++) {

        child=child+"<div class='ExtraCheck-row'><input type='checkbox' class='compare-input' onchange='ToggleCheck(this)' value='00' id="+arr[i].Id +">" +
            "<span class='compare-label'>"+arr[i].Title +"</span></div>";
    }

    el.children().remove();
    el.append(child);
}
/* put selected to selected option in element */
function AddSelcted(el,sel) {

    el.find("option[value="+sel+"]").attr("selected","selected");
    el.val(sel);
}
/* check empty fields */
function checkEmpty(el){

    let sv=el.val();

    //no select-empty
    if (sv == "00" ) {
        //show error
        el.parent().addClass("empty-box");
    }
    // select-full
    else {
        //hide error
        el.parent().removeClass("empty-box");
    }

}
/* count empty box */
function CountEmptyBox() {

    let i=0;
    $("#wizard-compare7").find(".empty-box.req").each(function () {
        i=i+1;
    });
    return i;
}
/* post data to api */
function PostData(obj,emptyCount){

    //is empty
    if (emptyCount > 0 ) {


        $(".result-list").children().remove();

        let str="<div  class='result-row empty-result'> <span class='fa fa-exclamation-triangle empty-alert'> </span><span> توجه! برای نمایش بسته های پیشنهادی بیمه ، اطلاعات مورد نیاز را وارد نمایید</span></div>";

        setTimeout(function () {
            $(".result-list").append(str);
        },200);
    }
    //is full
    else {

        (async () =>{

            try {
                const response = await fn1('api/insurance/insurance/fire/inquiry', 'POST', obj, {});
                const res = await response.json();


                var msg=res.Message;
                var ResArr=res.Inquiries;
                var CompArr=res.Companies;
                var DurArr=res.Durations;

                $(".result-list").children().remove();

                //has error message
                if (msg !=null) {

                    let str="<div  class='result-row empty-result'> <span class='fa fa-exclamation-triangle empty-alert'> </span><span>"+ msg +"</span></div>";
                    setTimeout(function () {
                        $(".result-list").append(str);
                    },200);
                }

                //no error message
                else {

                    if (ResArr.length==0) {

                        let str="<div  class='result-row empty-result'> <span class='fa fa-exclamation-triangle empty-alert'> </span><span> هیچ نتیجه ای برای این بیمه وجود ندارد</span></div>";
                        setTimeout(function () {
                            $(".result-list").append(str);
                        },200);
                    }
                    else {
                        loading(2000);
                        for (let i=0;i<ResArr.length;i++) {

                            var UniqueId=ResArr[i].UniqueId;

                            var TarhTitle=ResArr[i].Title;
                            var TarhSubTitle=ResArr[i].SubTitle;

                            var FirstAmount=ResArr[i].CashPrice.FirstAmount;
                            var FinalAmount=ResArr[i].CashPrice.FinalAmount;

                            var dis=false;

                            if (FirstAmount != FinalAmount ) {
                                dis=true;
                            }

                            var HasInstallments=ResArr[i].HasInstallments;

                            var CompanyId=ResArr[i].CompanyId;
                            var DurationId=ResArr[i].DurationId;

                            for (let j=0;j<CompArr.length;j++){
                                if ( CompArr[j].Id==CompanyId )  {
                                    var CompTitlt=CompArr[j].Title;
                                    var CompLogo=CompArr[j].LogoURL;
                                    var CompStar=CompArr[j].FinancialStrength;
                                    var CompBranch=CompArr[j].CompensationBranchCount;
                                }
                            }

                            for (let j=0;j<DurArr.length;j++){
                                if ( DurArr[j].Id==DurationId )  {
                                    var DurTitlt=DurArr[j].Title;
                                }
                            }

                            let  str="<div class='result-row' id="+UniqueId+">";

                            str=str+"<div class='result-row-box'>";

                            if (TarhTitle !=null) {
                                str=str+ "<div class='tarh-box'><span>"+TarhTitle+"</span>";
                            }
                            if (TarhSubTitle !=null) {
                                str=str+ "<span class='trah-sub'>"+TarhSubTitle+"</span>";
                            }
                            if (TarhTitle !=null) {
                                str=str+"</div>";
                            }

                            str=str+ "<div class='company-logo'><img src="+CompLogo+" alt="+CompTitlt+"></div>";

                            str=str+ "<div class='company-desc'><div class='desc-col'><span> سطح توانگری مالی </span>";
                            for (let k=0;k<CompStar;k++){
                                str=str+" <span class='fa fa-star star'></span>";
                            }
                            str=str+ "</div><div class='desc-col'><span class='branchCount'>"+CompBranch+"</span><span> شعبه پرداخت خسارت </span></div></div>";

                            str=str+ "<div class='company-dur'>"+DurTitlt+"</div>" ;

                            str=str+ "<div class='company-money'>";
                            if (dis) {
                                str=str+"<div class='company-col discount'>"+FirstAmount+"</div>" ;
                            }
                            str=str+"<div class='company-col'>"+FinalAmount+"</div>" ;
                            str=str+ "<p class='rial'> ریال </p></div>";

                            str=str+ "<div class='pay-box'> <button class='btn'> خرید نقدی </button>" ;
                            if (HasInstallments !=null) {
                                str=str+ "<button class='btn aghsat'> اقساطی </button>" ;
                            }
                            str=str+ "</div>";

                            str=str+ "</div>";

                            str=str+ "</div>";

                            setTimeout(function () {
                                $(".result-list").append(str);
                            },200);

                        }
                    }
                }


            }
            catch (e) {
                console.log(e);
            }

        })();
    }

}
/* for loading*/
function loading(time) {

    $(".loading-box").removeClass("dn");
    $(".loading-box .loading-sq").addClass("rotate-class");

    setTimeout(function () {
        $(".loading-box").addClass("dn");
        $(".loading-box .loading-sq").removeClass("rotate-class");
    },time);

}
/* for update cover checkbox*/
function ToggleCheck(th) {

    let vl=th.value;
    if (vl=="00") { th.value="1"; }
    else          { th.value="00"; }

    let par=$(th).parents(".ExtraCheck-box");

    PostNewCoverList(par);
}
/* for Post New CoverList*/
function PostNewCoverList(pr) {

    let temp=[];

    pr.find(".ExtraCheck-row .compare-input").each(function () {

        let cv=$(this).attr("value");
        let ci=$(this).attr("id");

        if (cv=="1")  { temp.push(parseInt(ci));}
    });

    let NameInObj=pr.attr("NameInObj");

    SendObj[NameInObj]=temp;
    let NewCount=CountEmptyBox();
    PostData(SendObj,NewCount);
}
