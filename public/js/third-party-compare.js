/* ------------------------------------------------------ in first  -----------------------------------------------------------------*/

//to save send object data
var SendObj;

/* get from api and prev page and check are empty or not*/
GetInfoFromApiPrev();

/* ------------------------------------------------------ in changes  -----------------------------------------------------------------*/

/* check empty */
$("#wizard-compare1 .compare-input").change(function(){

    let el=$(this);

    let myid=el.attr("id");

    if ( ( myid !="InsuranceStatuses-select")&&( myid !="Lossstatus-select") ){

        let Newval=el.val();

        let NameInObj=el.attr("NameInObj");

        let ty=el.attr("type");
        if ( ty=="date" ) { Newval=el.attr("value"); }

        checkEmpty(el);

        let NewCount=CountEmptyBox();

        SendObj[NameInObj]=Newval;
        PostData(SendObj,NewCount);
    }

});

/* check active or not in steps of form */
$("#InsuranceStatuses-select").change(function () {

    let v=$(this).val();
    //has prev insurance
    if (v=="2") {

        //show or hide conditional filed
        $(".conditional-box").removeClass("dn");
        $(".conditional-number").addClass("dn");

        //change req fileds
        $(".conditional-number").removeClass("req");
        $(".conditional-box > .form-group").addClass("req");


    }
    //dont not has prev insurance
    else {

        //show or hide conditional filed
        $(".conditional-box").addClass("dn");
        $(".conditional-number").removeClass("dn");

        //change req fileds
        $(".conditional-number").addClass("req");
        $(".conditional-box > .form-group").removeClass("req");

    }

    let el=$(this);
    checkEmpty(el);

    let NewCount=CountEmptyBox();

    let NameInObj=el.attr("NameInObj");

    SendObj[NameInObj]=v;
    PostData(SendObj,NewCount);


});
$("#Lossstatus-select").change(function () {

    let v=$(this).val();
    if (v=="0") {

        $(".conditional-loss").addClass("dn");
        $(".conditional-loss > .form-group").removeClass("req");
    }
    else {

        $(".conditional-loss").removeClass("dn");
        $(".conditional-loss > .form-group").addClass("req");

    }

    let el=$(this);
    checkEmpty(el);

    let NewCount=CountEmptyBox();

    let NameInObj=el.attr("NameInObj");

    SendObj[NameInObj]=v;
    PostData(SendObj,NewCount);

});

$("#Number-date").change(function () {

    let nv=$(this).val();

    let t=nv.length;
    if  (t==0) {nv=" ";}

    $(this).attr("value",nv);

});
$("#End-date").change(function () {

    let nv=$(this).val();

    let t=nv.length;
    if  (t==0) {nv=" ";}

    $(this).attr("value",nv);

});

/* get model */
$("#Brands-select").change(function () {

    let CarBrand=$(this).val();

    if (CarBrand !="00"){
        GetCarModel(CarBrand);
    }

});

/* -------------------------------------------------------------- my functions -----------------------------------------*/

/* get information from api for insurance form */
function  GetInfoFromApiPrev() {

    /* get list of selects box from api */

    const data = new FormData();
    data.append('_token', '{{csrf_token()}}');

    let Brands=[];
    let UsingTypes=[];
    let ProductionYears=[];
    let InsuranceStatuses=[];
    let Companies=[];
    let Durations=[];
    let DriverDiscounts=[];
    let ThirdPartyDiscounts=[];
    let LifeLosses=[];
    let PropertyLosses=[];
    let DriverLosses=[];
    let CoverageTypes=[];

    (async () =>{

        try {
            const response = await fn1('api/insurance/insurance/third-party/basic-data', 'GET', data, {});
            const res = await response.json();
            Brands=res.Brands;
            UsingTypes=res.UsingTypes;
            ProductionYears=res.ProductionYears;
            InsuranceStatuses=res.InsuranceStatuses;
            Companies=res.Companies;
            Durations=res.Durations;
            DriverDiscounts=res.DriverDiscounts;
            ThirdPartyDiscounts=res.ThirdPartyDiscounts;
            LifeLosses=res.LifeLosses;
            PropertyLosses=res.PropertyLosses;
            DriverLosses=res.DriverLosses;
            CoverageTypes=res.CoverageTypes;

        }
        catch (e) {
            console.log(e);
        }

        /* get selected option from prev page */
        let searchParams = new URLSearchParams(window.location.search);
        let SBrands= searchParams.get('Brands');
        let SModels= searchParams.get('Models');
        let SUsingTypes= searchParams.get('UsingTypes');
        let SProductionYears= searchParams.get('ProductionYears');
        let SInsuranceStatuses= searchParams.get('InsuranceStatuses');
        let SNumberdate= searchParams.get('Numberdate');
        let SCompanies= searchParams.get('Companies');
        let SDurations= searchParams.get('Durations');
        let SEnddate= searchParams.get('Enddate');
        let SDriverDiscounts= searchParams.get('DriverDiscounts');
        let SThirdPartyDiscounts= searchParams.get('ThirdPartyDiscounts');
        let SLossstatus= searchParams.get('Lossstatus');
        let SLifeLosses= searchParams.get('LifeLosses');
        let SPropertyLosses= searchParams.get('PropertyLosses');
        let SDriverLosses= searchParams.get('DriverLosses');

        /* put select box lists and selected option-check empty or not - check count az empty or not */
        let element;
        element=$("#Brands-select");
        AddInput(element,Brands);
        AddSelcted(element,SBrands);
        checkEmpty(element);
        if (SBrands !="00"){
            GetCarModel(SBrands);
        }
        element=$("#Model-select");
        AddSelcted(element,SModels);
        checkEmpty(element);
        element=$("#UsingTypes-select");
        AddInput(element,UsingTypes);
        AddSelcted(element,SUsingTypes);
        checkEmpty(element);
        element=$("#ProductionYears-select");
        AddInput(element,ProductionYears);
        AddSelcted(element,SProductionYears);
        checkEmpty(element);
        element=$("#InsuranceStatuses-select");
        AddInput(element,InsuranceStatuses);
        AddSelcted(element,SInsuranceStatuses);
        checkEmpty(element);

        let v=SInsuranceStatuses;
        //has prev insurance
        if (v=="2") {

            //show or hide conditional filed
            $(".conditional-box").removeClass("dn");
            $(".conditional-number").addClass("dn");

            //change req fileds
            $(".conditional-number").removeClass("req");
            $(".conditional-box > .form-group").addClass("req");


        }
        //dont not has prev insurance
        else {

            //show or hide conditional filed
            $(".conditional-box").addClass("dn");
            $(".conditional-number").removeClass("dn");

            //change req fileds
            $(".conditional-number").addClass("req");
            $(".conditional-box > .form-group").removeClass("req");

        }

        element=$("#Number-date");
        AddValue(element,SNumberdate);
        checkEmpty(element);
        element=$("#Companies-select");
        AddInput(element,Companies);
        AddSelcted(element,SCompanies);
        checkEmpty(element);
        element=$("#Durations-select");
        AddInput(element,Durations);
        AddSelcted(element,SDurations);
        checkEmpty(element);
        element=$("#End-date");
        AddValue(element,SEnddate);
        checkEmpty(element);
        element=$("#DriverDiscounts-select");
        AddInput(element,DriverDiscounts);
        AddSelcted(element,SDriverDiscounts);
        checkEmpty(element);
        element=$("#ThirdPartyDiscounts-select");
        AddInput(element,ThirdPartyDiscounts);
        AddSelcted(element,SThirdPartyDiscounts);
        checkEmpty(element);
        element=$("#Lossstatus-select");
        AddSelcted(element,SLossstatus);
        checkEmpty(element);

        let vv=SLossstatus;
        if (vv=="0") {

            $(".conditional-loss").addClass("dn");
            $(".conditional-loss > .form-group").removeClass("req");
        }
        else {

            $(".conditional-loss").removeClass("dn");
            $(".conditional-loss > .form-group").addClass("req");

        }

        element=$("#LifeLosses-select");
        AddInput(element,LifeLosses);
        AddSelcted(element,SLifeLosses);
        checkEmpty(element);
        element=$("#PropertyLosses-select");
        AddInput(element,PropertyLosses);
        AddSelcted(element,SPropertyLosses);
        checkEmpty(element);
        element=$("#DriverLosses-select");
        AddInput(element,DriverLosses);
        AddSelcted(element,SDriverLosses);
        checkEmpty(element);
        element=$("#CoverageTypes-select");
        AddInput(element,CoverageTypes);
        checkEmpty(element);

        /* obj to post */

        let ModelId=parseInt(SModels);
        let UsingTypeId=parseInt(SUsingTypes);
        let ProductionYearId=parseInt(SProductionYears);
        let PreviousInsuranceStatusId=parseInt(SInsuranceStatuses);
        let ReleaseDate=SNumberdate;
        let PreviousCompanyId=parseInt(SCompanies);
        let PreviousDurationId=parseInt(SDurations);
        let PreviousExpirationDate=SEnddate;
        let DriverDiscountId=parseInt(SDriverDiscounts);
        let ThirdPartyDiscountId=parseInt(SThirdPartyDiscounts);
        let LifeLossId=parseInt(SLifeLosses);
        let PropertyLossId=parseInt(SPropertyLosses);
        let DriverLossId=parseInt(SDriverLosses);

        SendObj={ ModelId:ModelId,UsingTypeId:UsingTypeId,ProductionYearId:ProductionYearId,PreviousInsuranceStatusId:PreviousInsuranceStatusId,PreviousDurationId:PreviousDurationId,
            PreviousCompanyId:PreviousCompanyId ,PreviousExpirationDate:PreviousExpirationDate ,DriverDiscountId:DriverDiscountId ,ThirdPartyDiscountId:ThirdPartyDiscountId ,
            LifeLossId:LifeLossId ,  PropertyLossId:PropertyLossId , DriverLossId:DriverLossId,ReleaseDate:ReleaseDate,

            _token : '{{csrf_token()}}'
        };

        /* count neccessary empty fileds */
        let emptyCount=CountEmptyBox();

        PostData(SendObj,emptyCount);


    })();

}
/* put arrays of api in select inputs */
function AddInput(el,arr) {


    let child="";

    for (let i=0;i<arr.length;i++) {

        child=child+"<option value="+arr[i].Id +">"+arr[i].Title +"</option>";

    }

    el.children().remove();
    let FirstOp="<option value='00'> انتخاب کنید  </option>";
    el.append(FirstOp);
    el.append(child);
}
/* put selected to selected option in element */
function AddSelcted(el,sel) {

    el.find("option[value="+sel+"]").attr("selected","selected");
    el.val(sel);
}
/* put value */
function AddValue(el,vl) {
    el.attr("value",vl);
}
/* check empty fields */
function checkEmpty(el){

    let sv=el.val();

    let ty=el.attr("type");
    if (ty=="date") { sv=el.attr("value"); }

    //no select-empty
    if ( (sv == "00" ) ||(sv==" ") ) {
        //show error
        el.parent().addClass("empty-box");
    }
    // select-full
    else {
        //hide error
        el.parent().removeClass("empty-box");
    }

}
/* count empty box */
function CountEmptyBox() {

    let i=0;
    $("#wizard-compare1").find(".empty-box.req").each(function () {
        i=i+1;
    });
    return i;
}
/* post data to api */
function PostData(obj,emptyCount){


    //is empty
    if (emptyCount > 0 ) {


        $(".result-list").children().remove();

        let str="<div  class='result-row empty-result'> <span class='fa fa-exclamation-triangle empty-alert'> </span><span> توجه! برای نمایش بسته های پیشنهادی بیمه ، اطلاعات مورد نیاز را وارد نمایید</span></div>";

        setTimeout(function () {
            $(".result-list").append(str);
        },200);
    }
    //is full
    else {

        (async () =>{

            try {
                const response = await fn1('api/insurance/insurance/third-party/inquiry', 'POST', obj, {});
                const res = await response.json();

                var msg=res.Message;
                var ResArr=res.Inquiries;
                var CompArr=res.Companies;
                var DurArr=res.Durations;

                $(".result-list").children().remove();


                //has error message
                if (msg !=null) {

                    let str="<div  class='result-row empty-result'> <span class='fa fa-exclamation-triangle empty-alert'> </span><span>"+ msg +"</span></div>";
                    setTimeout(function () {
                        $(".result-list").append(str);
                    },200);
                }

                //no error message
                else {


                    if (ResArr.length==0) {

                        let str="<div  class='result-row empty-result'> <span class='fa fa-exclamation-triangle empty-alert'> </span><span> هیچ نتیجه ای برای این بیمه وجود ندارد</span></div>";
                        setTimeout(function () {
                            $(".result-list").append(str);
                        },200);
                    }
                    else {
                        loading(2000);
                        for (let i=0;i<ResArr.length;i++) {

                            var UniqueId=ResArr[i].UniqueId;

                            var TarhTitle=ResArr[i].Title;
                            var TarhSubTitle=ResArr[i].SubTitle;

                            var FirstAmount=ResArr[i].CashPrice.FirstAmount;
                            var FinalAmount=ResArr[i].CashPrice.FinalAmount;

                            var dis=false;

                            if (FirstAmount != FinalAmount ) {
                                dis=true;
                            }

                            var HasInstallments=ResArr[i].HasInstallments;

                            var Description=ResArr[i].Details.Description;
                            var DelayPenalty=ResArr[i].Details.DelayPenalty;
                            var PenaltyDayCount=ResArr[i].Details.PenaltyDayCount;

                            var CompanyId=ResArr[i].CompanyId;
                            var DurationId=ResArr[i].DurationId;

                            for (let j=0;j<CompArr.length;j++){
                                if ( CompArr[j].Id==CompanyId )  {
                                    var CompTitlt=CompArr[j].Title;
                                    var CompLogo=CompArr[j].LogoURL;
                                    var CompStar=CompArr[j].FinancialStrength;
                                    var CompBranch=CompArr[j].CompensationBranchCount;
                                }
                            }

                            for (let j=0;j<DurArr.length;j++){
                                if ( DurArr[j].Id==DurationId )  {
                                    var DurTitlt=DurArr[j].Title;
                                }
                            }

                            let  str="<div class='result-row' id="+UniqueId+">";

                            str=str+"<div class='result-row-box'>";

                            if (TarhTitle !=null) {
                                str=str+ "<div class='tarh-box'><span>"+TarhTitle+"</span>";
                            }
                            if (TarhSubTitle !=null) {
                                str=str+ "<span class='trah-sub'>"+TarhSubTitle+"</span>";
                            }
                            if (TarhTitle !=null) {
                                str=str+"</div>";
                            }

                            str=str+ "<div class='company-logo'><img src="+CompLogo+" alt="+CompTitlt+"></div>";

                            str=str+ "<div class='company-desc'><div class='desc-col'><span> سطح توانگری مالی </span>";
                            for (let k=0;k<CompStar;k++){
                                str=str+" <span class='fa fa-star star'></span>";
                            }
                            str=str+ "</div><div class='desc-col'><span class='branchCount'>"+CompBranch+"</span><span> شعبه پرداخت خسارت </span></div></div>";

                            str=str+ "<div class='company-cover'><pre>"+Description+"</pre></div>";

                            str=str+ "<div class='company-dur'>"+DurTitlt+"</div>" ;

                            str=str+ "<div class='company-money'>";
                            if (dis) {
                                str=str+"<div class='company-col discount'>"+FirstAmount+"</div>" ;
                            }
                            str=str+"<div class='company-col'>"+FinalAmount+"</div>" ;
                            str=str+ "<p class='rial'> ریال </p></div>";

                            str=str+ "<div class='penalty-box'> <div class='penalty-col'> <span>   تعداد روز های جریمه : </span>"+ PenaltyDayCount+" </div>" ;
                            str=str+ "<div class='penalty-col'> <span>   مبلغ جریمه دیرکرد  : </span>"+ DelayPenalty+" ریال </div>" ;
                            str=str+ "</div>";

                            str=str+ "<div class='pay-box'> <button class='btn'> خرید نقدی </button>" ;
                            if (HasInstallments !=null) {
                                str=str+ "<button class='btn aghsat'> اقساطی </button>" ;
                            }
                            str=str+ "</div>";

                            str=str+ "</div>";

                            str=str+ "</div>";

                            setTimeout(function () {
                                $(".result-list").append(str);
                            },200);

                        }
                    }
                }


            }
            catch (e) {
                console.log(e);
            }

        })();
    }

}
/* get model of selected brand */
function GetCarModel(CarBrandId) {


    const data = new FormData();
    data.append('_token', '{{csrf_token()}}');

    let address='api/insurance/insurance/third-party/car-model/'+CarBrandId;

    let CarModels=[];

    (async () =>{

        try {
            const response = await fn1(address, 'GET', data, {});
            const res = await response.json();
            CarModels=res.Models;

            console.log(CarModels);
        }
        catch (e) {
            console.log(e);
        }

        let element;
        element=$("#Model-select");
        AddInput(element,CarModels);

    })();

    Innerloading(1000);

}
/* for loading*/
function loading(time) {

      $(".loading-box").removeClass("dn");
      $(".loading-box .loading-sq").addClass("rotate-class");

      setTimeout(function () {
          $(".loading-box").addClass("dn");
          $(".loading-box .loading-sq").removeClass("rotate-class");
      },time);

}
