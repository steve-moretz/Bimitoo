
    /* ------------------------------------------------------ in first  -----------------------------------------------------------------*/


    /* get from api */
    GetInfoFromApi();

    /* ------------------------------------------------------ in changes  -----------------------------------------------------------------*/

    /* go to compare page */
    $("#wizard5 .wizard-finish").click(function () {

        let queryst="";

        let Countries=$("#Countries-select").val();
        queryst=queryst+"Countries="+Countries;
        let TravelDurations=$("#TravelDurations-select").val();
        queryst=queryst+"&TravelDurations="+TravelDurations;
        let Plans=$("#Plans-select").val();
        queryst=queryst+"&Plans="+Plans;
        let BirthDate=$("#BirthDate-select").attr("value");
        queryst=queryst+"&BirthDate="+BirthDate;
        let VisaTypes=$("#VisaTypes-select").val();
        queryst=queryst+"&VisaTypes="+VisaTypes;

        window.open("http://bimitoo.com/travel-insurance-compare?"+queryst,"_self");

    });

    $("#BirthDate-select").change(function () {

        let nv=$(this).val();

        let t=nv.length;
        if  (t==0) {nv=" ";}

        $(this).attr("value",nv);


    });

    /* -------------------------------------------------------------- my functions -----------------------------------------*/
    /* get information from api for insurance form */
    function  GetInfoFromApi() {

        const data = new FormData();
        data.append('_token', '{{csrf_token()}}');

        let Countries=[];
        let TravelDurations=[];
        let Plans=[];
        let VisaTypes=[];

        (async () =>{

            try {
                const response = await fn1('api/insurance/insurance/travel-plus/basic-data', 'GET', data, {});
                const res = await response.json();

                Countries=res.Countries;
                TravelDurations=res.Durations;
                Plans=res.Plans;
                VisaTypes=res.VisaTypes;
            }
            catch (e) {
                console.log(e);
            }

            let element;
            element=$("#Countries-select");
            AddInput(element,Countries);
            element=$("#TravelDurations-select");
            AddInput(element,TravelDurations);
            element=$("#Plans-select");
            AddInput(element,Plans);
            element=$("#VisaTypes-select");
            AddInput(element,VisaTypes);

        })();
    }
    /* put arrays of api in select inputs */
    function AddInput(el,arr) {

        let child="";

        for (let i=0;i<arr.length;i++) {

            child=child+"<option value="+arr[i].Id +">"+arr[i].Title +"</option>";

        }
        el.children().remove();
        let FirstOp="<option value='00'> انتخاب کنید  </option>";
        el.append(FirstOp);
        el.append(child);
    }


