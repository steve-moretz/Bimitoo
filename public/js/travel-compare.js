/* ------------------------------------------------------ in first  -----------------------------------------------------------------*/

//to save send object data
var SendObj;

/* get from api and prev page and check are empty or not*/
GetInfoFromApiPrev();

/* ------------------------------------------------------ in changes  -----------------------------------------------------------------*/

/* check empty */
$("#wizard-compare5 .compare-input").change(function(){

    let el=$(this);

    let Newval=el.val();

    let NameInObj=el.attr("NameInObj");

    let ty=el.attr("type");
    if ( ty=="date" ) { Newval=el.attr("value"); }

    checkEmpty(el);

    let NewCount=CountEmptyBox();

    SendObj[NameInObj]=Newval;
    PostData(SendObj,NewCount);

});

$("#BirthDate-select").change(function () {

    let nv=$(this).val();

    let t=nv.length;
    if  (t==0) {nv=" ";}

    $(this).attr("value",nv);

});

/* -------------------------------------------------------------- my functions -----------------------------------------*/

/* get information from api for insurance form */
function  GetInfoFromApiPrev() {

    /* get list of selects box from api */

    const data = new FormData();
    data.append('_token', '{{csrf_token()}}');


    let Countries=[];
    let TravelDurations=[];
    let Plans=[];
    let VisaTypes=[];

    (async () =>{

        try {
            const response = await fn1('api/insurance/insurance/travel-plus/basic-data', 'GET', data, {});
            const res = await response.json();

            Countries=res.Countries;
            TravelDurations=res.Durations;
            Plans=res.Plans;
            VisaTypes=res.VisaTypes;
        }
        catch (e) {
            console.log(e);
        }

        /* get selected option from prev page */
        let searchParams = new URLSearchParams(window.location.search);
        let SCountries= searchParams.get('Countries');
        let STravelDurations= searchParams.get('TravelDurations');
        let SPlans= searchParams.get('Plans');
        let SVisaTypes= searchParams.get('VisaTypes');
        let SBirthDate= searchParams.get('BirthDate');


        /* put select box lists and selected option-check empty or not - check count az empty or not */
        let element;
        element=$("#Countries-select");
        AddInput(element,Countries);
        AddSelcted(element,SCountries);
        checkEmpty(element);
        element=$("#TravelDurations-select");
        AddInput(element,TravelDurations);
        AddSelcted(element,STravelDurations);
        checkEmpty(element);
        element=$("#Plans-select");
        AddInput(element,Plans);
        AddSelcted(element,SPlans);
        checkEmpty(element);
        element=$("#VisaTypes-select");
        AddInput(element,VisaTypes);
        AddSelcted(element,SVisaTypes);
        checkEmpty(element);
        element=$("#BirthDate-select");
        AddValue(element,SBirthDate);
        checkEmpty(element);
        /* obj to post */

        let CountryId=parseInt(SCountries);
        let DurationId=parseInt(STravelDurations);
        let VisaTypeId=parseInt(SVisaTypes);
        let PlanId=parseInt(SPlans);
        let BirthDate=SBirthDate;


        SendObj={ CountryId:CountryId,DurationId:DurationId,VisaTypeId:VisaTypeId,PlanId:PlanId,BirthDate:BirthDate,
            _token : '{{csrf_token()}}'
        };


        /* count neccessary empty fileds */
        let emptyCount=CountEmptyBox();


        PostData(SendObj,emptyCount);

    })();

}
/* put arrays of api in select inputs */
function AddInput(el,arr) {


    let child="";

    for (let i=0;i<arr.length;i++) {

        child=child+"<option value="+arr[i].Id +">"+arr[i].Title +"</option>";

    }

    el.children().remove();
    let FirstOp="<option value='00'> انتخاب کنید  </option>";
    el.append(FirstOp);
    el.append(child);
}
/* put selected to selected option in element */
function AddSelcted(el,sel) {

    el.find("option[value="+sel+"]").attr("selected","selected");
    el.val(sel);
}
/* put value */
function AddValue(el,vl) {
    el.attr("value",vl);
}
/* check empty fields */
function checkEmpty(el){

    let sv=el.val();

    let ty=el.attr("type");
    if (ty=="date") { sv=el.attr("value"); }

    //no select-empty
    if ( (sv == "00" ) ||(sv==" ") ) {
        //show error
        el.parent().addClass("empty-box");
    }
    // select-full
    else {
        //hide error
        el.parent().removeClass("empty-box");
    }

}
/* count empty box */
function CountEmptyBox() {

    let i=0;
    $("#wizard-compare5").find(".empty-box.req").each(function () {
        i=i+1;
    });
    return i;
}
/* post data to api */
function PostData(obj,emptyCount){

    //is empty
    if (emptyCount > 0 ) {


        $(".result-list").children().remove();

        let str="<div  class='result-row empty-result'> <span class='fa fa-exclamation-triangle empty-alert'> </span><span> توجه! برای نمایش بسته های پیشنهادی بیمه ، اطلاعات مورد نیاز را وارد نمایید</span></div>";

        setTimeout(function () {
            $(".result-list").append(str);
        },200);
    }
    //is full
    else {

        (async () =>{

            try {
                const response = await fn1('api/insurance/insurance/travel-plus/inquiry', 'POST', obj, {});
                const res = await response.json();

                var msg=res.Message;
                var ResArr=res.Inquiries;
                var CompArr=res.Companies;
                var DurArr=res.Durations;

                $(".result-list").children().remove();

                //has error message
                if (msg !=null) {

                    let str="<div  class='result-row empty-result'> <span class='fa fa-exclamation-triangle empty-alert'> </span><span>"+ msg +"</span></div>";
                    setTimeout(function () {
                        $(".result-list").append(str);
                    },200);
                }

                //no error message
                else{

                    if (ResArr.length==0) {

                        let str="<div  class='result-row empty-result'> <span class='fa fa-exclamation-triangle empty-alert'> </span><span> هیچ نتیجه ای برای این بیمه وجود ندارد</span></div>";
                        setTimeout(function () {
                            $(".result-list").append(str);
                        },200);
                    }
                    else {
                        loading(2000);
                        for (let i=0;i<ResArr.length;i++) {

                            var UniqueId=ResArr[i].UniqueId;

                            var TarhTitle=ResArr[i].Title;
                            var TarhSubTitle=ResArr[i].SubTitle;

                            var FirstAmount=ResArr[i].CashPrice.FirstAmount;
                            var FinalAmount=ResArr[i].CashPrice.FinalAmount;

                            var dis=false;

                            if (FirstAmount != FinalAmount ) {
                                dis=true;
                            }

                            var HasInstallments=ResArr[i].HasInstallments;

                            var PlanTitle=ResArr[i].Details.PlanTitle;
                            var  CoverLimit=ResArr[i].Details.CoverLimit;
                            var CoverageDetails=ResArr[i].Details.CoverageDetails;
                            var cdlen=CoverageDetails.length;

                            var CompanyId=ResArr[i].CompanyId;
                            var DurationId=ResArr[i].DurationId;

                            for (let j=0;j<CompArr.length;j++){
                                if ( CompArr[j].Id==CompanyId )  {
                                    var CompTitlt=CompArr[j].Title;
                                    var CompLogo=CompArr[j].LogoURL;
                                    var CompStar=CompArr[j].FinancialStrength;
                                    var CompBranch=CompArr[j].CompensationBranchCount;
                                }
                            }

                            for (let j=0;j<DurArr.length;j++){
                                if ( DurArr[j].Id==DurationId )  {
                                    var DurTitlt=DurArr[j].Title;
                                }
                            }

                            let  str="<div class='result-row' id="+UniqueId+">";

                            str=str+"<div class='result-row-box'>";

                            if (TarhTitle !=null) {
                                str=str+ "<div class='tarh-box'><span>"+TarhTitle+"</span>";
                            }
                            if (TarhSubTitle !=null) {
                                str=str+ "<span class='trah-sub'>"+TarhSubTitle+"</span>";
                            }
                            if (TarhTitle !=null) {
                                str=str+"</div>";
                            }

                            str=str+ "<div class='company-logo'><img src="+CompLogo+" alt="+CompTitlt+"></div>";

                            str=str+ "<div class='company-desc'><div class='desc-col'><span> سطح توانگری مالی </span>";
                            for (let k=0;k<CompStar;k++){
                                str=str+" <span class='fa fa-star star'></span>";
                            }
                            str=str+ "</div><div class='desc-col'><span class='branchCount'>"+CompBranch+"</span><span> شعبه پرداخت خسارت </span></div></div>";

                            str=str+ "<div class='plan-desc'><div class='plan-col'><span> حداکثر پوشش : </span>"+CoverLimit+"</div></div>";

                            str=str+ "<div class='company-dur'>"+DurTitlt+"</div>" ;

                            str=str+ "<div class='company-money'>";
                            if (dis) {
                                str=str+"<div class='company-col discount'>"+FirstAmount+"</div>" ;
                            }
                            str=str+"<div class='company-col'>"+FinalAmount+"</div>" ;
                            str=str+ "<p class='rial'> ریال </p></div>";

                            str=str+ "<div class='pay-box'> <button class='btn'> خرید نقدی </button>" ;
                            if (HasInstallments !=null) {
                                str=str+ "<button class='btn aghsat'> اقساطی </button>" ;
                            }
                            str=str+ "</div>";

                            str=str+ "</div>";

                            if (cdlen!=0) {

                                str=str+ "<div class='result-row-detail'><div class='plan-col'>" ;
                                let colid="collapse"+i;
                                let colidd="#collapse"+i;
                                str=str+ "<a href="+colidd +" data-toggle='collapse' > جزییات طرح <span class='fa fa-angle-down text-danger'></span> </a>";
                                str=str+ "<p class='collapse' id="+colid+">"+CoverageDetails+"</p></div></div>" ;
                            }

                            str=str+ "</div>";

                            setTimeout(function () {
                                $(".result-list").append(str);
                            },200);

                        }
                    }
                }


            }
            catch (e) {
                console.log(e);
            }

        })();
    }

}
/* for loading*/
function loading(time) {

    $(".loading-box").removeClass("dn");
    $(".loading-box .loading-sq").addClass("rotate-class");

    setTimeout(function () {
        $(".loading-box").addClass("dn");
        $(".loading-box .loading-sq").removeClass("rotate-class");
    },time);

}
