
    /* ------------------------------------------------------ in first  -----------------------------------------------------------------*/

    /* set index in wizard form */
    let el2=$("#wizard2");
    SetWizardStepId(el2);
    SetWizardIndText(el2);

    /* get from api */
    GetInfoFromApi();

    /* ------------------------------------------------------ in changes  -----------------------------------------------------------------*/

    /* check active or not in steps of form */
    $("#MotorInsuranceStatuses-select").change(function () {

        let v=$(this).val();
        let par=$("#wizard2");

        //has prev insurance
        if (v=="2") {

            par.find(".wizard-conditional").removeClass("dn");
            //show conditional-btn-next filed
            par.find(".conditional-btn-next").removeClass("dn");
            //hide conditional-btn-reg filed
            par.find(".number-condition-reg").addClass("dn");
        }
        //dont not has prev insurance
        else {

            par.find(".wizard-conditional").addClass("dn");
            //hide conditional-btn-next filed
            par.find(".conditional-btn-next").addClass("dn");
            //show conditional-btn-reg filed
            par.find(".number-condition-reg").removeClass("dn");

        }

        SetWizardStepId(par);
        SetWizardIndText(par);

    });
    $("#MotorLossstatus-select").change(function () {

        let v=$(this).val();
        let par=$("#wizard2");

        if (v==0) {

            par.find(".loss-condition").addClass("dn");

        }
        else {

            par.find(".loss-condition").removeClass("dn");

        }

    });

    $("#MotorEnd-date").change(function () {

        let nv=$(this).val();

        let t=nv.length;
        if  (t==0) {nv=" ";}

        $(this).attr("value",nv);

    });

    /* go to compare page */
    $("#wizard2 .wizard-finish").click(function () {

        let queryst="";


        let MotorModels=$("#MotorModel-select").val();
        queryst=queryst+"&MotorModels="+MotorModels;
        let MotorProductionYears=$("#MotorProductionYears-select").val();
        queryst=queryst+"&MotorProductionYears="+MotorProductionYears;
        let MotorInsuranceStatuses=$("#MotorInsuranceStatuses-select").val();
        queryst=queryst+"&MotorInsuranceStatuses="+MotorInsuranceStatuses;
        let MotorCompanies=$("#MotorCompanies-select").val();
        queryst=queryst+"&MotorCompanies="+MotorCompanies;
        let MotorDurations=$("#MotorDurations-select").val();
        queryst=queryst+"&MotorDurations="+MotorDurations;
        let MotorEnddate=$("#MotorEnd-date").attr("value");
        queryst=queryst+"&MotorEnddate="+MotorEnddate;
        let MotorDriverDiscounts=$("#MotorDriverDiscounts-select").val();
        queryst=queryst+"&MotorDriverDiscounts="+MotorDriverDiscounts;
        let MotorThirdPartyDiscounts=$("#MotorThirdPartyDiscounts-select").val();
        queryst=queryst+"&MotorThirdPartyDiscounts="+MotorThirdPartyDiscounts;
        let MotorLossstatus=$("#MotorLossstatus-select").val();
        queryst=queryst+"&MotorLossstatus="+MotorLossstatus;
        let MotorLifeLosses=$("#MotorLifeLosses-select").val();
        queryst=queryst+"&MotorLifeLosses="+MotorLifeLosses;
        let MotorPropertyLosses=$("#MotorPropertyLosses-select").val();
        queryst=queryst+"&MotorPropertyLosses="+MotorPropertyLosses;
        let MotorDriverLosses=$("#MotorDriverLosses-select").val();
        queryst=queryst+"&MotorDriverLosses="+MotorDriverLosses;

        window.open("http://bimitoo.com/motor-third-party-insurance-compare?"+queryst,"_self");

    });

    /* -------------------------------------------------------------- my functions -----------------------------------------*/

    /* get information from api for insurance form */
    function  GetInfoFromApi() {

        const data = new FormData();
        data.append('_token', '{{csrf_token()}}');

        let MotorTypes=[];
        let MotorProductionYears=[];
        let MotorInsuranceStatuses=[];
        let MotorCompanies=[];
        let MotorDurations=[];
        let MotorDriverDiscounts=[];
        let MotorThirdPartyDiscounts=[];
        let MotorLifeLosses=[];
        let MotorPropertyLosses=[];
        let MotorDriverLosses=[];

        (async () =>{

            try {
                const response = await fn1('api/insurance/insurance/motor/basic-data', 'GET', data, {});
                const res = await response.json();

                MotorTypes=res.MotorTypes;
                MotorProductionYears=res.ProductionYears;
                MotorInsuranceStatuses=res.InsuranceStatuses;
                MotorCompanies=res.Companies;
                MotorDurations=res.Durations;
                MotorDriverDiscounts=res.DriverDiscounts;
                MotorThirdPartyDiscounts=res.ThirdPartyDiscounts;
                MotorLifeLosses=res.LifeLosses;
                MotorPropertyLosses=res.PropertyLosses;
                MotorDriverLosses=res.DriverLosses;

            }
            catch (e) {
                console.log(e);
            }

            let element;
            element=$("#MotorModel-select");
            AddInput(element,MotorTypes);
            element=$("#MotorProductionYears-select");
            AddInput(element,MotorProductionYears);
            element=$("#MotorInsuranceStatuses-select");
            AddInput(element,MotorInsuranceStatuses);
            element=$("#MotorCompanies-select");
            AddInput(element,MotorCompanies);
            element=$("#MotorDurations-select");
            AddInput(element,MotorDurations);
            element=$("#MotorDriverDiscounts-select");
            AddInput(element,MotorDriverDiscounts);
            element=$("#MotorThirdPartyDiscounts-select");
            AddInput(element,MotorThirdPartyDiscounts);
            element=$("#MotorLifeLosses-select");
            AddInput(element,MotorLifeLosses);
            element=$("#MotorPropertyLosses-select");
            AddInput(element,MotorPropertyLosses);
            element=$("#MotorDriverLosses-select");
            AddInput(element,MotorDriverLosses);

        })();
    }
    /* put arrays of api in select inputs */
    function AddInput(el,arr) {

        let child="";

        for (let i=0;i<arr.length;i++) {

            child=child+"<option value="+arr[i].Id +">"+arr[i].Title +"</option>";

        }
        el.children().remove();
        let FirstOp="<option value='00'> انتخاب کنید  </option>";
        el.append(FirstOp);
        el.append(child);
    }
    /* put dynamic index in wizard steps */
    function SetWizardStepId(el){

        let i=1;
        el.find(".wizard-step").each(function(){

            let t=$(this).hasClass("dn");
            if (!t) {
                let strwid="step"+i.toString();
                $(this).attr("id",strwid);
                i=i+1;
            }
            else {
                $(this).removeAttr("id");
            }

        });

    }
    function SetWizardIndText(el){

        let i=1;
        el.find(".wizard-step-indicator").each(function(){

            let t=$(this).hasClass("dn");

            if (!t) {
                let strwid=i.toString();
                $(this).find("span").text(strwid);
                i=i+1;
            }
            else {
                $(this).find("span").text("");
            }

        });

    }


