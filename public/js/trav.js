
    /* ------------------------------------------------------ in first  -----------------------------------------------------------------*/

    var CountryListv=[];
    var CountryListt=[];

    /* get from api */
    GetInfoFromApi();

    /* ------------------------------------------------------ in changes  -----------------------------------------------------------------*/

    $("#TravCountries-select").change(function () {

        let SelElVl=$(this).val();

        if (SelElVl !="00") {

            let stop="option[value='"+SelElVl+"']";
            let SelEl= $(this).find(stop);

            SelEl.toggleClass("sel");

            SelectedCountry();

            ShowCountry(CountryListt);
        }

    });

    /* go to compare page */
    $("#wizard4 .wizard-finish").click(function () {

        let queryst="";

        console.log(CountryListv);

        queryst=queryst+"CountryList="+CountryListv;
        let TravDurations=$("#TravDurations-select").val();
        queryst=queryst+"&TravDurations="+TravDurations;
        let TravBirthDate=$("#TravBirthDate-select").attr("value");
        queryst=queryst+"&TravBirthDate="+TravBirthDate;

        window.open("http://bimitoo.com/life-insurance-compare?"+queryst,"_self");

    });

    $("#TravBirthDate-select").change(function () {

        let nv=$(this).val();

        let t=nv.length;
        if  (t==0) {nv=" ";}

        $(this).attr("value",nv);


    });

    /* -------------------------------------------------------------- my functions -----------------------------------------*/
    /* get information from api for insurance form */
    function  GetInfoFromApi() {

        const data = new FormData();
        data.append('_token', '{{csrf_token()}}');

        let TravCountries=[];
        let TravDurations=[];


        (async () =>{

            try {
                const response = await fn1('api/insurance/insurance/travel/basic-data', 'GET', data, {});
                const res = await response.json();

                TravCountries=res.Countries;
                TravDurations=res.Durations;

            }
            catch (e) {
                console.log(e);
            }

            let element;
            element=$("#TravCountries-select");
            AddInput(element,TravCountries);
            element=$("#TravDurations-select");
            AddInput(element,TravDurations);

        })();
    }
    /* put arrays of api in select inputs */
    function AddInput(el,arr) {

        let child="";

        for (let i=0;i<arr.length;i++) {

            child=child+"<option value="+arr[i].Id +">"+arr[i].Title +"</option>";

        }
        el.children().remove();
        let FirstOp="<option value='00'> انتخاب کنید  </option>";
        el.append(FirstOp);
        el.append(child);
    }
    /* update SelectedCountry */
    function SelectedCountry() {

        let arrv=[];
        let arrt=[];

        $("#TravCountries-select").find(".sel").each(function () {

            let cl=$(this).attr("class");

            if (cl=="sel") {

                let v=$(this).attr("value");
                let t=$(this).text();

                arrv.push(v);
                arrt.push(t);
            }

        });

        CountryListv=arrv;
        CountryListt=arrt;

    }
    /* show country list */
    function ShowCountry(list) {

        $(".country-listt").children().remove();

        let st="";
        st=st+"<div class='country-title'>کشورهای انتخابی : </div>";

        for (let i=0;i<list.length;i++) {

            st=st+"<div class='country-box'>"+list[i]+"</div>";
        }

        $(".country-listt").append(st);


    }

