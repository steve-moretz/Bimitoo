
/* ------------------------------------------------------ in first  -----------------------------------------------------------------*/



/* get from api */
GetInfoFromApi();

/* ------------------------------------------------------ in changes  -----------------------------------------------------------------*/

$("#HealthDate-input").change(function () {

    let nv=$(this).val();

    let t=nv.length;
    if  (t==0) {nv=" ";}

    $(this).attr("value",nv);

});

/* go to compare page */
$("#wizard6 .wizard-finish").click(function () {

    let queryst="";

    let HealthDate=$("#HealthDate-input").attr("value");
    queryst=queryst+"HealthDate="+HealthDate;
    let HealthBasicInsurers=$("#HealthBasicInsurers-select").val();
    queryst=queryst+"&HealthBasicInsurers="+HealthBasicInsurers;

    window.open("http://bimitoo.com/supplementary-health-insurance-compare?"+queryst,"_self");

});

/* -------------------------------------------------------------- my functions -----------------------------------------*/

/* get information from api for insurance form */
function  GetInfoFromApi() {

    const data = new FormData();
    data.append('_token', '{{csrf_token()}}');

    let BasicInsurers=[];

    (async () =>{

        try {
            const response = await fn1('api/insurance/insurance/health/basic-data', 'GET', data, {});
            const res = await response.json();

            BasicInsurers=res.BasicInsurers;

        }
        catch (e) {
            console.log(e);
        }

        let element;
        element=$("#HealthBasicInsurers-select");
        AddInput(element,BasicInsurers);

    })();
}
/* put arrays of api in select inputs */
function AddInput(el,arr) {

    let child="";

    for (let i=0;i<arr.length;i++) {

        child=child+"<option value="+arr[i].Id +">"+arr[i].Title +"</option>";

    }
    el.children().remove();
    let FirstOp="<option value='00'> انتخاب کنید  </option>";
    el.append(FirstOp);
    el.append(child);
}



