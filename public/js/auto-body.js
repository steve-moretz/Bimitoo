
    /* ------------------------------------------------------ in first  -----------------------------------------------------------------*/

    /* get from api */
    GetInfoFromApi();

    /* ------------------------------------------------------ in changes  -----------------------------------------------------------------*/

    /* get model */
    $("#AutoBrands-select").change(function () {

        let AutoBr=$(this).val();

        if (AutoBr !="00"){
            GetAutoModel(AutoBr);
        }

    });

    /* check active or not in steps of form */
    $("#AutoInsuranceStatuses-select").change(function () {

        let v=$(this).val();
        let par=$("#wizard3");

        //has prev insurance
        if (v==1) {

            par.find(".AutoYear-condition").removeClass("dn");

        }
        //dont not has prev insurance
        else {

            par.find(".AutoYear-condition").addClass("dn");


        }

    });

    /* go to compare page */
    $("#wizard3 .wizard-finish").click(function () {

        let queryst="";

        let AutoBrands=$("#AutoBrands-select").val();
        queryst=queryst+"AutoBrands="+AutoBrands;
        let AutoModels=$("#AutoModel-select").val();
        queryst=queryst+"&AutoModels="+AutoModels;
        let AutoUsingTypes=$("#AutoUsingTypes-select").val();
        queryst=queryst+"&AutoUsingTypes="+AutoUsingTypes;
        let AutoProductionYears=$("#AutoProductionYears-select").val();
        queryst=queryst+"&AutoProductionYears="+AutoProductionYears;
        let AutoLife=$("#AutoLife-select").val();
        queryst=queryst+"&AutoLife="+AutoLife;
        let AutoPrice=$("#AutoPrice-select").val();
        queryst=queryst+"&AutoPrice="+AutoPrice;
        let AutoVaredat=$("#AutoVaredat-select").val();
        queryst=queryst+"&AutoVaredat="+AutoVaredat;
        let AutoInsuranceStatuses=$("#AutoInsuranceStatuses-select").val();
        queryst=queryst+"&AutoInsuranceStatuses="+AutoInsuranceStatuses;
        let AutoNoDisYear=$("#AutoNoDisYear-select").val();
        queryst=queryst+"&AutoNoDisYear="+AutoNoDisYear;

        queryst="للل"

        window.open("http://bimitoo.com/auto-body-insurance-compare?"+queryst,"_self");

    });

    /* -------------------------------------------------------------- my functions -----------------------------------------*/

    /* get information from api for insurance form */
    function  GetInfoFromApi() {

        const data = new FormData();
        data.append('_token', '{{csrf_token()}}');

        let Brands=[];
        let UsingTypes=[];
        let ProductionYears=[];
        let CarBodyNoDamageYears=[];

        (async () =>{

            try {
                const response = await fn1('api/insurance/insurance/car-body/basic-data', 'GET', data, {});
                const res = await response.json();

                Brands=res.Brands;
                UsingTypes=res.UsingTypes;
                ProductionYears=res.ProductionYears;
                CarBodyNoDamageYears=res.CarBodyNoDamageYears;
            }
            catch (e) {
                console.log(e);
            }

            let element;
            element=$("#AutoBrands-select");
            AddInput(element,Brands);
            element=$("#AutoUsingTypes-select");
            AddInput(element,UsingTypes);
            element=$("#AutoProductionYears-select");
            AddInput(element,ProductionYears);
            element=$("#AutoNoDisYear-select");
            AddInput(element,CarBodyNoDamageYears);
        })();
    }
    /* put arrays of api in select inputs */
    function AddInput(el,arr) {

        let child="";

        for (let i=0;i<arr.length;i++) {

            child=child+"<option value="+arr[i].Id +">"+arr[i].Title +"</option>";

        }
        el.children().remove();
        let FirstOp="<option value='00'> انتخاب کنید  </option>";
        el.append(FirstOp);
        el.append(child);
    }
    /* get model of selected brand */
    function GetAutoModel(AutoBr) {

        const data = new FormData();
        data.append('_token', '{{csrf_token()}}');

        let address='api/insurance/insurance/car-body/car-model/'+AutoBr;

        let AutoModels=[];

        (async () =>{

            try {
                const response = await fn1(address, 'GET', data, {});
                const res = await response.json();
                AutoModels=res.Models;
            }
            catch (e) {
                console.log(e);
            }

            let element;
            element=$("#AutoModel-select");
            AddInput(element,AutoModels);

        })();

        Innerloading(1000);
    }
    /* for inner loading*/
    function Innerloading(time) {

        $(".Innerloading-box").removeClass("dn");
        $(".Innerloading-box .Innerloading-sq").addClass("opac-class");

        setTimeout(function () {
            $(".Innerloading-box").addClass("dn");
            $(".Innerloading-box .Innerloading-sq").removeClass("opac-class");

        },time);

    }



