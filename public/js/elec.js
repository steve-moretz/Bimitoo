
    /* ------------------------------------------------------ in first  -----------------------------------------------------------------*/

    /* get from api */
    GetInfoFromApi();

    /* ------------------------------------------------------ in changes  -----------------------------------------------------------------*/

    /* get brand of selected device */
    $("#Device-select").change(function () {

        let SelDev=$(this).val();

        if (SelDev !="00"){
            GetDevBrand(SelDev);
        }

    });

    /* get model of selected brand device */
    $("#DeviceBrand-select").change(function () {

        let DevBrand=$(this).val();

        if (DevBrand !="00"){
            GetDevModel(DevBrand);
        }

    });

    /* go to compare page */
    $("#wizard8 .wizard-finish").click(function () {

        let queryst="";

        let Device=$("#Device-select").val();
        queryst=queryst+"Device="+Device;
        let DeviceBrand=$("#DeviceBrand-select").val();
        queryst=queryst+"&DeviceBrand="+DeviceBrand;
        let DeviceModel=$("#DeviceModel-select").val();
        queryst=queryst+"&DeviceModel="+DeviceModel;
        let DeviceFran=$("#DeviceFran-select").val();
        queryst=queryst+"&DeviceFran="+DeviceFran;
        let DevicePrice=$("#DevicePrice-input").val();
        queryst=queryst+"&DevicePrice="+DevicePrice;

        window.open("http://bimitoo.com/other-insurance-compare?"+queryst,"_self");

    });

    /* -------------------------------------------------------------- my functions -----------------------------------------*/

    /* get information from api for insurance form */
    function  GetInfoFromApi() {

        const data = new FormData();
        data.append('_token', '{{csrf_token()}}');


        let Devices=[];
        let DevicesFran=[];

        (async () =>{

            try {
                const response = await fn1('api/insurance/insurance/electronic-equipment/basic-data', 'GET', data, {});
                const res = await response.json();

                Devices=res.Devices;
                DevicesFran=res.Franchises;


            }
            catch (e) {
                console.log(e);
            }

            let element;
            element=$("#Device-select");
            AddInput(element,Devices);
            element=$("#DeviceFran-select");
            AddInput(element,DevicesFran);


        })();
    }
    /* put arrays of api in select inputs */
    function AddInput(el,arr) {

        let child="";

        for (let i=0;i<arr.length;i++) {

            child=child+"<option value="+arr[i].Id +">"+arr[i].Title +"</option>";

        }
        el.children().remove();
        let FirstOp="<option value='00'> انتخاب کنید  </option>";
        el.append(FirstOp);
        el.append(child);
    }
    /* get brand of selected device */
    function GetDevBrand(DevId) {

        const data = new FormData();
        data.append('_token', '{{csrf_token()}}');

        let address='api/insurance/insurance/electronic-equipment/brand/'+DevId;

        let DevBrands=[];

        (async () =>{

            try {
                const response = await fn1(address, 'GET', data, {});
                const res = await response.json();

                DevBrands=res.Brands;
            }
            catch (e) {
                console.log(e);
            }

            let element;
            element=$("#DeviceBrand-select");
            AddInput(element,DevBrands);

        })();

        Innerloading(1000);
    }
    /* get model of selected brand device */
    function GetDevModel(DevBraId) {

        const data = new FormData();
        data.append('_token', '{{csrf_token()}}');

        let address='api/insurance/insurance/electronic-equipment/model/'+DevBraId;

        let DevModels=[];

        (async () =>{

            try {
                const response = await fn1(address, 'GET', data, {});
                const res = await response.json();

                DevModels=res.Models;
            }
            catch (e) {
                console.log(e);
            }

            let element;
            element=$("#DeviceModel-select");
            AddInput(element,DevModels);

        })();

        Innerloading(1000);
    }
    /* for inner loading*/
    function Innerloading(time) {

        $(".Innerloading-box").removeClass("dn");
        $(".Innerloading-box .Innerloading-sq").addClass("opac-class");

        setTimeout(function () {
            $(".Innerloading-box").addClass("dn");
            $(".Innerloading-box .Innerloading-sq").removeClass("opac-class");

        },time);

    }


