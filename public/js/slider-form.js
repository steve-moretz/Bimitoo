
$(document).ready(function(){


    $(".search-section .tab-pane.active").addClass("show");

    //for filter
    $(".nav-filter .nav-link").click(function () {
        $(".nav-filter .nav-item").removeClass("active");
        $(this).parent(".nav-item").addClass("active");
    });

    //for filter slider
    var openm=1;
    $(".wizard-header .slider-counter").click(function () {

        var numb=$(this).html();
        var myid="#step"+numb;

        var myheader=$(this).parent().parent().parent().parent();
        var myform=myheader.parent();

        var actstp=myform.find(".wizard-step.active");
        actstp.removeClass("active");

        var mystep=myform.find(myid);
        mystep.addClass("active");

        myheader.find(".wizard-step-indicator.active").removeClass("active");
        $(this).parent().parent(".wizard-step-indicator").addClass("active");
    });

});
