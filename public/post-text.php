<?php 

 include('layout/header.php');

?>

<!-- post-text -->
    <section class="container" id="blog">
        <!-- header-article -->
        <div class="row">
            <div class="col-md-12">
                <div>
                    <div class="col-md-12 shadow img-box" >
                        <div class="col-md-offset-1 col-md-5 ">
                            <div class="col-md-12 ">
                                <div class="row" style="margin-bottom: 30px;">
                                    <div class="col-md-6">
                                        <div class="bd-example">
                                            <div id="carouselExampleCaptions" class="carousel slide"
                                                 data-ride="carousel">
                                                <div class="carousel-inner">
                                                    <div class="carousel-item active">
                                                        <a href="#">
                                                            <img src="/images/kids-at-eye-doctor-overheard-optometry.jpg"
                                                                 class="d-block w-100" alt="...">
                                                            <div class="carousel-caption d-none d-md-block blog-img-caption">
                                                                <p>آیا بیمه تکمیلی هزینه عمل چشم را پرداخت می‌کند؟</p>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="bd-example">
                                            <div id="carouselExampleCaptions" class="carousel slide"
                                                 data-ride="carousel">
                                                <div class="carousel-inner">
                                                    <div class="carousel-item active">
                                                        <a href="#">
                                                            <img src="/images/kids-at-eye-doctor-overheard-optometry.jpg"
                                                                 class="d-block w-100" alt="...">
                                                            <div class="carousel-caption d-none d-md-block blog-img-caption">
                                                                <p>آیا بیمه تکمیلی هزینه عمل چشم را پرداخت می‌کند؟</p>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="bd-example">
                                            <div id="carouselExampleCaptions" class="carousel slide"
                                                 data-ride="carousel">
                                                <div class="carousel-inner">
                                                    <div class="carousel-item active">
                                                        <a href="#">
                                                            <img src="/images/kids-at-eye-doctor-overheard-optometry.jpg"
                                                                 class="d-block w-100" alt="...">
                                                            <div class="carousel-caption d-none d-md-block blog-img-caption">
                                                                <p>آیا بیمه تکمیلی هزینه عمل چشم را پرداخت می‌کند؟</p>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="bd-example">
                                            <div id="carouselExampleCaptions" class="carousel slide"
                                                 data-ride="carousel">
                                                <div class="carousel-inner">
                                                    <div class="carousel-item active">
                                                        <a href="#">
                                                            <img src="/images/kids-at-eye-doctor-overheard-optometry.jpg"
                                                                 class="d-block w-100" alt="...">
                                                            <div class="carousel-caption d-none d-md-block blog-img-caption">
                                                                <p>آیا بیمه تکمیلی هزینه عمل چشم را پرداخت می‌کند؟</p>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=" col-md-5 col-md-offset-1 " style="margin-right: 5px">
                            <div class="bd-example">
                                <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <a href="#">
                                                <img src="/images/kids-at-eye-doctor-overheard-optometry.jpg"
                                                     class="d-block w-100" alt="...">
                                                <div class="carousel-caption d-none d-md-block blog-img-caption">
                                                    <p>آیا بیمه تکمیلی هزینه عمل چشم را پرداخت می‌کند؟</p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end header-article -->
        <div class="row blog-wrapper" style="margin-top: 50px">


        <?php 

            include('layout/sidebar.php');

        ?>
            <!-- start of list-article -->  
            <div class="col-md-8 col-sm-12 col-xs-12">
                <div class="shadow">

                    <div class="article ">
                        <div class="post-title">
                            <h2>بهترین بیمه شخص ثالث؟ ارزان ترین و گران ترین بیمه شخص ثالث</h2>
                        </div>
                        <div class="post-img">
                            <img src="/images/kids-at-eye-doctor-overheard-optometry.jpg" class="img-fluid"
                                 alt="Responsive image">
                        </div>
                        <div class="post-content">
                            <div class="post-details">
                                <p><i class="fas fa-calendar-alt" style="padding-left: 9px"></i>2014-08-5</p>
                                <p class="card-text">نویسنده : زهرا محمدی</p>
                            </div>
                            <div class="post-main">
                                <div class="paragraph" style="padding: 15px ">
                                    لطفا دقیقه نودی نباشید و بیمه‌نامه شخص ثالث خود را به موقع تمدید کنید! چون اگر با
                                    تاخیر این کار را انجام بدهید، دردسرهای زیادی برایتان به وجود می‌آید.

                                    در ادامه بیشتر این موضوع را توضیح می‌دهیم.


                                    <h4 class="sub-title">اعتبار بیمه نامه ثالث چه زمانی شروع و تمام می شود؟</h4>

                                    <p>اعتبار بیمه شخص ثالث شما از <strong>ساعت
                                            12 شب</strong> (ساعت 24) روزی که صادر می شود، شروع شده و تا ساعت 12
                                        شب (ساعت 24) آخرین روز بیمه نامه اعتبار دارد.</p>
                                    <p>یعنی مثلا اگر تاریخ تمام شدن بیمه شما 20 آذر باشد، اعتبار این بیمه ساعت 12 شب
                                        روز 20 آذر به پایان می رسد.</p>
                                    <p>حالا اگر بخواهید بعد از ظهر همان روز برای تمدید بیمه اقدام کنید، ریسک بزرگی
                                        کرده اید.</p>
                                    <p>چون ممکن است فرآیند صدور به شکلی پیش برود که بیمه شما روز بعد یعنی 21 آذر
                                        صادر شده و اعتبار آن از ساعت 12 شب 21 آذر شروع شود.</p>
                                    <p>در این حالت یک روز فاصله بین >بیمه ثالث قبلی و تمدید بیمه جدید
                                        به وجود می آید.</p>
                                    <p>برای این یک روز شما باید جریمه پرداخت کنید اما موضوع مهم تر از <strong>جریمه
                                            دیرکرد </strong>هم هست…
                                    </p>
                                    <p>اگر خدای ناکرده حادثه یا تصادفی در این روز برای شما اتفاق بیفتد و مقصر باشید،
                                        <strong>بیمه آن را پوشش نمی دهد</strong>. در واقع شما بیمه ای
                                        ندارید که بخواهید از آن استفاده کنید.</p>
                                    <p>همچنین اگر در این روز از ماشین خود استفاده کنید، ممکن است پلیس شما را
                                        <strong>جریمه</strong> کند.</p>
                                    <p></p>
                                    <h4>بیمه خود را یک هفته زودتر تمدید کنید!</h4>
                                    <p></p>
                                    <p>با توجه به موضوعاتی که گفتیم، بهتر است تمدید بیمه ثالث را قبل از روز آخر
                                        اعتبار آن انجام دهید تا مشکلی برایتان ایجاد نشود.</p>
                                    <p>بهترین زمان تمدید بیمه نامه شخص ثالث، <strong>چند روز یا یک هفته
                                            قبل </strong>از تاریخ تمام شدن اعتبار آن است.</p>
                                    <p>البته حتی اگر بیمه نامه خود را چند روز زودتر تمدید کنید، بیمه جدیدتان از
                                        روزی که اعتبار بیمه قبلی تمام می شود، فعال خواهد شد.</p>
                                    <p></p>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- end of list-article -->  
        </div>
    </section>
<!-- end post-text -->

<?php

 include('layout/footer.php');

 ?>

</body>

</html>