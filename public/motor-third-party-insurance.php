<?php 

 include('layout-page/header.php');

?>

            <!-- this-filter session -->
    <div class="content">

  <section>
    <div class="search-section">
        <div class="aboutus">
            <span>بی می تو </span>
                 مقایسه، مشاوره و خرید آنلاین بیمه  
        </div>          
        <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-filter" >
                        <li class="nav-item lastitem">
                            <a class="nav-link " data-toggle="tab" href="#part2">
                                <i class="fas fa-user-shield icon fa-1x">
                                </i>
                                <span>  بیمه موتورسیکلت  </span>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
    <div class="tab-pane active fade" id="part2">
        <form id="wizard2" class="wizard slider-form">          
            <div class="wizard-content">
                <div id="step1" class="wizard-step">
                    <div class="col-sm-10 col-xs-12  content-step">
                                                    <div class="col-lg-4 col-md-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> مدل  موتورسیکلت  : </label>
                                                             <select class="slider-input form-control " id="">                                       
                                                                    <option value=""> انتخاب  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> سال ساخت : </label>
                                                            <select class="slider-input form-control " id="">
                                                              
                                                                   <option value=""> انتخاب  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> بیمه گر قبلی  :</label>
                                                            <select class="slider-input form-control " id="">
                                                              
                                                                   <option value=""> کاربری  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                    </div>
                    <div class="col-sm-2 col-xs-12  btns-step next-par">
                        <button type="button" class="wizard-btn wizard-next btn press-btn">
                            <i class="fas fa-caret-left"></i>
                            <span> بعد  </span>
                        </button>
                    </div>                                                  
                </div>
                <div id="step2" class="wizard-step">
                    <div class="col-sm-2 col-xs-12 btns-step prev-par">
                        <button type="button" class="wizard-btn wizard-prev btn press-btn">
                            <i class="fas fa-caret-right"></i>
                            <span> قبلی  </span>
                        </button>
                    </div>   
                    <div class="col-sm-8 col-xs-12 content-step">
                                                    <div class="col-md-6 col-sm-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label">  تاریخ شروع بیمه نامه قبلی  : </label>
                                                            <select class="slider-input form-control " id="">
                                                              
                                                                   <option value=""> انتخاب  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> تاریخ انقضای بیمه نامه قبلی  : </label>
                                                            <select class="slider-input form-control " id="">
                                                              
                                                                   <option value=""> کاربری  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                    </div>
                    <div class="col-sm-2 col-xs-12  btns-step next-par">
                        <button type="button" class="wizard-btn wizard-next btn press-btn">
                            <i class="fas fa-caret-left"></i>
                            <span> بعد  </span>
                        </button>
                    </div>   
                </div>
                <div id="step3" class="wizard-step">
                    <div class=" col-sm-2 col-xs-12  btns-step prev-par">
                        <button type="button" class="wizard-btn wizard-prev btn press-btn">
                            <i class="fas fa-caret-right"></i>
                            <span> قبلی  </span>
                        </button>
                    </div>   
                    <div class="col-sm-8 col-xs-12 content-step">
                                                    <div class="col-lg-4 col-md-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> درصد تخفیف ثالث   </label>
                                                             <select class="slider-input form-control  " id="">                                       
                                                                    <option value=""> رصد تخفیف ثالث   </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label">  درصد تخفیف حوادث راننده  </label>
                                                            <select class="slider-input form-control " id="">
                                                              
                                                                   <option value=""> درصد تخفیف حوادث راننده  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> خسارت از بیمه نامه  قبلی  </label>
                                                            <select class="slider-input form-control " id="">
                                                              
                                                                   <option value=""> خسارت از بیمه نامه  قبلی </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                    </div>
                    <div class=" col-sm-2 col-xs-12  btns-step next-par reg-par">
                        <button type="button" class="wizard-finish wizard-reg btn bimi-btns press-btn">
                            <span> مقایسه  </span>
                        </button>
                    </div>   
                </div>
            </div>
            <div class="wizard-header">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="wizard-step-indicator"><a href="#!">
                        <span class="badge slider-counter">1</span>
                    </a></li>
                    <li role="presentation" class="wizard-step-indicator"><a href="#!">
                        <span class="badge slider-counter">2</span>
                    </a></li>
                    <li role="presentation" class="wizard-step-indicator"><a href="#!">
                        <span class="badge slider-counter">3</span>
                    </a></li>
                </ul>
                <div class="slider-line"></div>
            </div> 
        </form>
    </div>
                    </div>
   
    </div>
 </section>
</div>
    <!-- content session -->
    <section id="third-party-insurance-content">
        <div class="container">

            <!-- explanatin tab -->
            <div class="row">
                <div class="col-md-12">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs justify-content-center">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#explain">

                                معرفی بیمه موتور سیکلت
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " data-toggle="tab" href="#question">
                                سوالات متداول
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#counciler">
                                درخواست مشاوره
                            </a>
                        </li>

                    </ul>
                    <!-- tab-content -->
                    <div class="tab-content shadow">
                        <!-- explanatin tab -->
                        <div class="tab-pane container active" id="explain">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="third-title">
                                            <h4> بیمه موتور سیکلت چیست؟</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                خرید بیمه شخص ثالث برای موتورسیکلت‌ها نیز مانند اتومبیل‌ها اجباری است.
                                                با این وجود متقاضیان خرید این بیمه در میان موتورسواران نسبت به صاحبان
                                                اتومبیل‌ها کمتر بوده و موتورسیکلت‌های زیادی در سطح شهر بدون خریداری این
                                                بیمه در حال تردد هستند. بر اساس آمارهای منتشر شده حدود ۹۰٪
                                                موتورسیکلت‌های کشور بیمه شخص ثالث ندارند و این به معنای احتمال وقوع
                                                میلیون‌ها خسارت جبران ناپذیر است. به نظر می‌رسد موتورسواران آشنایی
                                                چندانی با مزایای بیمه شخص ثالث موتورسیکلت ندارند و از خطرهایی که بدون
                                                بیمه موتورسیکلت آنها را تهدید می‌کند نیز مطلع نیستند.
                                            </p>
                                            <p class="paragraph">
                                                به طور کلی در بیمه موتورسیکلت خسارت‌های به وجود آمده در یک حادثه، توسط
                                                شرکت بیمه جبران می‌شود. در این بیمه‌نامه خسارات جانی و مالی که راننده‌
                                                مقصر موتورسیکلت به شخص زیان‌دیده یا وسیله نقلیه او وارد کرده است تا سقف
                                                تعهدات بیمه پرداخت خواهد شد. همچنین خسارات جانی وارد شده به راننده
                                                موتورسیکلت که شامل هزینه‌های درمان، نقص عضو و فوت وی باشد هم تحت پوشش
                                                بیمه قرار می‌گیرد.
                                            </p>
                                            <div class="paragraph">

                                                با خرید بیمه موتور سیکلت تمام خسارت های وارد شده به اشخاص ثالث تحت پوشش
                                                قرار می گیرد. این خسارت ها عبارتند از:

                                                <ul>
                                                    <li>
                                                        غرامت فوت و نقص عضو
                                                    </li>
                                                    <li>
                                                        هزینه های پزشکی و درمان
                                                    </li>
                                                    <li>
                                                        هزینه خسارت های وارد شده به وسیله نقلیه و یا محمولات آن
                                                    </li>
                                                </ul>

                                            </div>
                                            <p class="paragraph">
                                                به عبارت دیگر در رابطه با اشخاص ثالث زیان‌دیده، تمام هزینه‌های مالی و
                                                جانی به هر نحوی که باشد تا سقف معین شده در بیمه‌نامه موتورسیکلت تحت پوشش
                                                خواهد بود.
                                            </p>
                                            <div class="paragraph">
                                                همچنین خسارت های وارد شده به راننده مقصر موتور سیکلت هم تحت پوشش بیمه
                                                نامه حوادث راننده قرار دارد؛ این خسارت ها عبارتند از:

                                                <ul>
                                                    <li>
                                                        غرامت فوت و نقص عضو
                                                    </li>
                                                    <li>
                                                        هزینه های پزشکی و درمان
                                                    </li>
                                                </ul>

                                            </div>
                                            <p class="paragraph">
                                                البته بیمه موتور سیکلت در مورد خسارت‌های مالی وارد شده به موتورسیکلت
                                                راننده مقصر، هیچ‌گونه تعهدی ندارد و این خسارت‌ها را می‌توان تحت پوشش
                                                بیمه بدنه قرار داد.
                                            </p>
                                            <div class="paragraph">
                                                سقف پوشش هزینه های مالی و جانی بیمه موتور سیکلت

                                                <ul>
                                                    <li>
                                                        سقف هزینه های مالی تا سقف پوشش انتخاب شده در زمان خرید بیمه نامه
                                                        (در سال ۹۸ بین ۹ تا ۳۰ میلیون تومان) است.
                                                    </li>
                                                    <li>
                                                        هزینه های جانی اشخاص ثالث هم در بیمه موتور سیکلت در سال ۹۸ تا
                                                        سقف مبلغ ۳۶۰ میلیون تومان (معادل دیه کامل مسلمان در ماه حرام)
                                                        پرداخت می شود.
                                                    </li>
                                                    <li>
                                                        هزینه های جانی راننده مقصر در سال ۹۸ تا سقف مبلغ ۲۷۰ میلیون
                                                        تومان (معادل دیه کامل مسلمان در ماه عادی) پرداخت می شود.
                                                    </li>
                                                </ul>

                                            </div>
                                        </div>
                                        <div class="third-title">
                                            <h4>نرخ و قیمت بیمه موتور سیکلت</h4>
                                        </div>
                                        <div class="third-content">
                                            <div class="paragraph">
                                                نرخ پایه بیمه موتور سیکلت بر اساس سیلندر موتورها محاسبه می شود.
                                                موتورسیکلت ها به چهار دسته کلی زیر تقسیم می شوند:

                                                <ul>
                                                    <li>
                                                        موتورسیکلت گازی
                                                    </li>
                                                    <li>
                                                        موتورسیکلت دنده ای یک سیلندر
                                                    </li>
                                                    <li>
                                                        موتورسیکلت دو سیلندر و بالاتر
                                                    </li>
                                                    <li>
                                                        موتورسیکلت دارای سه چرخ و سایدکار
                                                    </li>
                                                </ul>

                                            </div>
                                            <p class="paragraph">
                                                به این ترتیب قیمت بیمه موتور سیکلت در هر دسته نسبت به دسته قبلی خود
                                                بالاتر خواهد بود. همچنین نرخ پوشش مالی انتخابی توسط خریدار (در سال ۹۸
                                                بین ۹ تا ۳۰ میلیون تومان) هم باعث افزایش حق بیمه می‌شود.
                                            </p>
                                            <p class="paragraph">
                                                علاوه بر نرخ پایه و پوشش مالی انتخابی، تخفیف عدم خسارت و جریمه دیرکرد هم
                                                بر روی نرخ حق ‌بیمه موتور سیکلت تاثیر می‌گذارد. به این شکل که تخفیف عدم
                                                خسارت باعث کاهش قیمت بیمه موتور سیکلت می‌شود و جریمه دیرکرد باعث افزایش
                                                آن.
                                            </p>
                                        </div>
                                        <div class="third-title">
                                            <h4>بیمه موتور سیکلت اقساطی</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                بهتر است بدانید که بیمه موتور سیکلت نسبت به بیمه‌های دیگر قیمت چندان
                                                بالایی ندارد، به خصوص اگر شما تخفیف عدم خسارت داشته باشید. با توجه به
                                                این موضوع معمولا شرکت‌های بیمه تمایل زیادی برای ارائه بیمه موتور سیکلت
                                                اقساطی ندارند و فروش این بیمه را به صورت نقدی انجام می‌دهند. </p>
                                        </div>
                                        <div class="third-title">
                                            <h4>ارزان ترین بیمه موتور سیکلت</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                با توجه به عوامل تأثیرگذار بر قیمت بیمه موتور سیکلت که در مطالب بالا آن
                                                را بررسی کردیم، اگر به دنبال ارزان ترین بیمه موتور سیکلت هستید، باید
                                                تخفیف عدم خسارت بیشتر و جریمه دیرکرد کمتری داشته باشید. </p>
                                            <p class="paragraph">
                                                همچنین برای انتخاب و خرید ارزان ترین بیمه موتور سیکلت، به بررسی و مقایسه
                                                این بیمه در شرکت‌های مختلف بیمه نیاز خواهید داشت که در حال حاضر این
                                                امکان از طریق سایت بی می تو فراهم شده است.
                                            </p>
                                            <p class="paragraph">
                                                در بی می تو شما می‌توانید با وارد کردن مشخصات موتور سیکلت خود، قیمت بیمه
                                                نامه را در شرکت‌های معتبر بیمه مقایسه کنید و ارزان ترین بیمه را انتخاب
                                                کنید.
                                            </p>

                                        </div>
                                        <div class="third-title">
                                            <h4>انتقال بیمه موتورسیکلت</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                بیمه موتورسیکلت نیز مثل بقیه بیمه های شخص ثالث قابلیت انتقال به صاحب
                                                جدید موتورسیکلت را خواهد داشت. برای این کار کافیست فروشنده و خریدار
                                                موتورسیکلت به نمایندگی صادرکننده بیمه مراجعه کنند. همچنین عدم انتقال
                                                رسمی بیمه نامه نیز از نظر قانونی منعی نخواهد داشت. چرا که صاحب بیمه نامه
                                                از نظر قانونی همان مالک وسیله نقلیه است.
                                                <br>
                                                در ضمن اگر مالک قبلی بیمه نامه قصد داشته باشد بیمه نامه را برای خود نگه
                                                دارد و از تخفیف های آن استفاده کند، می تواند با مراجعه به نمایندگی،
                                                تخفیف بیمه نامه را به موتورسیکلت جدید خود منتقل نماید. همچنین انتقال
                                                بیمه نامه به بستگان درجه اول هم امکان پذیر است.
                                            </p>
                                        </div>

                                        <div class="third-title">
                                            <h4>بهترین بیمه موتور سیکلت</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                اگر قصد خرید بهترین بیمه موتور سیکلت را دارید، به بررسی و مقایسه شرایط
                                                این بیمه در شرکت‌های مختلف نیاز خواهید داشت. برای این منظور شما
                                                می‌توانید از طریق سایت بی می تو قیمت و شرایط بیمه‌های موتور سیکلت را
                                                مقایسه کنید و پس از دریافت مشاوره از متخصصان بی می تو، بهترین بیمه موتور
                                                سیکلت را خریداری کنید.
                                            </p>
                                            <p class="paragraph">
                                                مشاوران بی می تو برای ارائه مشاوره دقیق و بی‌طرفانه در زمینه انتخاب
                                                بهترین بیمه موتور سیکلت همراه شما خواهند بود.
                                            </p>

                                        </div>
                                        <div class="third-title">
                                            <h4>استعلام قیمت بیمه موتور سیکلت</h4>
                                        </div>
                                        <div class="third-content">
                                            <p class="paragraph">
                                                برای استعلام قیمت بیمه موتور سیکلت دو روش پیش روی شماست. در روش اول که
                                                روش سنتی است شما باید به صورت حضوری به یکی از دفاتر بیمه مراجعه کنید و
                                                بدون مقایسه و آگاهی دقیق از شرایط بیمه‌های موتور سیکلت، بیمه نامه خود را
                                                خریداری کنید. </p>
                                            <p class="paragraph">
                                                اما در روش دوم یعنی روش آنلاین، شما می‌توانید بدون نیاز به صرف زمان و
                                                انرژی زیاد، قیمت بیمه‌نامه موتور سیکلت خود را در شرکت‌های مختلف بیمه
                                                استعلام بگیرید و پس از مقایسه و دریافت مشاوره، برای خرید بیمه مورد نظر
                                                خود اقدام کنید. </p>
                                            <p class="paragraph">
                                                استعلام آنلاین بیمه موتور سیکلت از سایت بی می تو، روش بسیار سریع و ساده
                                                خواهد بود و خرید بیمه تنها با چند کلیک انجام خواهد شد
                                            </p>
                                        </div>

                                        <div class="third-title">
                                            <h4>خرید اینترنتی بیمه موتور سیکلت از سایت بی می تو</h4>
                                        </div>
                                        <div class="third-content">
                                            <div class="paragraph">
                                                برای خرید بیمه شخص ثالث از سایت بی می تو ۴ مرحله پیش رو دارید:

                                                <ul>
                                                    <li>
                                                        مشخصات موتور سیکلت خود را در فیلدهای مشخص شده وارد کنید. مشخصاتی
                                                        مثل: مدل موتور، سال ساخت، سابقه عدم خسارت، تاریخ سررسید بیمه
                                                        نامه قبلی و...
                                                    </li>
                                                    <li>
                                                        در مرحله بعد می توانید نرخ و شرایط بیمه موتور سیکلت خود را در
                                                        برترین شرکت های بیمه کشور مشاهده و با هم مقایسه کنید و بر اساس
                                                        نیازها و اولویت های خود یکی از بیمه ها را انتخاب کنید.
                                                    </li>
                                                    <li>
                                                        پس از تأیید اطلاعات در مرحله بعد باید اطلاعات شخصی مثل نام و نام
                                                        خانوادگی، کد ملی، آدرس و ... را وارد کنید و سپس عکس پشت و روی
                                                        کارت موتور و بیمه نامه قبلی خود را ارسال کنید.
                                                    </li>
                                                    <li>
                                                        در مرحله نهایی می توانید نحوه پرداخت حق بیمه (پرداخت آنلاین یا
                                                        پرداخت در محل) را انتخاب کرده و هزینه بیمه نامه خود را به یکی از
                                                        این دو روش پرداخت کنید.
                                                    </li>
                                                    <li>
                                                        بیمه نامه شما در کمترین زمان ممکن صادر شده و به صورت رایگان به
                                                        آدرس مورد نظرتان ارسال خواهد شد.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end explanatin tab -->

                        <!-- question tab-->
                        <div class="tab-pane container fade" id="question">
                            <div class="accordion" id="accordionExample">
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link" type="button" data-toggle="collapse"
                                                    data-target="#question1" aria-expanded="true"
                                                    aria-controls="question1">
                                                چرا باید بیمه موتورسیکلت داشته باشیم؟
                                            </button>
                                        </h2>
                                    </div>

                                    <div id="question1" class="collapse show" aria-labelledby="headingOne"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph"><p>تصور کنید شما در هنگام رانندگی با موتور سیکلت به شخص یا اشخاصی خسارت وارد کنید، و در آن حادثه مقصر شناخته شوید، طبق قانون شما مسئول جبران این خسارت به زیان&zwnj;دیدگان هستید. ممکن است این خسارت مالی و یا خسارت بدنی باشد. بنابراین نیاز به پشتوانه&zwnj;ای دارید تا در هنگام بروز این حوادث در جبران خسارت در کنار شما باشد. <br>بیمه موتورسیکلت در واقع یک بیمه مسئولیت است (بیمه مسئولیت مدنی دارندگان وسایل نقلیه موتوری زمینی) که زیر گروه بیمه&zwnj;های اموال قرار دارد.<br> بر اساس قانونمالک خودرو یا کسی که خودرو در اختیار اوست باید اقدام به تهیه بیمه موتورسیکلت نماید. (در این صورت این شخص بیمه&zwnj;گذار نامیده میشود) </p> </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingTwo">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question2" aria-expanded="false"
                                                    aria-controls="question2">
                                                منظور از خسارت بدنی در بیمه موتورسیکلت چیست؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question2" class="collapse" aria-labelledby="headingTwo"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph"><p> به هر نوع دیه یا ارش (دیه جراحت) که درصورت بروز آسیب، شکستگی، نقص عضو و ازکارافتادگی (جزئی یا کلی ـ موقت یا دائم) یا فوت شخص ثالث به دلیل حوادث مشمول موضوع قانون بیمه اجباری شخص ثالث است، گفته می&zwnj;شود؛ بیمه شخص ثالث موتورسیکلت تمامی هزینه&zwnj;های درمانی و دیه را در صورتی که مشمول قانون دیگری نباشد، جبران خواهد کرد. </p> </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingThree">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question3" aria-expanded="false"
                                                    aria-controls="question3">
                                                کدام حوادث مشمول قوانین بیمه موتورسیکلت است؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question3" class="collapse" aria-labelledby="headingThree"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                حوادث شامل هرسانحه‌ای از قبیل تصادم، تصادف، سقوط، واژگونی، آتش سوزی و یا انفجار یا هر نوع سانحه است که مطابق با شرایط بیمه‌نامه باشد و موتورسیکلت باعث به وجود آمدن این حوادث غیرمترقبه شده باشد.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingFour">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question4" aria-expanded="false"
                                                    aria-controls="question4">

                                                وظیفه بیمه‌گر هنگام تصادف ناشی از تخلف رانندگی چیست؟                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question4" class="collapse" aria-labelledby="headingFour"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                بیمه‌گر ملزم به جبران خسارت‌های وارد شده به اشخاص ثالث تا سقف تعهدات مندرج در بیمه‌نامه خواهد بود. در حوادث رانندگی منجر به جرح یا فوت که به استناد گزارش کارشناس تصادفات راهنمایی و رانندگی یا پلیس‌راه علت اصلی وقوع تصادف یکی از تخلفات رانندگی حادثه‌‌ساز باشد، بیمه‌گر موظف است خسارت زیان‌دیده را بدون هیچ شرطی پرداخت کند و پس از آن می‌تواند جهت بازیافت یک درصد از خسارت‌های بدنی و دو درصد از خسارت‌های مالی پرداخت شده به مسبب حادثه مراجعه کند؛ علاوه بر آن در این صورت گواهی‌نامه راننده مسبب حادثه از یک تا سه ماه توقیف می‌شود و رانندگی در این مدت ممنوع و در حکم رانندگی بدون گواهی‌نامه است.                                                </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingFive">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question5" aria-expanded="false"
                                                    aria-controls="question5">
                                                وظیفه بیمه‌گر هنگام طبیعی نبودن تصادف چیست؟                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question5" class="collapse" aria-labelledby="headingFive"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                در صورت اثبات عمد راننده در ایجاد حادثه توسط مراجع قضایی و یا رانندگی در حالت مستی یا استعمال مواد مخدر یا روانگردان مؤثر در وقوع حادثه، یا در صورتی که راننده مسبب فاقد گواهی‌نامه رانندگی باشد یا گواهی‌نامه او متناسب با نوع وسیله نقلیه نباشد شرکت بیمه موظف است بدون اخذ تضمین، خسارت زیان‌دیده را پرداخت کرده و پس از آن می‌تواند به نیابت از زیان‌دیده از طریق مراجع قانونی برای استرداد تمام یا بخشی از وجوه پرداخت شده به شخصی که موجب خسارت شده است مراجعه کند.                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingSix">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question6" aria-expanded="false"
                                                    aria-controls="question6">

                                                چه خسارت‌هایی که تحت پوشش بیمه موتورسیکلت نیستند؟                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question6" class="collapse" aria-labelledby="headingSix"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph"><p> </p><ul> <li>خسارت وارد شده به موتورسیکلت وقتی مسبب حادثه باشد (راننده مقصر)</li> <li>خسارت مستقیم و یا غیرمستقیم ناشی از تشعشعات اتمی و رادیواکتیو</li> <li>خسارت ناشی از محکومیت جزایی و یا پرداخت جرائم</li> </ul> <p></p> </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingSeven">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question7" aria-expanded="false"
                                                    aria-controls="question7">

                                                ارائه خدمات به وسایل نقلیه فاقد بیمه‌نامه چگونه است؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question7" class="collapse" aria-labelledby="headingSeven"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph"><p> مطابق قانون، ارائه هرگونه خدمات به دارندگان وسایل نقلیه موتوری زمینی فاقد بیمه&zwnj;نامه شخص ثالث معتبر، توسط راهنمایی و رانندگی، دفاتر اسناد رسمی و سازمان&zwnj;ها و نهادهای مرتبط با امر حمل و نقل ممنوع است.<br> دفاتر اسناد رسمی مکلفند هنگام تنظیم هرگونه سند در مورد موتورسیکلت، مشخصات بیمه&zwnj;نامه شخص ثالث آن را در اسناد تنظیمی درج نمایند. </p> </div>                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingEight">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question8" aria-expanded="false"
                                                    aria-controls="question8">

                                                عواقب نداشتن و یا دیرکرد تمدید بیمه موتورسیکلت چیست؟                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question8" class="collapse" aria-labelledby="headingEight"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph"><p> در صورت دیرکرد تمدید بیمه موتورسیکلت، علاوه بر عواقب مالی، مشکلات دیگری نیز وجود خواهد داشت که موجب صرف زمان زیادی می شود. از عواقب نداشتن بیمه موتورسیکلت می توان به موارد زیر اشاره کرد:<br><br> در صورت شناسایی موتور فاقد بیمه موتورسیکلت توسط پلیس راهنمایی و رانندگی، موتور متوقف شده و به پارکینگ منتقل می&zwnj;شود. در این حالت شخص خاطی برای ترخیص موتورسیکلت، باید بیمه&zwnj;&zwnj;نامه شخص ثالث موتورسیکلت معتبر ارائه نماید و باید علاوه بر حق بیمه جدید، حق بیمه روزهایی را هم که وسیله نقلیه او بیمه نداشته (تا حداکثر یک سال) را بپردازد، و همچنین علاوه بر پرداخت جریمه دیرکرد، هزینه پارکینگ را نیز بپردازد.<br> در صورت نداشتن بیمه موتورسیکلت در حوادث، اگر مالک موتور مقصر باشد، هیچ شرکت بیمه&zwnj;ای خسارت را پرداخت نمی&zwnj;کند، باید کلیه هزینه&zwnj;ها را شخصا بپردازد. <br> طبق قوانین جدید بیمه شخص ثالث، در شرایطی که راننده مقصر بیمه شخص ثالث موتورسیکلت نداشته باشد و در صورت بروز حوادث منجر به فوت یا خسارات بدنی، جبران این گونه خسارت&zwnj;ها به عهده صندوق تامین خسارت&zwnj;های بدنی است؛ و پرداخت دیه را برای راننده مقصر حادثه به صورت قسطی خواهد بود. این قانون برای حمایت از اشخاص ثالث زیان&zwnj;دیده است. امکان خرید و فروش موتوری که تحت پوشش بیمه موتورسیکلت نیست وجود ندارد. <br> اگر موتور متعلق به دوست من باشد و بیمه داشته باشد و من راننده آن باشم و تصادف کنم آیا بیمه خسارت زیان&zwnj;دیده را پرداخت می&zwnj;کند؟ بله؛ بیمه موتورسیکلت وابسته به موتور است و در صورتی که شما گواهی&zwnj;نامه معتبر متناسب با وسیله نقلیه (گواهی&zwnj;نامه موتورسیکلت) داشته باشید، حتی اگر موتور متعلق به شما نباشد، بیمه خسارت وارد شده را جبران خواهد نمود. </p> </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingNine">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question9" aria-expanded="false"
                                                    aria-controls="question9">

                                                شخص ثالث در بیمه موتورسیکلت به چه کسانی گفته می‌شود؟                                            </button>

                                        </h2>
                                    </div>
                                    <div id="question9" class="collapse" aria-labelledby="headingNine"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph"><p> به همه کسانی (اشخاص حقیقی یا حقوقی) که توسط موتورسیکلت دچار زیان مالی و یا بدنی شوند به جز خود راننده&zwnj;ی مقصر حادثه، شخص ثالث گفته می&zwnj;شود.<br> اشخاص ثالث شامل جنین داخل رحم و همچنین سرنشین&zwnj; موتور نیز می&zwnj;شود حتی اگر بیشتر از تعداد نفرات مجاز درج شده بر روی کارت موتورسیکلت باشد. </p> </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading11">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question11" aria-expanded="false"
                                                    aria-controls="question11">
                                                اگر در یک حادثه به بیشتر از یک نفر زیان جانی برسد چگونه است؟ آیا بیمه دیه افراد دیگر را هم پوشش می‌دهد؟                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question11" class="collapse" aria-labelledby="heading11"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                بله؛ بر اساس قانون شرکت بیمه‌گر مکلف به پرداخت تمام دیات است.                                            </P>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading12">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question12" aria-expanded="false"
                                                    aria-controls="question12">
                                                آیا دیه زن و مرد، مسلمان و غیر مسلمان برابر است؟                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question12" class="collapse" aria-labelledby="heading12"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                بله؛ در قوانین جدید بیمه موتورسیکلت دیه زن و مرد و همچنین شخص مسلمان و غیر مسلمان (اهل کتاب) برابر است.                                            </P>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading13">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question13" aria-expanded="false"
                                                    aria-controls="question13">
                                                بیمه‌نامه شخص ثالث در حکم وثیقه است؟                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question13" class="collapse" aria-labelledby="heading13"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                دادگاه‌ها موظفند در حوادث رانندگی منجر به خسارت بدنی، بیمه‌نامه شخص ثالثی را که اصالت آن از سوی شرکت بیمه ذی‌ربط کتبا مورد تأیید قرار گرفته است تا میزان مندرج در بیمه‌نامه به عنوان وثیقه قبول کنند.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading14">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question14" aria-expanded="false"
                                                    aria-controls="question14">
                                                چرا هر سال حق بیمه موتورسیکلت تغییر می‌کند؟                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question14" class="collapse" aria-labelledby="heading14"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                حق بیمه موتورسیکلت و میزان پوشش مالی و بدنی آن بر اساس دیه کامل یک انسان در ماه حرام تعیین می‌گردد. این مبلغ در ابتدای هر سال با انتشار بخشنامه‌ای توسط قوه قضائیه اعلام می‌شود؛ همین موضوع باعث تغییر حق بیمه در هر سال است.                                            </P>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading15">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question15" aria-expanded="false"
                                                    aria-controls="question15">
                                                آیا حق بیمه موتورسیکلت مطابق با قیمت موتور تغییر می کند؟                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question15" class="collapse" aria-labelledby="heading15"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="paragraph"><p> خیر؛ در تعیین حق بیمه موتورسیکلت ارزش موتور تاثیری ندارد. ملاک&zwnj;های تعیین حق بیمه در بیمه&zwnj;نامه شخص ثالث عباتند از:<br> </p><li>حق بیمه پایه ابلاغ شده توسط بیمه مرکزی (تعداد سیلندر وسیله نقلیه) </li> <li>تخفیف عدم خسارت بیمه موتورسیکلت</li> <li>کاربری موتور</li> <li>جریمه دیرکرد بیمه موتورسیکلت</li> <li>میزان پوشش مالی مورد درخواست</li> <li>مدت اعتبار بیمه&zwnj;نامه</li>  <p></p> </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading16">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question16" aria-expanded="false"
                                                    aria-controls="question16">
                                                بیمه حوادث راننده که با بیمه موتورسیکلت صادر می شود، چیست؟                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question16" class="collapse" aria-labelledby="heading16"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                منظور از شخص ثالث، هر شخصی است که به سبب حوادث وسایل نقلیه موتوری دچار زیان‌های بدنی و مالی شود به استثنای راننده مسبب حادثه. شرکت بیمه، هنگام صدور بیمه‌نامه شخص ثالث، با دریافت حق بیمه حوادث راننده، خود راننده را هم تا حد دیه ماه حرام تحت پوشش بیمه حوادث قرار می‌دهد؛ بیمه حوادث راننده مسئولیت جبران خسارت‌های فوت و نقص عضو راننده مقصر را به عهده دارد و همچنین شامل پرداخت هزینه‌‌های درمان راننده است.

                                            </P>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading17">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question17" aria-expanded="false"
                                                    aria-controls="question17">
                                                حق بیمه حوادث راننده چقدر است؟
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question17" class="collapse" aria-labelledby="heading17"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                نرخ حق بیمه حوادث راننده در آیین‌نامه ابلاغ شده به شرکت‌های بیمه (بر اساس دیه در ماه عادی) تعیین شده است؛ حداکثر حق بیمه پوشش حوادث راننده در هر سال هنگام صدور بیمه نامه شخص ثالث در بیمه‌نامه درج و به حق بیمه اضافه می‌شود. این حق بیمه با توجه به نوع کاربری تغییر میابد و برای گروه موتور سیکلت ۸۸۴,۷۰۰ ریال است.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading18">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question18" aria-expanded="false"
                                                    aria-controls="question18">
                                                اگر وسیله نقلیه بیمه‌نامه شخص ثالث نداشته باشد و تصادف هم نکند، مشکل چیست؟                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question18" class="collapse" aria-labelledby="heading18"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                داشتن بیمه نامه شخص ثالث برای همه وسایل نقلیه موتوری (موتور سیلکت و انواع خودرو) اجباری است؛ حرکت وسایل نقلیه موتوری بدون داشتن بیمه نامه شخص ثالث ممنوع است. مأموران راهنمایی و رانندگی و پلیس راه موتورسیکلت‌های بدون بیمه‌نامه را متوقف و راننده متخلف را ملزم به پرداخت جریمه می‌کنند.                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading19">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question19" aria-expanded="false"
                                                    aria-controls="question19">
                                                در صورت دریافت خسارت از شرکت بیمه، بیمه‌نامه سال بعد به چه شکل تمدید می‌شود؟                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question19" class="collapse" aria-labelledby="heading19"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                بر روی بیمه نامه شخص ثالث آیتمی تحت عنوان تخفیف عدم خسارت (به اختصار ت.ع.خ) وجود دارد. موتوری که در طول مدت بیمه‌نامه خود خسارتی دریافت نکرده باشد، در زمان تمدید خود شامل تخفیف تشویقی از سمت شرکت بیمه‌گر می‌شود. در صورتی که اگر موتوری تصادف کند و از کوپن‌های بیمه‌نامه خود استفاده کند، تخفیف‌های عدم خسارت او به صورت پلکانی کاهش میابند.                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading20">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question20" aria-expanded="false"
                                                    aria-controls="question20">
                                                آیا می‌توان هنگام فروش وسیله نقلیه، تخفیف‌های بیمه موتورسیکلت را به موتور جدید انتقال داد؟                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question20" class="collapse" aria-labelledby="heading20"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                بله؛ در صورتی که تقاضای بیمه‌گذار قبل از تاریخ انتقال بیمه‌نامه باشد، امکان انتقال تخفیف‌ها به موتور جدید وجود دارد.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading21">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question21" aria-expanded="false"
                                                    aria-controls="question21">
                                                آیا در بیمه‌نامه شخص ثالث موتورسیکلت هزینه‌های درمانی نیز پرداخت می‌شود؟                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question21" class="collapse" aria-labelledby="heading21"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                بر اساس مصوبه مجلس شورای اسلامی، وزارت بهداشت، درمان و آموزش پزشکی مکلف است مصدومان ناشی از حوادث رانندگی را بدون دریافت وجه پذیرش کند. برای این منظور شرکت‌های بیمه از محل بیمه‌نامه شخص ثالث ۱۰ درصد عوارض به وزارت بهداشت می‌پردازند.                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading22">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question22" aria-expanded="false"
                                                    aria-controls="question22">
                                                اگر راننده مقصر حادثه گواهی‌نامه رانندگی نداشته باشد آیا به راننده دیه تعلق می‌گیرد؟                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question22" class="collapse" aria-labelledby="heading22"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                با توجه به این که در مدارک لازم برای پرداخت خسارت داشتن گواهی‌نامه رانندگی متناسب با نوع کاربری اتومبیل قید شده است امکان پرداخت خسارت وجود ندارد.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading23">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question23" aria-expanded="false"
                                                    aria-controls="question23">
                                                ماه‌های حرام كدامند؟                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question23" class="collapse" aria-labelledby="heading23"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                چهار ماه رجب، ذی القعده، ذی الحجه و محرم ماه‌های حرام هستند. در این ماه‌ها دیه افزایش می‌یابد.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="heading24">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#question24" aria-expanded="false"
                                                    aria-controls="question24">
                                                تفاوت ماه‌های حرام با ماه‌های عادی چيست؟                                            </button>
                                        </h2>
                                    </div>
                                    <div id="question24" class="collapse" aria-labelledby="heading24"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p class="paragraph">
                                                چنانچه حادثه رانندگی و فوت زيان‌ديده هر دو در ماه‌های حرام اتفاق افتد، بر اساس قانون مجازات اسلامی، ميزان ديه يک سوم بيشتر از ماه‌های عادی خواهد بود.                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- end question tab-->

                        <!-- counciler tab-->
                        <div class="tab-pane container fade" id="counciler">


                            <section class="form-elegant">

                                <div class="third-title">
                                    <h4>درخواست مشاوره</h4>
                                </div>
                                <div class="row">
                                    <div class="col-md-10 col-md-offset-1">


                                        <form>
                                            <div class="form-group col-md-8">
                                                <label for="nameInput">نام و نام خانوادگی</label>
                                                <input type="text" class="form-control none-border" id="nameInput"
                                                       placeholder="">
                                            </div>

                                            <div class="form-group col-md-8">
                                                <label for="phoneInput">شماره تماس </label>
                                                <input type="text" class="form-control none-border" id="phoneInput"
                                                       placeholder="">
                                            </div>
                                            <div class="form-group col-md-8">
                                                <label for="description">متن درخواست مشاوره</label>
                                                <textarea id="description" class="form-control none-border"
                                                          style="float: right;">

                                            </textarea>
                                            </div>

                                            <div class="col-md-8  btn ">
                                                <div class="compare-btn btn ">
                                                    <a href="#">ارسال درخواست</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </section>

                        </div>
                        <!--  end counciler tab -->
                    </div>
                    <!--  end tab-content  -->
                </div>
            </div>
        </div>
        </div>
        </div>
        </div>
    </section>
    <!-- end  content session -->

<?php

 include('layout-page/footer.php');

 ?>


 <script type="text/javascript">

        $(".top-nav-menu .nav-item").removeClass("active");
        $(".top-nav-menu #m2").addClass("active");

</script>

</body>

</html>
