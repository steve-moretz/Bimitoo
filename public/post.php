<?php 

 include('layout/header.php');

?>


<!-- post  -->
<div class="content">
    <section class="container" id="blog">
        <!-- header-article -->
        <div class="row">
            <div class="col-md-12">
                <div>
                    <div class="col-md-12 shadow img-box" >
                        <div class="col-md-offset-1 col-md-5 ">
                            <div class="col-md-12 ">
                                <div class="row" style="margin-bottom: 30px;">
                                    <div class="col-md-6">
                                        <div class="bd-example">
                                            <div id="carouselExampleCaptions" class="carousel slide"
                                                 data-ride="carousel">
                                                <div class="carousel-inner">
                                                    <div class="carousel-item active">
                                                        <a href="#">
                                                            <img src="/images/kids-at-eye-doctor-overheard-optometry.jpg"
                                                                 class="d-block w-100" alt="...">
                                                            <div class="carousel-caption d-none d-md-block blog-img-caption">
                                                                <p>آیا بیمه تکمیلی هزینه عمل چشم را پرداخت می‌کند؟</p>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="bd-example">
                                            <div id="carouselExampleCaptions" class="carousel slide"
                                                 data-ride="carousel">
                                                <div class="carousel-inner">
                                                    <div class="carousel-item active">
                                                        <a href="#">
                                                            <img src="/images/kids-at-eye-doctor-overheard-optometry.jpg"
                                                                 class="d-block w-100" alt="...">
                                                            <div class="carousel-caption d-none d-md-block blog-img-caption">
                                                                <p>آیا بیمه تکمیلی هزینه عمل چشم را پرداخت می‌کند؟</p>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="bd-example">
                                            <div id="carouselExampleCaptions" class="carousel slide"
                                                 data-ride="carousel">
                                                <div class="carousel-inner">
                                                    <div class="carousel-item active">
                                                        <a href="#">
                                                            <img src="/images/kids-at-eye-doctor-overheard-optometry.jpg"
                                                                 class="d-block w-100" alt="...">
                                                            <div class="carousel-caption d-none d-md-block blog-img-caption">
                                                                <p>آیا بیمه تکمیلی هزینه عمل چشم را پرداخت می‌کند؟</p>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="bd-example">
                                            <div id="carouselExampleCaptions" class="carousel slide"
                                                 data-ride="carousel">
                                                <div class="carousel-inner">
                                                    <div class="carousel-item active">
                                                        <a href="#">
                                                            <img src="/images/kids-at-eye-doctor-overheard-optometry.jpg"
                                                                 class="d-block w-100" alt="...">
                                                            <div class="carousel-caption d-none d-md-block blog-img-caption">
                                                                <p>آیا بیمه تکمیلی هزینه عمل چشم را پرداخت می‌کند؟</p>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=" col-md-5 col-md-offset-1 " style="margin-right: 5px">
                            <div class="bd-example">
                                <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <a href="#">
                                                <img src="/images/kids-at-eye-doctor-overheard-optometry.jpg"
                                                     class="d-block w-100" alt="...">
                                                <div class="carousel-caption d-none d-md-block blog-img-caption">
                                                    <p>آیا بیمه تکمیلی هزینه عمل چشم را پرداخت می‌کند؟</p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end header-article -->
        <div class="row blog-wrapper" style="margin-top: 50px">

        <?php 

            include('layout/sidebar.php');

        ?>

             <!-- list-article -->            
             <div class="col-md-8 col-sm-12 col-xs-12">
                <div class="shadow">
                    <div class="article ">
                        <div class="card mb-3">
                            <div class="blog-title">
                                <h2 class="card-title">بهترین بیمه شخص ثالث؟ ارزان ترین و گران ترین بیمه شخص ثالث</h2>
                            </div>
                            <div class="row no-gutters blog-content">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="/images/kids-at-eye-doctor-overheard-optometry.jpg" class="card-img"
                                             alt="...">
                                    </div>
                                    <div class="col-md-8">
                                        <div class="card-body">
                                            <p class="card-text">
                                                بیمه شخص ثالث بیمه ای اجباری است و رانندگان فاقد این بیمه‌نامه طبق قانون
                                                جریمه می‌شوند. درنتیجه به دلیل اجباری بودن و حق‌بیمه‌ی بالا، می‌تواند به
                                                دغدغه‌ی مالی برای رانندگان تبدیل شود. البته باید توجه داشت اجباری بودن
                                                این بیمه نباید باعث شود هنگام خرید دقت کافی نداشته باشیم. به طور مثال
                                                انتخاب شرکت بیمه‌ای... </p>
                                        </div>
                                        <div class="row col-md-12">
                                            <div class="col-md-12  btn ">
                                                <div class="compare-btn btn ">
                                                    <a href="{{route('post')}}"> ادامه مطلب</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="article ">
                        <div class="card mb-3">
                            <div class="blog-title">
                                <h2 class="card-title">بهترین بیمه شخص ثالث؟ ارزان ترین و گران ترین بیمه شخص ثالث</h2>
                            </div>
                            <div class="row no-gutters blog-content">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="/images/kids-at-eye-doctor-overheard-optometry.jpg" class="card-img"
                                             alt="...">
                                    </div>
                                    <div class="col-md-8">
                                        <div class="card-body">
                                            <p class="card-text">
                                                بیمه شخص ثالث بیمه ای اجباری است و رانندگان فاقد این بیمه‌نامه طبق قانون
                                                جریمه می‌شوند. درنتیجه به دلیل اجباری بودن و حق‌بیمه‌ی بالا، می‌تواند به
                                                دغدغه‌ی مالی برای رانندگان تبدیل شود. البته باید توجه داشت اجباری بودن
                                                این بیمه نباید باعث شود هنگام خرید دقت کافی نداشته باشیم. به طور مثال
                                                انتخاب شرکت بیمه‌ای... </p>
                                        </div>
                                        <div class="row col-md-12">
                                            <div class="col-md-12  btn ">
                                                <div class="compare-btn btn ">
                                                    <a href="{{route('post')}}"> ادامه مطلب</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="article ">
                        <div class="card mb-3">
                            <div class="blog-title">
                                <h2 class="card-title">بهترین بیمه شخص ثالث؟ ارزان ترین و گران ترین بیمه شخص ثالث</h2>
                            </div>
                            <div class="row no-gutters blog-content">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="/images/kids-at-eye-doctor-overheard-optometry.jpg" class="card-img"
                                             alt="...">
                                    </div>
                                    <div class="col-md-8">
                                        <div class="card-body">
                                            <p class="card-text">
                                                بیمه شخص ثالث بیمه ای اجباری است و رانندگان فاقد این بیمه‌نامه طبق قانون
                                                جریمه می‌شوند. درنتیجه به دلیل اجباری بودن و حق‌بیمه‌ی بالا، می‌تواند به
                                                دغدغه‌ی مالی برای رانندگان تبدیل شود. البته باید توجه داشت اجباری بودن
                                                این بیمه نباید باعث شود هنگام خرید دقت کافی نداشته باشیم. به طور مثال
                                                انتخاب شرکت بیمه‌ای... </p>
                                        </div>
                                        <div class="row col-md-12">
                                            <div class="col-md-12  btn ">
                                                <div class="compare-btn btn ">
                                                    <a href="{{route('post')}}"> ادامه مطلب</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="article ">
                        <div class="card mb-3">
                            <div class="blog-title">
                                <h2 class="card-title">بهترین بیمه شخص ثالث؟ ارزان ترین و گران ترین بیمه شخص ثالث</h2>
                            </div>
                            <div class="row no-gutters blog-content">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="/images/kids-at-eye-doctor-overheard-optometry.jpg" class="card-img"
                                             alt="...">
                                    </div>
                                    <div class="col-md-8">
                                        <div class="card-body">
                                            <p class="card-text">
                                                بیمه شخص ثالث بیمه ای اجباری است و رانندگان فاقد این بیمه‌نامه طبق قانون
                                                جریمه می‌شوند. درنتیجه به دلیل اجباری بودن و حق‌بیمه‌ی بالا، می‌تواند به
                                                دغدغه‌ی مالی برای رانندگان تبدیل شود. البته باید توجه داشت اجباری بودن
                                                این بیمه نباید باعث شود هنگام خرید دقت کافی نداشته باشیم. به طور مثال
                                                انتخاب شرکت بیمه‌ای... </p>
                                        </div>
                                        <div class="row col-md-12">
                                            <div class="col-md-12  btn ">
                                                <div class="compare-btn btn ">
                                                    <a href="{{route('post')}}"> ادامه مطلب</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           <!-- end list-article -->   
        </div>
    </section>
</div>
<!--end post  -->


<?php

 include('layout/footer.php');

 ?>

</body>

</html>