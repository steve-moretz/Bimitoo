<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     
    <!-- Bootstrap CSS Ver 4.3.1 -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-rtl.min.css">
    <link rel="stylesheet" href="css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="css/bootstrap-grid.min.css">
    
    <!-- Font awesome Css 5.8.2 -->
    <link rel="stylesheet" href="css/fontawesome.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    
    <!--  Main Style -->
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/style.css">
    <!-- login Style -->
    <link rel="stylesheet" href="css/login.css">
    <!-- insurance Style -->
    <link rel="stylesheet" href="css/insurance.css">
    <!-- content Style -->
    <link rel="stylesheet" href="css/kamadatepicker.css"/>
    <!-- blog Style -->
    <link rel="stylesheet" href="css/blog.css">

    <title> سایت بی می تو  </title>
    
</head>
<body>

<header>
    <nav class="navbar navbar-expand-md fixed-top top-nav">
        <div class="col-lg-2 col-md-1 col-sm-8 col-6 top-nav-brand">
            <a class="navbar-brand" href="#!">    
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                    <i class="fa fa-bars">  
                    </i>
                </button>   
            </a>        
            <a href="index.php" class="logo-img d-inline-block"><img src="images/bimito.png" alt="" style="width: 60px;height: 60px"></a>
            <a href="index.php" class="logo-txt d-lg-inline d-md-none d-sm-inline d-none"> Bimitoo.com </a>              
        </div>
        <div class="col-lg-8 col-md-9 top-nav-menu collapse navbar-collapse" id="collapsibleNavbar">
              <ul class="navbar-nav">
                <li class="nav-item text-center active" id="m1">                
                    <a class="bincleft nav-link" href="third-party-insurance.php">
                       <i class="fas fa-user-shield fa-1x"></i>
                         بیمه شخص ثالث 
                    </a>
                </li>
                <li class="nav-item text-center" id="m2">
                    <a class="bincleft nav-link " href="motor-third-party-insurance.php">
                     <i class="fas fa-motorcycle fa-1x"></i>
                    بیمه موتورسیکلت  
                    </a>
                </li>
                <li class="nav-item text-center " id="m3">
                    <a class="bincleft nav-link" href="auto-body-insurance.php">
                        <i class="fas fa-car fa-1x"></i>
                    بیمه بدنه
                    </a>
                </li>
                <li class="nav-item text-center" id="m4">
                    <a class="bincleft nav-link " href="life-insurance.php">
                        <i class="fas fa-fire fa-1x"></i>
                    بیمه عمر  
                    </a>
                </li>  
                <li class="nav-item text-center" id="m5">
                    <a class="bincleft nav-link" href="travel-insurance.php">
                       <i class="fas fa-suitcase-rolling fa-1x"></i>
                    بیمه مسافرتی  
                    </a>
                </li>
                <li class="nav-item text-center" id="m6">
                    <a class="bincleft nav-link " href="supplementary-health-insurance.php">
                        <i class="fas fa-clinic-medical fa-1x"></i>
                    بیمه تکمیلی 
                    </a>
                </li>
                <li class="nav-item text-center" id="m7">
                    <a class="bincleft nav-link " href="erth-insurance.php">
                         <i class="fas fa-fire fa-1x"></i>
                         بیمه زلزله 
                    </a>
                </li>
                <li class="nav-item text-center" id="m8">
                    <a class="bincleft nav-link " href="other-insurance.php">
                       <i class="fas fa-plus fa-1x"></i>
                        سایر بیمه ها 
                    </a>
                </li>
              </ul>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-4 col-6 top-nav-login">                                
                    <div class="btn-group ">  
                        <a href="login.php" class="phone zoom_anim "> <i class="fa fa-phone"></i> </a>                                     
                        <a href="login.php" type="button" class="btn btn-login log" >
                          ورود 
                        </a>
                        <a href="reg.php" type="button" class="btn btn-login reg" >
                        ثبت نام
                        </a>                       
                    </div>
        </div>
    </nav>
</header>