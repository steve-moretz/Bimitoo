
<!-- sidebar -->
<div class="col-md-4 col-sm-12 col-xs-12">

    <div class="shadow">
        <div class="card">
            <div class="card-header">
                خرید آنلاین بیمه
            </div>
            <div class="card-body">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item"><a href="">بیمه شخص ثالث</a></li>
                    <li class="list-group-item"><a href="">بیمه بدنه</a></li>
                    <li class="list-group-item"><a href="">بیمه موتورسیکلت</a></li>
                    <li class="list-group-item"><a href="">بیمه آتش سوزی</a></li>
                    <li class="list-group-item"><a href="">بیمه زلزله</a></li>
                    <li class="list-group-item"><a href="">بیمه مسافرتی</a></li>
                    <li class="list-group-item"><a href="">بیمه عمر</a></li>
                    <li class="list-group-item"><a href="">بیمه تکمیلی</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="shadow">
        <div class="card">
            <div class="card-header">
                خرید آنلاین از شرکت های بیمه
            </div>
            <ul class="list-group list-group-flush">
                <div class="accordion" id="accordionExample">
                    <div class="card sub-card">
                        <div class="card-header" id="iran">
                            <img src="images/iran-insurance.png" alt="" class="logo-company">
                            <h3 class="mb-0">

                                بیمه ایران
                            </h3>
                            <button class="btn btn-link" type="button" data-toggle="collapse"
                                    data-target="#collapseOne" aria-expanded="true"
                                    aria-controls="collapseOne">
                                <i class="fas fa-chevron-down "></i>
                            </button>
                        </div>

                        <div id="collapseOne" class="collapse " aria-labelledby="iran"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item"><a href=""> بیمه شخص ثالث ایران</a></li>
                                    <li class="list-group-item"><a href=""> بیمه بدنه ایران</a></li>
                                    <li class="list-group-item"><a href=""> بیمه موتورسیکلت ایران</a></li>
                                    <li class="list-group-item"><a href=""> بیمه آتش سوزی ایران</a></li>
                                    <li class="list-group-item"><a href=""> بیمه زلزله ایران</a></li>
                                    <li class="list-group-item"><a href=""> بیمه مسافرتی ایران</a></li>
                                    <li class="list-group-item"><a href=""> بیمه عمرایران</a></li>
                                    <li class="list-group-item"><a href=""> بیمه تکمیلی ایران</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card sub-card">
                        <div class="card-header" id="asia">
                            <img src="images/asia-insurance.png" alt="" class="logo-company">
                            <h3 class="mb-0">
                                بیمه آسیا
                            </h3>
                            <button class="btn btn-link" type="button" data-toggle="collapse"
                                    data-target="#collapseOne" aria-expanded="true"
                                    aria-controls="collapseOne">
                                <i class="fas fa-chevron-down"></i>
                            </button>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="asia"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item"> بیمه شخص ثالث آسیا</li>
                                    <li class="list-group-item"> بیمه بدنه آسیا</li>
                                    <li class="list-group-item">بیمه موتورسیکلت آسیا</li>
                                    <li class="list-group-item">بیمه آتش سوزی آسیا</li>
                                    <li class="list-group-item">بیمه زلزله آسیا</li>
                                    <li class="list-group-item">بیمه مسافرتی آسیا</li>
                                    <li class="list-group-item">بیمه عمر آسیا</li>
                                    <li class="list-group-item">بیمه تکمیلی آسیا</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card sub-card">
                        <div class="card-header" id="dena">
                            <img src="images/dana-insurance.png" alt="" class="logo-company">
                            <h3 class="mb-0">
                                بیمه دانا
                            </h3>
                            <button class="btn btn-link" type="button" data-toggle="collapse"
                                    data-target="#collapseOne" aria-expanded="true"
                                    aria-controls="collapseOne">
                                <i class="fas fa-chevron-down"></i>
                            </button>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="dena"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item"> بیمه شخص ثالث دانا</li>
                                    <li class="list-group-item"> بیمه بدنه دانا</li>
                                    <li class="list-group-item">بیمه موتورسیکلت دانا</li>
                                    <li class="list-group-item">بیمه آتش سوزی دانا</li>
                                    <li class="list-group-item">بیمه زلزله دانا</li>
                                    <li class="list-group-item">بیمه مسافرتی دانا</li>
                                    <li class="list-group-item">بیمه عمر دانا</li>
                                    <li class="list-group-item">بیمه تکمیلی دانا</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card sub-card">
                        <div class="card-header" id="alborz">
                            <img src="images/alborz_insurance.png" alt="" class="logo-company">
                            <h3 class="mb-0">
                                بیمه البرز
                            </h3>
                            <button class="btn btn-link" type="button" data-toggle="collapse"
                                    data-target="#collapseOne" aria-expanded="true"
                                    aria-controls="collapseOne">
                                <i class="fas fa-chevron-down"></i>
                            </button>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="alborz"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item"> بیمه شخص ثالث البرز</li>
                                    <li class="list-group-item"> بیمه بدنه البرز</li>
                                    <li class="list-group-item">بیمه موتورسیکلت البرز</li>
                                    <li class="list-group-item">بیمه آتش سوزی البرز</li>
                                    <li class="list-group-item">بیمه زلزله البرز</li>
                                    <li class="list-group-item">بیمه مسافرتی البرز</li>
                                    <li class="list-group-item">بیمه عمر البرز</li>
                                    <li class="list-group-item">بیمه تکمیلی البرز</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card sub-card">
                        <div class="card-header" id="saman">
                            <img src="images/Saman-insurance1.png" alt="" class="logo-company">
                            <h3 class="mb-0">
                                بیمه سامان
                            </h3>
                            <button class="btn btn-link" type="button" data-toggle="collapse"
                                    data-target="#collapseOne" aria-expanded="true"
                                    aria-controls="collapseOne">
                                <i class="fas fa-chevron-down "></i>
                            </button>
                        </div>

                        <div id="collapseOne" class="collapse " aria-labelledby="saman"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item"><a href=""> بیمه شخص ثالث سامان</a></li>
                                    <li class="list-group-item"><a href=""> بیمه بدنه سامان</a></li>
                                    <li class="list-group-item"><a href=""> بیمه موتورسیکلت سامان</a></li>
                                    <li class="list-group-item"><a href=""> بیمه آتش سوزی سامان</a></li>
                                    <li class="list-group-item"><a href=""> بیمه زلزله سامان</a></li>
                                    <li class="list-group-item"><a href=""> بیمه مسافرتی سامان</a></li>
                                    <li class="list-group-item"><a href=""> بیمه عمرسامان</a></li>
                                    <li class="list-group-item"><a href=""> بیمه تکمیلی سامان</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card sub-card">
                        <div class="card-header" id="pasargad">
                            <img src="images/pasargad-insurance.png" alt="" class="logo-company">
                            <h3 class="mb-0">
                                بیمه پاسارگاد
                            </h3>
                            <button class="btn btn-link" type="button" data-toggle="collapse"
                                    data-target="#collapseOne" aria-expanded="true"
                                    aria-controls="collapseOne">
                                <i class="fas fa-chevron-down"></i>
                            </button>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="pasargad"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item"> بیمه شخص ثالث پاسارگاد</li>
                                    <li class="list-group-item"> بیمه بدنه پاسارگاد</li>
                                    <li class="list-group-item">بیمه موتورسیکلت پاسارگاد</li>
                                    <li class="list-group-item">بیمه آتش سوزی پاسارگاد</li>
                                    <li class="list-group-item">بیمه زلزله پاسارگاد</li>
                                    <li class="list-group-item">بیمه مسافرتی پاسارگاد</li>
                                    <li class="list-group-item">بیمه عمر پاسارگاد</li>
                                    <li class="list-group-item">بیمه تکمیلی پاسارگاد</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card sub-card">
                        <div class="card-header" id="moalem">
                            <img src="images/moallem_insurance_company.png" alt="" class="logo-company">
                            <h3 class="mb-0">
                                بیمه معلم
                            </h3>
                            <button class="btn btn-link" type="button" data-toggle="collapse"
                                    data-target="#collapseOne" aria-expanded="true"
                                    aria-controls="collapseOne">
                                <i class="fas fa-chevron-down"></i>
                            </button>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="moalem"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item"> بیمه شخص ثالث معلم</li>
                                    <li class="list-group-item"> بیمه بدنه معلم</li>
                                    <li class="list-group-item">بیمه موتورسیکلت معلم</li>
                                    <li class="list-group-item">بیمه آتش سوزی معلم</li>
                                    <li class="list-group-item">بیمه زلزله معلم</li>
                                    <li class="list-group-item">بیمه مسافرتی معلم</li>
                                    <li class="list-group-item">بیمه عمر معلم</li>
                                    <li class="list-group-item">بیمه تکمیلی معلم</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card sub-card">
                        <div class="card-header" id="parsian">
                            <img src="images/parsian_insurance_company.png" alt="" class="logo-company">
                            <h3 class="mb-0">
                                بیمه پارسیان
                            </h3>
                            <button class="btn btn-link" type="button" data-toggle="collapse"
                                    data-target="#collapseOne" aria-expanded="true"
                                    aria-controls="collapseOne">
                                <i class="fas fa-chevron-down"></i>
                            </button>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="parsian"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item"> بیمه شخص ثالث پارسیان</li>
                                    <li class="list-group-item"> بیمه بدنه پارسیان</li>
                                    <li class="list-group-item">بیمه موتورسیکلت پارسیان</li>
                                    <li class="list-group-item">بیمه آتش سوزی پارسیان</li>
                                    <li class="list-group-item">بیمه زلزله پارسیان</li>
                                    <li class="list-group-item">بیمه مسافرتی پارسیان</li>
                                    <li class="list-group-item">بیمه عمر پارسیان</li>
                                    <li class="list-group-item">بیمه تکمیلی پارسیان</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card sub-card">
                        <div class="card-header" id="novin">
                            <img src="images/novin_insurance_company.png" alt="" class="logo-company">
                            <h3 class="mb-0">
                                بیمه نوین
                            </h3>
                            <button class="btn btn-link" type="button" data-toggle="collapse"
                                    data-target="#collapseOne" aria-expanded="true"
                                    aria-controls="collapseOne">
                                <i class="fas fa-chevron-down"></i>
                            </button>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="novin"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item"> بیمه شخص ثالث نوین</li>
                                    <li class="list-group-item"> بیمه بدنه نوین</li>
                                    <li class="list-group-item">بیمه موتورسیکلت نوین</li>
                                    <li class="list-group-item">بیمه آتش سوزی نوین</li>
                                    <li class="list-group-item">بیمه زلزله نوین</li>
                                    <li class="list-group-item">بیمه مسافرتی نوین</li>
                                    <li class="list-group-item">بیمه عمر نوین</li>
                                    <li class="list-group-item">بیمه تکمیلی نوین</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card sub-card">
                        <div class="card-header" id="razi">
                            <img src="images/razi_insurance_company.png" alt="" class="logo-company">
                            <h3 class="mb-0">
                                بیمه رازی
                            </h3>
                            <button class="btn btn-link" type="button" data-toggle="collapse"
                                    data-target="#collapseOne" aria-expanded="true"
                                    aria-controls="collapseOne">
                                <i class="fas fa-chevron-down "></i>
                            </button>
                        </div>

                        <div id="collapseOne" class="collapse " aria-labelledby="razi"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item"><a href=""> بیمه شخص ثالث رازی</a></li>
                                    <li class="list-group-item"><a href=""> بیمه بدنه رازی</a></li>
                                    <li class="list-group-item"><a href=""> بیمه موتورسیکلت رازی</a></li>
                                    <li class="list-group-item"><a href=""> بیمه آتش سوزی رازی</a></li>
                                    <li class="list-group-item"><a href=""> بیمه زلزله رازی</a></li>
                                    <li class="list-group-item"><a href=""> بیمه مسافرتی رازی</a></li>
                                    <li class="list-group-item"><a href=""> بیمه عمررازی</a></li>
                                    <li class="list-group-item"><a href=""> بیمه تکمیلی رازی</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card sub-card">
                        <div class="card-header" id="dei">
                            <img src="images/dey-insurance1.png" alt="" class="logo-company">
                            <h3 class="mb-0">
                                بیمه دی
                            </h3>
                            <button class="btn btn-link" type="button" data-toggle="collapse"
                                    data-target="#collapseOne" aria-expanded="true"
                                    aria-controls="collapseOne">
                                <i class="fas fa-chevron-down"></i>
                            </button>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="dei"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item"> بیمه شخص ثالث دی</li>
                                    <li class="list-group-item"> بیمه بدنه دی</li>
                                    <li class="list-group-item">بیمه موتورسیکلت دی</li>
                                    <li class="list-group-item">بیمه آتش سوزی دی</li>
                                    <li class="list-group-item">بیمه زلزله دی</li>
                                    <li class="list-group-item">بیمه مسافرتی دی</li>
                                    <li class="list-group-item">بیمه عمر دی</li>
                                    <li class="list-group-item">بیمه تکمیلی دی</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card sub-card">
                        <div class="card-header" id="melat">
                            <img src="images/mellat_insurance_company.png" alt="" class="logo-company">
                            <h3 class="mb-0">
                                بیمه ملت
                            </h3>
                            <button class="btn btn-link" type="button" data-toggle="collapse"
                                    data-target="#collapseOne" aria-expanded="true"
                                    aria-controls="collapseOne">
                                <i class="fas fa-chevron-down"></i>
                            </button>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="melat"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item"> بیمه شخص ثالث ملت</li>
                                    <li class="list-group-item"> بیمه بدنه ملت</li>
                                    <li class="list-group-item">بیمه موتورسیکلت ملت</li>
                                    <li class="list-group-item">بیمه آتش سوزی ملت</li>
                                    <li class="list-group-item">بیمه زلزله ملت</li>
                                    <li class="list-group-item">بیمه مسافرتی ملت</li>
                                    <li class="list-group-item">بیمه عمر ملت</li>
                                    <li class="list-group-item">بیمه تکمیلی ملت</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card sub-card">
                        <div class="card-header" id="asmari">
                            <img src="images/asmari-insurance-1.png" alt="" class="logo-company">
                            <h3 class="mb-0">
                                بیمه آسماری
                            </h3>
                            <button class="btn btn-link" type="button" data-toggle="collapse"
                                    data-target="#collapseOne" aria-expanded="true"
                                    aria-controls="collapseOne">
                                <i class="fas fa-chevron-down"></i>
                            </button>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="asmari"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item"> بیمه شخص ثالث آسماری</li>
                                    <li class="list-group-item"> بیمه بدنه آسماری</li>
                                    <li class="list-group-item">بیمه موتورسیکلت آسماری</li>
                                    <li class="list-group-item">بیمه آتش سوزی آسماری</li>
                                    <li class="list-group-item">بیمه زلزله آسماری</li>
                                    <li class="list-group-item">بیمه مسافرتی آسماری</li>
                                    <li class="list-group-item">بیمه عمر آسماری</li>
                                    <li class="list-group-item">بیمه تکمیلی آسماری</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </ul>
        </div>
    </div>
 
    <div class="shadow">
        <div class="card">
            <div class="card-header">
                بیمه در یک دقیقه
            </div>
            <ul class="list-group list-group-flush">
                <div class="accordion" id="accordionExample">
                    <div class="card sub-card">
                        <div class="card-header" id="insurance-in-1-minute">
                            <h3 class="mb-0">
                                بیمه در یک دقیقه
                            </h3>
                            <button class="btn btn-link" type="button" data-toggle="collapse"
                                    data-target="#collapseOne" aria-expanded="true"
                                    aria-controls="collapseOne">
                                <i class="fas fa-chevron-down "></i>
                            </button>
                        </div>

                        <div id="collapseOne" class="collapse " aria-labelledby="insurance-in-1-minute"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item"><a href=""> اینجا کجاست؟ | بیمه در یک دقیقه
                                            با «بی می تو» </a></li>
                                    <li class="list-group-item"><a href=""> بیمه چیست؟ چرا باید بیمه خرید؟ |
                                            ضرورت داشتن بیمه</a></li>
                                    <li class="list-group-item"><a href=""> برای اینکه آگاهانه بیمه بخریم،
                                            باید اصطلاحات بیمه را بلد باشیم</a></li>
                                    <li class="list-group-item"><a href=""> چند نوع بیمه وجود دارد؟ و چه
                                            بیمه‌‌هایی هستند؟ | بررسی انواع بیمه</a></li>
                                    <li class="list-group-item"><a href=""> فرانشیز بیمه در یک دقیقه و به
                                            زبان ساده</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card sub-card">
                        <div class="card-header" id="thired-insurance-in-1-minute">
                            <h3 class="mb-0">
                                بیمه شخص ثالث در یک دقیقه
                            </h3>
                            <button class="btn btn-link" type="button" data-toggle="collapse"
                                    data-target="#collapseOne" aria-expanded="true"
                                    aria-controls="collapseOne">
                                <i class="fas fa-chevron-down "></i>
                            </button>
                        </div>

                        <div id="collapseOne" class="collapse "
                             aria-labelledby="thired-insurance-in-1-minute"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item"><a href=""> بیمه شخص ثالث را در یک دقیقه یاد
                                            بگیریم | چرا بیمه ثالث اجباری است؟</a></li>
                                    <li class="list-group-item"><a href=""> بیمه شخص ثالث را در یک دقیقه یاد
                                            بگیریم | چرا بیمه ثالث اجباری است؟</a></li>
                                    <li class="list-group-item"><a href=""> محاسبه قیمت بیمه ثالث چه طور
                                            انجام می شود؟</a></li>
                                    <li class="list-group-item"><a href="">چطور از بیمه ثالث تخفیف بگیریم؟ |
                                            تخفیف عدم خسارت بیمه شخص ثالث</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card sub-card">
                        <div class="card-header" id="auto-body-insurance-in-1-minute">
                            <h3 class="mb-0">
                                بیمه بدنه در یک دقیقه
                            </h3>
                            <button class="btn btn-link" type="button" data-toggle="collapse"
                                    data-target="#collapseOne" aria-expanded="true"
                                    aria-controls="collapseOne">
                                <i class="fas fa-chevron-down "></i>
                            </button>
                        </div>

                        <div id="collapseOne" class="collapse "
                             aria-labelledby="auto-body-insurance-in-1-minute"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item"><a href=""> بیمه بدنه را در یک دقیقه یاد
                                            بگیریم | چرا بیمه بدنه اجباری است؟</a></li>
                                    <li class="list-group-item"><a href=""> بیمه بدنه را در یک دقیقه یاد
                                            بگیریم | چرا بیمه بدنه اجباری است؟</a></li>
                                    <li class="list-group-item"><a href=""> محاسبه قیمت بیمه بدنه چه طور
                                            انجام می شود؟</a></li>
                                    <li class="list-group-item"><a href="">چطور از بیمه بدنه تخفیف بگیریم؟ |
                                            تخفیف عدم خسارت بیمه بدنه</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </ul>
        </div>
    </div>
</div>
<!-- end sidebar -->