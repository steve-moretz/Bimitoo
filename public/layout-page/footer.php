<!-- footer -->
<section>
    <div class="footer">
            <div class="row">
                <div class="col-md-6 col-sm-8 footer-box">
                    <div class="footer-title">
                        <h3>راه های ارتباطی  </h3>
                    </div>
                    <div class="footer-cont">
                            <p>
                                <i class="fa fa-map-marker"></i>
                                 آدرس : تهران ، خیابن آزادی ، نبش  شادمهر ، پلاک 409 ، واحد  21
                            </p>
                            <p>
                                <i class="fa fa-envelope"></i>
                                info@bimitoo.com
                            </p>
                            <p>
                               <i class="fa fa-phone"></i>
                               021-22120114
                            </p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 footer-box">
                    <div class="footer-title">
                        <h3>منوی سایت </h3>
                    </div>
                    <div class="footer-cont">
                                    <p>
                                        <a href="third-party-insurance.php">بیمه شخص ثالث</a>
                                    </p>
                                    <p><a href="auto-body-insurance.php">بیمه بدنه</a></p>
                                    <p><a href="motor-third-party-insurance.php">بیمه موتور سیکلت</a></p>
                                    <p><a href="fire-insurance.php">بیمه آتش سوزی و زلزله</a></p>
                                    <p>
                                        <a href="hrefplife-insurance.php">بیمه عمر</a>
                                    </p>
                                    <p><a href="hreftravel-insurance.php">بیمه مسافرتی</a></p>
                                    <p><a href="hrefsupplementary-health-insurance.php">بیمه درمان</a></p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12 footer-box">
                    <div class="footer-title">
                        <h3 class="text-right"> اطلاعات بیشتر   </h3>
                    </div>
                    <div class="footer-cont">
                                    <p>
                                        <a href="third-party-insurance.php"> مجله ما  </a>
                                    </p>
                                    <p><a href="motor-third-party-insurance.php">سوالات متداول  </a></p>
                                    <p><a href="fire-insurance.php"> همکارای با ما  </a></p>
                    </div>
                </div>
            </div>
    </div>
</section>

<div class="footer-text">
    <h3>کلیه حقوق این سایت متعلق به وب سایت بی می تو می باشد</h3>
    <h3>   طراحی  و پشتیبانی  : نیک  رایان  </h3>
</div>
<!-- end footer -->

<!-- jquery-3.4.1  -->
<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>

<!-- Bootstrap js Ver 4.3.1 -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrap.bundle.min.js"></script>

<!-- Font awesome js 5.8.2 -->
<script type="text/javascript" src="fontawesome.min.js"></script>


<!-- form slider -->
<link href="dist/jquery.simplewizard.css" rel="stylesheet" />
<script src="dist/jquery.simplewizard.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.min.js"></script>
<script>
        $(function () {
            $(".wizard").simpleWizard({
                cssClassStepActive: "active",
                cssClassStepDone: "done",
                onFinish: function() {
                    alert("Wizard finished")
                }
            });
        });
</script>

<!-- login -->
<script>
    $(function() {

        $(".input input").focus(function() {

            $(this).parent(".input").each(function() {
                $("label", this).css({
                    "line-height": "18px",
                    "font-size": "18px",
                    "font-weight": "100",
                    "top": "0px"
                })
                $(".spin", this).css({
                    "width": "100%"
                })
            });
        }).blur(function() {
            $(".spin").css({
                "width": "0px"
            })
            if ($(this).val() == "") {
                $(this).parent(".input").each(function() {
                    $("label", this).css({
                        "line-height": "60px",
                        "font-size": "24px",
                        "font-weight": "300",
                        "top": "10px"
                    })
                });

            }
        });

        $(".button").click(function(e) {
            var pX = e.pageX,
                    pY = e.pageY,
                    oX = parseInt($(this).offset().left),
                    oY = parseInt($(this).offset().top);

            $(this).append('<span class="click-efect x-' + oX + ' y-' + oY + '" style="margin-left:' + (pX - oX) + 'px;margin-top:' + (pY - oY) + 'px;"></span>')
            $('.x-' + oX + '.y-' + oY + '').animate({
                "width": "500px",
                "height": "500px",
                "top": "-250px",
                "left": "-250px",

            }, 600);
            $("button", this).addClass('active');
        })

        $(".alt-2").click(function() {
            if (!$(this).hasClass('material-button')) {
                $(".shape").css({
                    "width": "100%",
                    "height": "100%",
                    "transform": "rotate(0deg)"
                })

                setTimeout(function() {
                    $(".overbox").css({
                        "overflow": "initial"
                    })
                }, 600)

                $(this).animate({
                    "width": "140px",
                    "height": "140px"
                }, 500, function() {
                    $(".box").removeClass("back");

                    $(this).removeClass('active')
                });

                $(".overbox .title").fadeOut(300);
                $(".overbox .input").fadeOut(300);
                $(".overbox .button").fadeOut(300);

                $(".alt-2").addClass('material-buton');
            }

        })

        $(".material-button").click(function() {

            if ($(this).hasClass('material-button')) {
                setTimeout(function() {
                    $(".overbox").css({
                        "overflow": "hidden"
                    })
                    $(".box").addClass("back");
                }, 200)
                $(this).addClass('active').animate({
                    "width": "700px",
                    "height": "700px"
                });

                setTimeout(function() {
                    $(".shape").css({
                        "width": "50%",
                        "height": "50%",
                        "transform": "rotate(45deg)"
                    })

                    $(".overbox .title").fadeIn(300);
                    $(".overbox .input").fadeIn(300);
                    $(".overbox .button").fadeIn(300);
                }, 700)

                $(this).removeClass('material-button');

            }

            if ($(".alt-2").hasClass('material-buton')) {
                $(".alt-2").removeClass('material-buton');
                $(".alt-2").addClass('material-button');
            }

        });

    });
</script>

