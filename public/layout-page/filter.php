<!--  filter -->
<div class="content">

  <section>
    <div class="search-section">
        <div class="aboutus">
            <span>بی می تو </span>
                 مقایسه، مشاوره و خرید آنلاین بیمه  
        </div>          
        <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-filter" >
                        <li class="nav-item active" id="p1">
                            <a class="nav-link " data-toggle="tab" href="#part1">
                                <i class="fas fa-user-shield icon fa-1x">
                                </i>
                                <span>شخص ثالث  </span>
                            </a>
                        </li>
                        <li class="nav-item "  id="p2">
                            <a class="nav-link" data-toggle="tab" href="#part2">
                                <i class="fas fa-motorcycle icon fa-1x">
                                </i>
                                <span> موتور سیکلت  </span>
                            </a>
                        </li> 
                        <li class="nav-item"  id="p3">
                            <a class="nav-link" data-toggle="tab" href="#part3">
                                <i class="fas fa-car icon fa-1x">
                                </i>
                                <span>بدنه  </span>
                            </a>
                        </li>
                        <li class="nav-item"  id="p4">
                            <a class="nav-link" data-toggle="tab" href="#part4">
                                <i class="fas fa-fire icon fa-1x">
                                </i>
                                <span> عمر </span>
                            </a>
                        </li>
                        <li class="nav-item "  id="p5">
                            <a class="nav-link" data-toggle="tab" href="#part5">
                                <i class="fas fa-suitcase-rolling icon fa-1x">
                                </i>
                                <span> مسافرتی  </span>
                            </a>
                        </li>
                        <li class="nav-item "  id="p6">
                            <a class="nav-link" data-toggle="tab" href="#part6">
                                <i class="fas fa-clinic-medical icon fa-1x">
                                </i>
                                <span> درمان تکمیلی  </span>
                            </a>
                        </li>
                        <li class="nav-item"  id="p7">
                            <a class="nav-link" data-toggle="tab" href="#part7">
                                <i class="fas fa-fire icon fa-1x">
                                </i>
                                <span> زلزله  </span>
                            </a>
                        </li>
                         <li class="nav-item lastmenu"  id="p8">
                            <a class="nav-link" data-toggle="tab" href="#part8">
                                <i class="fas fa-plus icon fa-1x">
                                </i>
                                <span> سایر بیمه ها </span>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">

    <div class="tab-pane fade active" id="part1">
        <form id="wizard2" class="wizard slider-form">          
            <div class="wizard-content">
                <div id="step1" class="wizard-step">
                    <div class="col-sm-10 col-xs-12 content-step">
                                                    <div class="col-lg-4 col-md-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> نام  وسیله نقلیه : </label>
                                                             <select class="slider-input form-control " id="">                                       
                                                                    <option value=""> انتخاب  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> مدل وسیله نقلیه : </label>
                                                            <select class="slider-input form-control " id="">
                                                              
                                                                   <option value=""> انتخاب  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> کاربری : </label>
                                                            <select class="slider-input form-control " id="">
                                                              
                                                                   <option value=""> کاربری  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                    </div>
                    <div class="col-sm-2 col-xs-12 btns-step next-par">
                        <button type="button" class="wizard-btn wizard-next btn press-btn">
                            <i class="fas fa-caret-left"></i>
                            <span> بعد  </span>
                        </button>
                    </div>                                                  
                </div>
                <div id="step2" class="wizard-step">
                    <div class="col-sm-2 col-xs-12 btns-step prev-par">
                        <button type="button" class="wizard-btn wizard-prev btn press-btn">
                            <i class="fas fa-caret-right"></i>
                            <span> قبلی  </span>
                        </button>
                    </div>   
                    <div class="col-sm-8 col-xs-12 content-step">
                                                    <div class="col-md-6 col-sm-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label">سال ساخت :  </label>
                                                            <select class="slider-input form-control " id="">
                                                              
                                                                    <option value=""></option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> بیمه گر قبلی  :  </label>
                                                             <select class="slider-input form-control " id="">                                       
                                                                    <option value=""> انتخاب  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                    </div>
                    <div class="col-sm-2 col-xs-12 btns-step next-par">
                        <button type="button" class="wizard-btn wizard-next btn press-btn">
                            <i class="fas fa-caret-left"></i>
                            <span> بعد  </span>
                        </button>
                    </div>   
                </div>
                <div id="step3" class="wizard-step">
                    <div class="col-sm-2 col-xs-12 btns-step prev-par">
                        <button type="button" class="wizard-btn wizard-prev btn press-btn ">
                            <i class="fas fa-caret-right"></i>
                            <span> قبلی  </span>
                        </button>
                    </div>   
                    <div class="col-sm-8 col-xs-12  content-step">
                                                    <div class="col-md-6 col-sm-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label">  تاریخ شروع بیمه نامه قبلی  : </label>
                                                            <select class="slider-input form-control " id="">
                                                              
                                                                   <option value=""> انتخاب  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> تاریخ انقضای بیمه نامه قبلی  : </label>
                                                            <select class="slider-input form-control " id="">
                                                              
                                                                   <option value=""> کاربری  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                    </div>
                    <div class="col-sm-2 col-xs-12 btns-step next-par">
                        <button type="button" class="wizard-btn wizard-next btn press-btn">
                            <i class="fas fa-caret-left"></i>
                            <span> بعد  </span>
                        </button>
                    </div>   
                </div>                            
                <div id="step4" class="wizard-step">
                    <div class="col-sm-2 col-xs-12 btns-step prev-par">
                        <button type="button" class="wizard-btn wizard-prev btn press-btn ">
                            <i class="fas fa-caret-right"></i>
                            <span> قبلی  </span>
                        </button>
                    </div>   
                    <div class=" col-sm-8 col-xs-12 content-step">
                                                    <div class="col-lg-4 col-md-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> درصد تخفیف بیمه‌نامه قبلی  : </label>
                                                             <select class="slider-input form-control  " id="">                                       
                                                                    <option value=""> درصد تخفیف بیمه‌نامه قبلی  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label">  درصد تخفیف حوادث راننده  : </label>
                                                            <select class="slider-input form-control " id="">
                                                              
                                                                   <option value=""> درصد تخفیف حوادث راننده  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> خسارت از بیمه نامه قبلی :   </label>
                                                            <select class="slider-input form-control " id="">
                                                              
                                                                   <option value=""> خسارت از بیمه نامه قبلی :  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                    </div>
                    <div class="col-sm-2 col-xs-12 btns-step next-par reg-par">
                        <button type="button" class="wizard-finish wizard-reg btn bimi-btns press-btn">
                            <span> مقایسه  </span>
                        </button>
                    </div>   
                </div>
            </div>
            <div class="wizard-header">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="wizard-step-indicator"><a href="#!">
                        <span class="badge slider-counter">1</span>
                    </a></li>
                    <li role="presentation" class="wizard-step-indicator"><a href="#!">
                        <span class="badge slider-counter">2</span>
                    </a></li>
                    <li role="presentation" class="wizard-step-indicator"><a href="#!">
                        <span class="badge slider-counter">3</span>
                    </a></li>
                    <li role="presentation" class="wizard-step-indicator"><a href="#!">
                        <span class="badge slider-counter">4</span>
                    </a></li>
                </ul>
                <div class="slider-line"></div>
            </div> 
        </form>
    </div>
    <div class="tab-pane  fade" id="part2">
        <form id="wizard2" class="wizard slider-form">          
            <div class="wizard-content">
                <div id="step1" class="wizard-step">
                    <div class="col-sm-10 col-xs-12  content-step">
                                                    <div class="col-lg-4 col-md-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> مدل  موتورسیکلت  : </label>
                                                             <select class="slider-input form-control " id="">                                       
                                                                    <option value=""> انتخاب  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> سال ساخت : </label>
                                                            <select class="slider-input form-control " id="">
                                                              
                                                                   <option value=""> انتخاب  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> بیمه گر قبلی  :</label>
                                                            <select class="slider-input form-control " id="">
                                                              
                                                                   <option value=""> کاربری  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                    </div>
                    <div class="col-sm-2 col-xs-12  btns-step next-par">
                        <button type="button" class="wizard-btn wizard-next btn press-btn">
                            <i class="fas fa-caret-left"></i>
                            <span> بعد  </span>
                        </button>
                    </div>                                                  
                </div>
                <div id="step2" class="wizard-step">
                    <div class="col-sm-2 col-xs-12 btns-step prev-par">
                        <button type="button" class="wizard-btn wizard-prev btn press-btn">
                            <i class="fas fa-caret-right"></i>
                            <span> قبلی  </span>
                        </button>
                    </div>   
                    <div class="col-sm-8 col-xs-12 content-step">
                                                    <div class="col-md-6 col-sm-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label">  تاریخ شروع بیمه نامه قبلی  : </label>
                                                            <select class="slider-input form-control " id="">
                                                              
                                                                   <option value=""> انتخاب  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> تاریخ انقضای بیمه نامه قبلی  : </label>
                                                            <select class="slider-input form-control " id="">
                                                              
                                                                   <option value=""> کاربری  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                    </div>
                    <div class="col-sm-2 col-xs-12  btns-step next-par">
                        <button type="button" class="wizard-btn wizard-next btn press-btn">
                            <i class="fas fa-caret-left"></i>
                            <span> بعد  </span>
                        </button>
                    </div>   
                </div>
                <div id="step3" class="wizard-step">
                    <div class=" col-sm-2 col-xs-12  btns-step prev-par">
                        <button type="button" class="wizard-btn wizard-prev btn press-btn">
                            <i class="fas fa-caret-right"></i>
                            <span> قبلی  </span>
                        </button>
                    </div>   
                    <div class="col-sm-8 col-xs-12 content-step">
                                                    <div class="col-lg-4 col-md-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> درصد تخفیف ثالث   </label>
                                                             <select class="slider-input form-control  " id="">                                       
                                                                    <option value=""> رصد تخفیف ثالث   </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label">  درصد تخفیف حوادث راننده  </label>
                                                            <select class="slider-input form-control " id="">
                                                              
                                                                   <option value=""> درصد تخفیف حوادث راننده  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> خسارت از بیمه نامه  قبلی  </label>
                                                            <select class="slider-input form-control " id="">
                                                              
                                                                   <option value=""> خسارت از بیمه نامه  قبلی </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                    </div>
                    <div class=" col-sm-2 col-xs-12  btns-step next-par reg-par">
                        <button type="button" class="wizard-finish wizard-reg btn bimi-btns press-btn">
                            <span> مقایسه  </span>
                        </button>
                    </div>   
                </div>
            </div>
            <div class="wizard-header">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="wizard-step-indicator"><a href="#!">
                        <span class="badge slider-counter">1</span>
                    </a></li>
                    <li role="presentation" class="wizard-step-indicator"><a href="#!">
                        <span class="badge slider-counter">2</span>
                    </a></li>
                    <li role="presentation" class="wizard-step-indicator"><a href="#!">
                        <span class="badge slider-counter">3</span>
                    </a></li>
                </ul>
                <div class="slider-line"></div>
            </div> 
        </form>
    </div>
    <div class="tab-pane  fade" id="part3">
        <form id="wizard3" class="wizard slider-form">          
            <div class="wizard-content">
                <div id="step1" class="wizard-step">
                    <div class="col-sm-10 col-xs-12 content-step">
                                                    <div class="col-lg-4 col-md-12 tab-col">
                                                        <div class="form-group">             
                                                            <label class="slider-label"> نوع خودرو :  </label>
                                                             <select class="slider-input form-control " id="">                                       
                                                                    <option value=""> انتخاب  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> مدل خودرو  : </label>
                                                            <select class="slider-input form-control " id="">
                                                              
                                                                   <option value=""> انتخاب  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> سال ساخت خودرو : </label>
                                                            <select class="slider-input form-control " id="">
                                                              
                                                                   <option value=""> کاربری  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                    </div>
                    <div class="col-sm-2 col-xs-12  btns-step next-par">
                        <button type="button" class="wizard-btn wizard-next btn press-btn">
                            <i class="fas fa-caret-left"></i>
                            <span> بعد  </span>
                        </button>
                    </div>                                                  
                </div>
                <div id="step2" class="wizard-step">
                    <div class="col-sm-2 col-xs-12  btns-step  prev-par">
                        <button type="button" class="wizard-btn wizard-prev btn press-btn">
                            <i class="fas fa-caret-right"></i>
                            <span> قبلی  </span>
                        </button>
                    </div>   
                    <div class="col-sm-8 col-xs-12 content-step ">
                                                    <div class="col-xl-6 col-md-6 col-sm-12 tab-col">
                                                       <button class="btn slider-inner-btn btn-true">
                                                        <i class="fa fa-user"></i>
                                                            بیمه  بدنه دارم 
                                                       </button>
                                                    </div>   
                                                    <div class="col-xl-6 col-md-6 col-sm-12 tab-col">
                                                       <button class="btn slider-inner-btn btn-false">
                                                        <i class="fa fa-user"></i>
                                                         بیمه بدنه ندارم 
                                                       </button>
                                                    </div>       
                    </div>
                    <div class="col-sm-2 col-xs-12 btns-step next-par">
                        <button type="button" class="wizard-btn wizard-next btn press-btn">
                            <i class="fas fa-caret-left"></i>
                            <span> بعد  </span>
                        </button>
                    </div>   
                </div>
                <div id="step3" class="wizard-step">
                    <div class="col-sm-2 col-xs-12  btns-step prev-par">
                        <button type="button" class="wizard-btn wizard-prev btn press-btn">
                            <i class="fas fa-caret-right"></i>
                            <span> قبلی  </span>
                        </button>
                    </div>   
                    <div class="col-sm-8 col-xs-12  content-step">
                                                    <div class="col-md-6 col-sm-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> تخفیف عدم خسارت  : </label>
                                                             <select class="slider-input form-control  " id="">                                       
                                                                    <option value=""> انتخاب </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label">  ارزش روز خودرو  : </label>
                                                            <input type="text" name="" placeholder="تومان " class="form-control slider-input">
                                                        </div>
                                                    </div>
                    </div>
                    <div class="col-sm-2 col-xs-12  btns-step next-par reg-par">
                        <button type="button" class="wizard-finish wizard-reg btn bimi-btns press-btn">
                            <span> مقایسه  </span>
                        </button>
                    </div>   
                </div>
            </div>
            <div class="wizard-header">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="wizard-step-indicator"><a href="#!">
                        <span class="badge slider-counter">1</span>
                    </a></li>
                    <li role="presentation" class="wizard-step-indicator"><a href="#!">
                        <span class="badge slider-counter">2</span>
                    </a></li>
                    <li role="presentation" class="wizard-step-indicator"><a href="#!">
                        <span class="badge slider-counter">3</span>
                    </a></li>
                </ul>
                <div class="slider-line"></div>
            </div> 
        </form>
    </div>
    <div class="tab-pane fade " id="part4">
        <form id="wizard2" class="wizard slider-form">          
            <div class="wizard-content">
                <div id="step1" class="wizard-step"> 
                    <div class="col-sm-10 col-xs-12  content-step">
                                                    <div class="col-md-4 col-sm-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> روز تولد  :  </label>
                                                             <select class="slider-input form-control " id="">                                       
                                                                    <option value=""> انتخاب  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-sm-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label">    ماه تولد  : </label>
                                                            <select class="slider-input form-control " id="">
                                                              
                                                                   <option value=""> انتخاب  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-sm-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> سال تولد  : </label>
                                                            <select class="slider-input form-control " id="">
                                                              
                                                                   <option value=""> انتخاب   </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                    </div>
                    <div class="col-sm-2 col-xs-12 btns-step next-par">
                        <button type="button" class="wizard-btn wizard-next btn press-btn">
                            <i class="fas fa-caret-left"></i>
                            <span> بعد  </span>
                        </button>
                    </div>   
                </div>
                <div id="step2" class="wizard-step">
                    <div class="col-sm-2 col-xs-12 btns-step prev-par">
                        <button type="button" class="wizard-btn wizard-prev btn press-btn">
                            <i class="fas fa-caret-right"></i>
                            <span> قبلی  </span>
                        </button>
                    </div>   
                    <div class="col-sm-8 col-xs-12 content-step">
                                                    <div class="col-xl-8 col-xl-offset-2 col-lg-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> مدت قرارداد  :  </label>
                                                             <select class="slider-input form-control " id="">                                       
                                                                    <option value=""> انتخاب  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                    </div>
                    <div class="col-sm-2 col-xs-12  btns-step next-par">
                        <button type="button" class="wizard-btn wizard-next btn press-btn">
                            <i class="fas fa-caret-left"></i>
                            <span> بعد  </span>
                        </button>
                    </div>   
                </div>
                <div id="step3" class="wizard-step">
                    <div class="col-sm-2 col-xs-12 btns-step prev-par">
                        <button type="button" class="wizard-btn wizard-prev btn press-btn">
                            <i class="fas fa-caret-right"></i>
                            <span> قبلی  </span>
                        </button>
                    </div>   
                    <div class="col-sm-8 col-xs-12 content-step">
                                                    <div class="col-xl-8 col-xl-offset-2 col-lg-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> توان پرداخت ماهانه : </label>
                                                            <input type="text" class="form-control slider-input" name="" placeholder="توان پرداخت ماهانه ">
                                                        </div>
                                                    </div>
                    </div>
                    <div class="col-sm-2 col-xs-12 btns-step next-par reg-par">
                        <button type="button" class="wizard-finish wizard-reg btn bimi-btns press-btn">
                            <span> مقایسه  </span>
                        </button>
                    </div>   
                </div>
            </div>
            <div class="wizard-header">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="wizard-step-indicator"><a href="#!">
                        <span class="badge slider-counter">1</span>
                    </a></li>
                    <li role="presentation" class="wizard-step-indicator"><a href="#!">
                        <span class="badge slider-counter">2</span>
                    </a></li>
                    <li role="presentation" class="wizard-step-indicator"><a href="#!">
                        <span class="badge slider-counter">3</span>
                    </a></li>
                </ul>
                <div class="slider-line"></div>
            </div> 
        </form>                                                                                            
    </div>              
    <div class="tab-pane  fade" id="part5">
        <form id="wizard3" class="wizard slider-form">          
            <div class="wizard-content">
                <div id="step1" class="wizard-step">
                    <div class="col-sm-10 col-xs-12  content-step">
                                                    <div class="col-md-6 col-sm-12 tab-col">
                                                       <button class="btn slider-inner-btn">
                                                        <i class="fa fa-user"></i>
                                                            سفر به یک کشور 
                                                       </button>
                                                    </div>   
                                                    <div class="col-md-6 col-sm-12 tab-col">
                                                       <button class="btn slider-inner-btn ">
                                                        <i class="fa fa-user"></i>
                                                         سفر به چند کشور 
                                                       </button>
                                                    </div>       
                    </div>
                    <div class="col-sm-2 col-xs-12  btns-step next-par">
                        <button type="button" class="wizard-btn wizard-next btn press-btn">
                            <i class="fas fa-caret-left"></i>
                            <span> بعد  </span>
                        </button>
                    </div>   
                </div>
                <div id="step2" class="wizard-step">
                    <div class="col-sm-2 col-xs-12  btns-step prev-par">
                        <button type="button" class="wizard-btn wizard-prev btn press-btn">
                            <i class="fas fa-caret-right"></i>
                            <span> قبلی  </span>
                        </button>
                    </div>   
                    <div class="col-sm-8 col-xs-12  content-step">
                                                    <div class="col-md-4 col-sm-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> کشور مقصد  : </label>
                                                             <select class="slider-input form-control  " id="">                                       
                                                                    <option value=""> انتخاب </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-sm-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> مدت سفر  : </label>
                                                             <select class="slider-input form-control  " id="">                                       
                                                                    <option value=""> انتخاب </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-sm-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> تعداد مسافر  : </label>
                                                            <input type="number" name="" class="form-control slider-input">
                                                        </div>
                                                    </div>
                    </div>
                    <div class=" col-sm-2 col-xs-12  btns-step next-par reg-par">
                        <button type="button" class="wizard-finish wizard-reg btn bimi-btns press-btn">
                            <span> مقایسه  </span>
                        </button>
                    </div>   
                </div>
            </div>
            <div class="wizard-header">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="wizard-step-indicator"><a href="#!">
                        <span class="badge slider-counter">1</span>
                    </a></li>
                    <li role="presentation" class="wizard-step-indicator"><a href="#!">
                        <span class="badge slider-counter">2</span>
                    </a></li>
                </ul>
                <div class="slider-line"></div>
            </div> 
        </form>
    </div>
    <div class="tab-pane fade " id="part6">
        <form id="wizard2" class="wizard slider-form">          
            <div class="wizard-content">
                <div id="step1" class="wizard-step">
                    <div class="col-sm-10 col-xs-12 content-step">
                                                    <div class="col-xl-offset-1 col-xl-5 col-md-6 col-sm-12 tab-col">
                                                       <button class="btn slider-inner-btn">
                                                        <i class="fa fa-user"></i>
                                                        انفردای
                                                       </button>
                                                    </div>   
                                                    <div class="col-xl-offset-1 col-xl-5 col-md-6 col-sm-12 tab-col">
                                                       <button class="btn slider-inner-btn">
                                                        <i class="fa fa-user"></i>
                                                        شرکتی 
                                                       </button>
                                                    </div>       
                    </div>
                    <div class="col-sm-2 col-xs-12 btns-step next-par">
                        <button type="button" class="wizard-btn wizard-next btn press-btn">
                            <i class="fas fa-caret-left"></i>
                            <span> بعد  </span>
                        </button>
                    </div>                                                  
                </div>
                <div id="step2" class="wizard-step">
                    <div class="col-sm-2 col-xs-12 btns-step prev-par">
                        <button type="button" class="wizard-btn wizard-prev btn press-btn">
                            <i class="fas fa-caret-right"></i>
                            <span> قبلی  </span>
                        </button>
                    </div>   
                    <div class="col-sm-8 col-xs-12 content-step">
                                                    <div class="col-lg-4 col-md-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> روز تولد  :  </label>
                                                             <select class="slider-input form-control " id="">                                       
                                                                    <option value=""> انتخاب  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label">    ماه تولد  : </label>
                                                            <select class="slider-input form-control " id="">
                                                              
                                                                   <option value=""> انتخاب  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> سال تولد  : </label>
                                                            <select class="slider-input form-control " id="">
                                                              
                                                                   <option value=""> انتخاب   </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                    </div>
                    <div class="col-sm-2 col-xs-12 btns-step next-par">
                        <button type="button" class="wizard-btn wizard-next btn press-btn">
                            <i class="fas fa-caret-left"></i>
                            <span> بعد  </span>
                        </button>
                    </div>   
                </div>
                <div id="step3" class="wizard-step">
                    <div class="col-sm-2 col-xs-12 btns-step prev-par">
                        <button type="button" class="wizard-btn wizard-prev btn press-btn">
                            <i class="fas fa-caret-right"></i>
                            <span> قبلی  </span>
                        </button>
                    </div>   
                    <div class="col-sm-8 col-xs-12 content-step">
                                                    <div class="col-xl-8 col-xl-offset-2 col-lg-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> بیمه گر پایه  : </label>
                                                             <select class="slider-input form-control  " id="">                                       
                                                                    <option value=""> انتخاب  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>                                                            
                                                            </select>
                                                        </div>
                                                    </div>
                    </div>
                    <div class="col-sm-2 col-xs-12 btns-step next-par reg-par">
                        <button type="button" class="wizard-finish wizard-reg btn bimi-btns press-btn">
                            <span> مقایسه  </span>
                        </button>
                    </div>   
                </div>
            </div>
            <div class="wizard-header">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="wizard-step-indicator"><a href="#!">
                        <span class="badge slider-counter">1</span>
                    </a></li>
                    <li role="presentation" class="wizard-step-indicator"><a href="#!">
                        <span class="badge slider-counter">2</span>
                    </a></li>
                    <li role="presentation" class="wizard-step-indicator"><a href="#!">
                        <span class="badge slider-counter">3</span>
                    </a></li>
                </ul>
                <div class="slider-line"></div>
            </div> 
        </form>                                                                                            
    </div>
    <div class="tab-pane fade " id="part7">
        <form id="wizard2" class="wizard slider-form">          
            <div class="wizard-content">
                <div id="step1" class="wizard-step"> 
                    <div class="col-sm-10 col-xs-12  content-step">
                                                    <div class="col-md-4 col-sm-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> نوع ملک  :  </label>
                                                             <select class="slider-input form-control " id="">                                       
                                                                    <option value=""> انتخاب  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-sm-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label">    تعداد واحدها  : </label>
                                                            <select class="slider-input form-control " id="">
                                                              
                                                                   <option value=""> انتخاب  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-sm-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> نوع سازه  : </label>
                                                            <select class="slider-input form-control " id="">
                                                              
                                                                   <option value=""> انتخاب   </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                    </div>
                    <div class="col-sm-2 col-xs-12  btns-step next-par">
                        <button type="button" class="wizard-btn wizard-next btn press-btn">
                            <i class="fas fa-caret-left"></i>
                            <span> بعد  </span>
                        </button>
                    </div>   
                </div>
                <div id="step2" class="wizard-step">
                    <div class="col-sm-2 col-xs-12  btns-step prev-par">
                        <button type="button" class="wizard-btn wizard-prev btn press-btn">
                            <i class="fas fa-caret-right"></i>
                            <span> قبلی  </span>
                        </button>
                    </div>   
                    <div class="col-sm-8 col-xs-12  content-step">
                                                    <div class="col-xl-8 col-xl-offset-2 col-lg-9 col-l-offset-2 col-md-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> متراژ مورد بیمه :  </label>
                                                            <input type="text" name="" class="form-control slider-input" placeholder=" متراژ مورد بیمه ">
                                                        </div>
                                                    </div>
                    </div>
                    <div class="col-sm-2 col-xs-12 btns-step next-par">
                        <button type="button" class="wizard-btn wizard-next btn press-btn">
                            <i class="fas fa-caret-left"></i>
                            <span> بعد  </span>
                        </button>
                    </div>   
                </div>
                <div id="step3" class="wizard-step">
                    <div class="col-sm-2 col-xs-12 btns-step prev-par">
                        <button type="button" class="wizard-btn wizard-prev btn press-btn">
                            <i class="fas fa-caret-right"></i>
                            <span> قبلی  </span>
                        </button>
                    </div>   
                    <div class="col-sm-8 col-xs-12 content-step">
                                                    <div class="col-xl-8 col-xl-offset-2 col-lg-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> ارزش لوازم منزل خود را به استثنای پول نقد و طلا و جواهرات به تومان وارد کنید : </label>
                                                            <input type="text" class="form-control slider-input" name="" placeholder="ارزش لوازم منزل خود را به استثنای پول نقد و طلا و جواهرات به تومان وارد کنید">
                                                        </div>
                                                    </div>
                    </div>
                    <div class="col-sm-2 col-xs-12 btns-step next-par reg-par">
                        <button type="button" class="wizard-finish wizard-reg btn bimi-btns press-btn">
                            <span> مقایسه  </span>
                        </button>
                    </div>   
                </div>
            </div>
            <div class="wizard-header">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="wizard-step-indicator"><a href="#!">
                        <span class="badge slider-counter">1</span>
                    </a></li>
                    <li role="presentation" class="wizard-step-indicator"><a href="#!">
                        <span class="badge slider-counter">2</span>
                    </a></li>
                    <li role="presentation" class="wizard-step-indicator"><a href="#!">
                        <span class="badge slider-counter">3</span>
                    </a></li>
                </ul>
                <div class="slider-line"></div>
            </div> 
        </form>                                                                                            
    </div> 
    <div class="tab-pane fade " id="part8">
        <form id="wizard2" class="wizard slider-form">          
            <div class="wizard-content">
                <div id="step1" class="wizard-step"> 
                    <div class="col-sm-10 col-xs-12 content-step">
                                                    <div class="col-md-4 col-sm-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> نوع ملک  :  </label>
                                                             <select class="slider-input form-control " id="">                                       
                                                                    <option value=""> انتخاب  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-sm-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label">    تعداد واحدها  : </label>
                                                            <select class="slider-input form-control " id="">
                                                              
                                                                   <option value=""> انتخاب  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-sm-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> نوع سازه  : </label>
                                                            <select class="slider-input form-control " id="">
                                                              
                                                                   <option value=""> انتخاب   </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                                    <option value=""> گزینه  </option>
                                                             
                                                            </select>
                                                        </div>
                                                    </div>
                    </div>
                    <div class="col-sm-2 col-xs-12 btns-step next-par">
                        <button type="button" class="wizard-btn wizard-next btn press-btn">
                            <i class="fas fa-caret-left"></i>
                            <span> بعد  </span>
                        </button>
                    </div>   
                </div>
                <div id="step2" class="wizard-step">
                    <div class="col-sm-2 col-xs-12 btns-step prev-par">
                        <button type="button" class="wizard-btn wizard-prev btn press-btn">
                            <i class="fas fa-caret-right"></i>
                            <span> قبلی  </span>
                        </button>
                    </div>   
                    <div class="col-sm-8 col-xs-12 content-step">
                                                    <div class="col-xl-8 col-xl-offset-2collg-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> متراژ مورد بیمه :  </label>
                                                            <input type="text" name="" class="form-control slider-input" placeholder=" متراژ مورد بیمه ">
                                                        </div>
                                                    </div>
                    </div>
                    <div class="col-sm-2 col-xs-12 btns-step next-par">
                        <button type="button" class="wizard-btn wizard-next btn press-btn">
                            <i class="fas fa-caret-left"></i>
                            <span> بعد  </span>
                        </button>
                    </div>   
                </div>
                <div id="step3" class="wizard-step">
                    <div class="col-sm-2 col-xs-12 btns-step prev-par">
                        <button type="button" class="wizard-btn wizard-prev btn press-btn">
                            <i class="fas fa-caret-right"></i>
                            <span> قبلی  </span>
                        </button>
                    </div>   
                    <div class="col-sm-8 col-xs-12 content-step">
                                                    <div class="col-xl-8 col-xl-offset-2 col-lg-12 tab-col">
                                                        <div class="form-group">
                                                            <label class="slider-label"> ارزش لوازم منزل خود را به استثنای پول نقد و طلا و جواهرات به تومان وارد کنید : </label>
                                                            <input type="text" class="form-control slider-input" name="" placeholder="ارزش لوازم منزل خود را به استثنای پول نقد و طلا و جواهرات به تومان وارد کنید">
                                                        </div>
                                                    </div>
                    </div>
                    <div class="col-sm-2 col-xs-12 btns-step next-par reg-par">
                        <button type="button" class="wizard-finish wizard-reg btn bimi-btns press-btn">
                            <span> مقایسه  </span>
                        </button>
                    </div>   
                </div>
            </div>
            <div class="wizard-header">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="wizard-step-indicator"><a href="#!">
                        <span class="badge slider-counter">1</span>
                    </a></li>
                    <li role="presentation" class="wizard-step-indicator"><a href="#!">
                        <span class="badge slider-counter">2</span>
                    </a></li>
                    <li role="presentation" class="wizard-step-indicator"><a href="#!">
                        <span class="badge slider-counter">3</span>
                    </a></li>
                </ul>
                <div class="slider-line"></div>
            </div> 
        </form>                                                                                            
    </div> 

                    </div>
   
    </div>
 </section>
</div>
