<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'insurance'
], function () {
    Route::get('/{rest?}', function ($rest) {
        $endPoint = env('RESTCORE_API').$rest;
//        dd(env('APP_URL'),env('RESTCORE_API'));
        $headers = [
            'token' => '788b8f8c-2fcd-11ea-8ff7-005056b43abe',
            'X-Requested-With'=>'XMLHttpRequest',
            'Content-Type'=>'application/x-www-form-urlencoded',
            'JWT_Token'=>'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6Im81b1VjVUxZNDNRdms1NEVHU3dodTFGK0R5RkJFYzVLUHJrM3hyTXp1eGtkRGh1d25rdC9ncHNoeFpoUlFvV2ZzOHJVY3ZKUnZDdjE0UnN4aDBsME43N0phUit2U1NNb3dQd1hkckNaOTh2SStXN1pRSStjQkFRZC9MOHNIM2xZN1lEOEwxbGNXNjRJUVJLaFIwUGNMQW1iTnRNTnppbnY4S2w2b2p5Q2NPdFpQeVJRUXYzMmgxWkdHd2VoZVdacDM3OFJOS3ZnMCtpdCtQUy9qRGJtdGZTancwVkpkaXhTdU5CWVg1VGZaM3ViSnZBSExsNkxKZ3A2T2twVlNyYWdjVERIeDZCZHJhV1N2TjI2bVBTS0V0RFRoODFWTjhRRVVIUWVWdnZNY3lvNmE4NzlnQktDTG9hN3VjT0dIajAyazNBRHBsTGVZMHlqMkNMbExaYW9LQT09IiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvbW9iaWxlcGhvbmUiOiJDbFZ3ejhuNWsyZHdzTUcvTjc1L2sycUIxMDBodEswQ2xiK2RFVG5JdDVua3g2RmNsVVVEQzlKM2VNdDl2ZnlkTFQyeGZuOUxob1BZVmJIMUJjekFXRFRrN1BXOWVUWDBxbTNsTWE5L3FBakxzRlpUY1gya0tQaldudVN5OEs1bFBTcm1pcG5xN2c5NWloSjh1K21FaVdiNVc1clZjeTRRczlNV09NRDA3NXp4WGpXTlc3YVVqTkU2M0FWdnA2SStmWC93d003ZFowZlRnYVZmb3BuSkFrV0sxWVQ5L2plbjdrNENadjk2UDE4L1J5eWp2ai9wakFObXpQQXQwZks3UVZMNG1IZHYzc3M4VmJDMnEreis4UWZDSGdOV1JUeFFWZHhseSs1Q1R2QWFuV08zQUY1ZEppMjFKMnp1N2RkUzdNdStnWHNlNzNmaTJXTzllZ3RFb1E9PSIsIm5hbWVpZCI6Ik5WWTc1amhMVmZWaVZGcmNQa29maE91M0czY0t3V2tCTThVbjJHZEhSeGd4cHdwT0dORytJOHZES3BUeUxxTjNhSEZobDVjb2JhNWdXT2ZvODZEM2l1RUdydEdxQVdWOTliUHQvUGxOb2FOWVZMM1RBVGdxVFZud2xpOHBxQVR4U2s2Sks3d09LZXhYUXA5TjJsTVphaUhNMC80Y0pORmU0dmc3b3F2QVlDRzBjY2VxK1lGdUpZczFmV1M2RW5yZ29nOFZEbmNWdFQ0L2o0d21WQnNyeml3R0lJbms0WWJJamZHZjgrMGo3alFWaldtb2NueS9oalRsT3F0TXNnY3J0c1diQUZxaUdGR2tIL3hDT1FkQnFFaFZRbGtrNXV4VUZWeW91NVZYYzRDSlp4ZkdTL3VJNEpjNDJRaHU5aU1KUitheCtSMlhUeXZZcVJ6K2xIVW1kUT09IiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvYW5vbnltb3VzIjoiV0x1bXJxWnAwczVZaGROVUh1dHg5WFFTdGRub2ZUOVgwazRmemk3cEpobitmWFF2VFA4NjRKTDZvQXV5MnVESGo5VUV0a2tjU2dJYUFvTGxsV2Rta2t6ZVVWaHJjdjVoVEJNRWNqc0traTYvRXYzVWx2eHpVaUxkVTU3Nkl1aitWU0txWmtYeHJTbldNazRhWWVYclVNengvcFNXYS9LQUxVdUxiTUIvbll6NXpIUTBRM0prcnYxUm95dWdWcWVZZXhGTmJHZWFrYkFFVVhMUnVnSzFLaWxsYUVDRUd5SVJGM1ptNjc2UGZMSndpUzNwcHZtRU9oQjdzeVNkTDhmbGlUajNKWWtuVHRSVmlKRkFEdnF2VStHZk83cS9pTUNCbjRlalZBV2ZqQzBocHV5YkxpYWxQUjFiNVZnZWxidkdGSlkzS0JuRXJnQ1VmL1QyOXRCNEtBPT0iLCJuYmYiOjE1Nzk1MTk2NDgsImV4cCI6MTU4MDU5OTY0OCwiaWF0IjoxNTc5NTE5NjQ4LCJpc3MiOiJML2lpeHlqM0NlTG5tM05kcDlBeUdDVjVERTI4cGQ0Ym1CNzF0NzE2YkJ5UE9aKzMvbXJ2SVVjeGlFak1vWnErVXBncW5lWE5ZSUlibjJEbi9YV3U5TmdiSndjZDRCcDYwNU9QUTEvS0lWNUE2MjZ6OS90bkdKdHpTdC8vd2NkREROQUJNR3ViS2VBVlhVVG5RakFpVjFlbWJLb3NaUnhneFJaMTZnUUt2aTZyVHNxVXY2Z2k4R0JVMzRTZmR3NmZheFYwVVVna0pxempiam10RjhMQmpGNVZuQ2huSWw0dVNtT0NNYmtrZzJlNVlmTE1NNnZ1M1ZyY2VWS1BCbjNWM21TKzRqbGRyam10TFBhWjdDSzlORkV3eVBxcEd2TnVMamdCZmsvdm40WkwwZ1RhdHJQbUhvemY2RVpyam5tdWRoZFk1cStOQWV1ZmMzcWhpVHRRVkE9PSJ9.-LNCDzR8EY8PGmiPVoB1ESgH_je7SBzyyds2hsDuquY'
        ];
        $client = new Client(['headers' => $headers]);
        try{
            $response = $client->get($endPoint);
            return $response->getBody()->getContents();
        } catch (RequestException $e) {
            return $e->getResponse()->getBody()->getContents();
        }
    })->where('rest', '(.*)');

    Route::post('/{rest?}', function ($rest) {
        $endPoint = env('RESTCORE_API').$rest;
        $body = (request()->all());
        $headers = [
            'token' => '788b8f8c-2fcd-11ea-8ff7-005056b43abe',
            'X-Requested-With'=>'XMLHttpRequest',
            'Content-Type'=>'application/x-www-form-urlencoded',
            'JWT_Token'=>'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6Im81b1VjVUxZNDNRdms1NEVHU3dodTFGK0R5RkJFYzVLUHJrM3hyTXp1eGtkRGh1d25rdC9ncHNoeFpoUlFvV2ZzOHJVY3ZKUnZDdjE0UnN4aDBsME43N0phUit2U1NNb3dQd1hkckNaOTh2SStXN1pRSStjQkFRZC9MOHNIM2xZN1lEOEwxbGNXNjRJUVJLaFIwUGNMQW1iTnRNTnppbnY4S2w2b2p5Q2NPdFpQeVJRUXYzMmgxWkdHd2VoZVdacDM3OFJOS3ZnMCtpdCtQUy9qRGJtdGZTancwVkpkaXhTdU5CWVg1VGZaM3ViSnZBSExsNkxKZ3A2T2twVlNyYWdjVERIeDZCZHJhV1N2TjI2bVBTS0V0RFRoODFWTjhRRVVIUWVWdnZNY3lvNmE4NzlnQktDTG9hN3VjT0dIajAyazNBRHBsTGVZMHlqMkNMbExaYW9LQT09IiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvbW9iaWxlcGhvbmUiOiJDbFZ3ejhuNWsyZHdzTUcvTjc1L2sycUIxMDBodEswQ2xiK2RFVG5JdDVua3g2RmNsVVVEQzlKM2VNdDl2ZnlkTFQyeGZuOUxob1BZVmJIMUJjekFXRFRrN1BXOWVUWDBxbTNsTWE5L3FBakxzRlpUY1gya0tQaldudVN5OEs1bFBTcm1pcG5xN2c5NWloSjh1K21FaVdiNVc1clZjeTRRczlNV09NRDA3NXp4WGpXTlc3YVVqTkU2M0FWdnA2SStmWC93d003ZFowZlRnYVZmb3BuSkFrV0sxWVQ5L2plbjdrNENadjk2UDE4L1J5eWp2ai9wakFObXpQQXQwZks3UVZMNG1IZHYzc3M4VmJDMnEreis4UWZDSGdOV1JUeFFWZHhseSs1Q1R2QWFuV08zQUY1ZEppMjFKMnp1N2RkUzdNdStnWHNlNzNmaTJXTzllZ3RFb1E9PSIsIm5hbWVpZCI6Ik5WWTc1amhMVmZWaVZGcmNQa29maE91M0czY0t3V2tCTThVbjJHZEhSeGd4cHdwT0dORytJOHZES3BUeUxxTjNhSEZobDVjb2JhNWdXT2ZvODZEM2l1RUdydEdxQVdWOTliUHQvUGxOb2FOWVZMM1RBVGdxVFZud2xpOHBxQVR4U2s2Sks3d09LZXhYUXA5TjJsTVphaUhNMC80Y0pORmU0dmc3b3F2QVlDRzBjY2VxK1lGdUpZczFmV1M2RW5yZ29nOFZEbmNWdFQ0L2o0d21WQnNyeml3R0lJbms0WWJJamZHZjgrMGo3alFWaldtb2NueS9oalRsT3F0TXNnY3J0c1diQUZxaUdGR2tIL3hDT1FkQnFFaFZRbGtrNXV4VUZWeW91NVZYYzRDSlp4ZkdTL3VJNEpjNDJRaHU5aU1KUitheCtSMlhUeXZZcVJ6K2xIVW1kUT09IiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvYW5vbnltb3VzIjoiV0x1bXJxWnAwczVZaGROVUh1dHg5WFFTdGRub2ZUOVgwazRmemk3cEpobitmWFF2VFA4NjRKTDZvQXV5MnVESGo5VUV0a2tjU2dJYUFvTGxsV2Rta2t6ZVVWaHJjdjVoVEJNRWNqc0traTYvRXYzVWx2eHpVaUxkVTU3Nkl1aitWU0txWmtYeHJTbldNazRhWWVYclVNengvcFNXYS9LQUxVdUxiTUIvbll6NXpIUTBRM0prcnYxUm95dWdWcWVZZXhGTmJHZWFrYkFFVVhMUnVnSzFLaWxsYUVDRUd5SVJGM1ptNjc2UGZMSndpUzNwcHZtRU9oQjdzeVNkTDhmbGlUajNKWWtuVHRSVmlKRkFEdnF2VStHZk83cS9pTUNCbjRlalZBV2ZqQzBocHV5YkxpYWxQUjFiNVZnZWxidkdGSlkzS0JuRXJnQ1VmL1QyOXRCNEtBPT0iLCJuYmYiOjE1Nzk1MTk2NDgsImV4cCI6MTU4MDU5OTY0OCwiaWF0IjoxNTc5NTE5NjQ4LCJpc3MiOiJML2lpeHlqM0NlTG5tM05kcDlBeUdDVjVERTI4cGQ0Ym1CNzF0NzE2YkJ5UE9aKzMvbXJ2SVVjeGlFak1vWnErVXBncW5lWE5ZSUlibjJEbi9YV3U5TmdiSndjZDRCcDYwNU9QUTEvS0lWNUE2MjZ6OS90bkdKdHpTdC8vd2NkREROQUJNR3ViS2VBVlhVVG5RakFpVjFlbWJLb3NaUnhneFJaMTZnUUt2aTZyVHNxVXY2Z2k4R0JVMzRTZmR3NmZheFYwVVVna0pxempiam10RjhMQmpGNVZuQ2huSWw0dVNtT0NNYmtrZzJlNVlmTE1NNnZ1M1ZyY2VWS1BCbjNWM21TKzRqbGRyam10TFBhWjdDSzlORkV3eVBxcEd2TnVMamdCZmsvdm40WkwwZ1RhdHJQbUhvemY2RVpyam5tdWRoZFk1cStOQWV1ZmMzcWhpVHRRVkE9PSJ9.-LNCDzR8EY8PGmiPVoB1ESgH_je7SBzyyds2hsDuquY'
        ];
        $client = new Client(['headers' => $headers]);
        try{
            $response = $client->post($endPoint, array('form_params'=> $body));
            return $response->getBody()->getContents();
        } catch (RequestException $e) {
//            return $e->getMessage();
            return $e->getResponse()->getBody()->getContents();
        }
    })->where('rest', '(.*)');
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
