<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ReminderTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->actingAs(User::find(2));
        $response = $this->post('/reminder/set',[
            'date'=>'2020-06-18 12:02:52'
        ],[
            'Content-Type'=> 'multipart/form-data',
            'X-Requested-With'=> 'XMLHttpRequest',
        ]);
        dd($response->decodeResponseJson());

        $response->assertStatus(200);
    }
}
