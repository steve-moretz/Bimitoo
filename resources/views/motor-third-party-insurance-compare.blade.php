@include('layouts.header')

<!-- content session -->
<section id="motor-insurance-compare" class="compare-sec">
    <div class="compare-header">
        <span class="bread-crump"> بی می تو / بیمه شخص ثالث </span>
        <ul class="advertisment">
        </ul>
    </div>
    <div class="compare-cont" id="wizard-compare2">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-12">
                    <div class="form-box">
                        <div id="accordion">
                            <div class="card">
                                <div class="card-header first-head" >
                                    <h5 class="mb-0 card-header-box"  data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <button class="btn ">
                                            اطلاعات موتورسیکلت
                                        </button>
                                        <span class="fa fa-angle-down"></span>
                                    </h5>
                                </div>
                                <div id="collapseOne" class="collapse show"   data-parent="#accordion">
                                    <div class="card-body">
                                        <form class="form">
                                            <div class="form-group req">
                                                <label class="compare-label">  نوع موتور : </label>
                                                <span class="req-star"> * </span>
                                                <select class="compare-input form-control " id="MotorModel-select" NameInObj="MotorTypeId">
                                                    <option value="00" > انتخاب کنید  </option>
                                                </select>
                                            </div>
                                            <div class="form-group req">
                                                <label class="compare-label"> سال ساخت : </label>
                                                <span class="req-star"> * </span>
                                                <select class="compare-input form-control " id="MotorProductionYears-select"  NameInObj="ProductionYearId">
                                                    <option value="00" > انتخاب کنید  </option>
                                                </select>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header last-head" >
                                    <h5 class="mb-0 card-header-box" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseOne">
                                        <button class="btn " >
                                            اطلاعات بیمه نامه
                                        </button>
                                        <span class="fa fa-angle-down"></span>
                                    </h5>
                                </div>
                                <div id="collapseTwo" class="collapse"  data-parent="#accordion">
                                    <div class="card-body">
                                        <form class="form">
                                            <div class="form-group req">
                                                <label class="compare-label"> بیمه نامه قبلی  :  </label>
                                                <span class="req-star"> * </span>
                                                <select class="compare-input form-control "  id="MotorInsuranceStatuses-select" NameInObj="PreviousInsuranceStatusId">
                                                    <option value="00" > انتخاب کنید  </option>
                                                </select>
                                            </div>
                                            <div class="motor-condition conditional-box dn">
                                                <div class="form-group">
                                                    <label class="compare-label"> بیمه گر قبلی  :  </label>
                                                    <span class="req-star"> * </span>
                                                    <select class="compare-input form-control " id="MotorCompanies-select" NameInObj="PreviousCompanyId">
                                                        <option value="00" > انتخاب کنید  </option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="compare-label"> نوع بیمه نامه قبلی  : </label>
                                                    <span class="req-star"> * </span>
                                                    <select class="compare-input form-control " id="MotorDurations-select"  NameInObj="PreviousDurationId">
                                                        <option value="00" > انتخاب کنید  </option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="compare-label"> تاریخ انقضای بیمه نامه قبلی  : </label>
                                                    <span class="req-star"> * </span>
                                                    <input type="date" class="compare-input form-control " id="MotorEnd-date" value=" " NameInObj="PreviousExpirationDate">

                                                </div>
                                                <div class="form-group">
                                                    <label class="compare-label"> درصد تخفیف حوادث بیمه‌نامه قبلی (راننده)  : </label>
                                                    <span class="req-star"> * </span>
                                                    <select class="compare-input form-control  " id="MotorDriverDiscounts-select" NameInObj="DriverDiscountId">
                                                        <option value="00" > انتخاب کنید  </option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="compare-label"> درصد تخفیف حوادث بیمه‌نامه قبلی (شخص ثالث) : </label>
                                                    <span class="req-star"> * </span>
                                                    <select class="compare-input form-control  " id="MotorThirdPartyDiscounts-select" NameInObj="ThirdPartyDiscountId">
                                                        <option value="00" > انتخاب کنید  </option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="compare-label"> وضعیت خسارت از بیمه نامه قبلی: </label>
                                                    <span class="req-star"> * </span>
                                                    <select class="compare-input form-control  " id="MotorLossstatus-select">
                                                        <option value="0"> ندارم  </option>
                                                        <option value="1"> دارم  </option>
                                                    </select>
                                                </div>
                                                <div class="motor-condition conditional-loss dn">
                                                    <div class="form-group">
                                                        <label class="compare-label"> تعداد خسارت های مالی : </label>
                                                        <span class="req-star"> * </span>
                                                        <select class="compare-input form-control  " id="MotorPropertyLosses-select"  NameInObj="PropertyLossId">
                                                            <option value="00" > انتخاب کنید  </option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="compare-label">  تعداد خسارت های بیمه جانی : </label>
                                                        <span class="req-star"> * </span>
                                                        <select class="compare-input form-control " id="MotorLifeLosses-select" NameInObj="LifeLossId">
                                                            <option value="00" > انتخاب کنید  </option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="compare-label">  تعداد خسارت حوادث راننده  : </label>
                                                        <span class="req-star"> * </span>
                                                        <select class="compare-input form-control " id="MotorDriverLosses-select"  NameInObj="DriverLossId">
                                                            <option value="00" > انتخاب کنید  </option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="compare-label"> پوشش مالی :  </label>
                                                <select class="compare-input form-control "  id="MotorCoverageTypes-select"  NameInObj="CoverageTypeId">
                                                    <option value="00" > انتخاب کنید  </option>
                                                </select>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-12" >
                    <div class="result-list">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="loading-box dn">
        <div class="loading-sq"></div>
    </div>
</section>

@include('layouts.footer')
<script type="text/javascript">


    $(".menu-box .menu-list .menu-list-item").removeClass("active");
    $(".menu-box .menu-list #m2").addClass("active");

</script>
<script src="/js/fetchServerPure.js" ></script>
<script src="/js/motor-compare.js"></script>

</body>

</html>
