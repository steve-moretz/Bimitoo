@include('layouts.header')

<!-- content session -->
<section id="third-party-insurance-compare" class="compare-sec">
    <div class="compare-header">
        <span class="bread-crump"> بی می تو / بیمه شخص ثالث </span>
        <ul class="advertisment">
        </ul>
    </div>
    <div class="compare-cont" id="wizard-compare1">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-12">
                    <div class="form-box">
                        <div id="accordion">
                            <div class="card">
                                <div class="card-header first-head">
                                    <h5 class="mb-0 card-header-box"  data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <button class="btn ">
                                            اطلاعات خودرو
                                        </button>
                                        <span class="fa fa-angle-down"></span>
                                    </h5>
                                </div>
                                <div id="collapseOne" class="collapse show" data-parent="#accordion">
                                    <div class="card-body">
                                        <form class="form">
                                            <div class="form-group">
                                                <label class="compare-label"> نام  وسیله نقلیه : </label>
                                                <select class="compare-input form-control " id="Brands-select" >
                                                    <option value="00" > انتخاب کنید  </option>
                                                </select>

                                            </div>
                                            <div class="form-group req">
                                                <label class="compare-label"> مدل وسیله نقلیه : </label>
                                                <span class="req-star"> * </span>
                                                <select class="compare-input form-control " id="Model-select" NameInObj="ModelId">
                                                    <option value="00" > انتخاب کنید  </option>
                                                </select>
                                            </div>
                                            <div class="form-group req">
                                                <label class="compare-label">  نوع کاربری : </label>
                                                <span class="req-star"> * </span>
                                                <select class="compare-input form-control " id="UsingTypes-select" NameInObj="UsingTypeId">
                                                    <option value="00" > انتخاب کنید  </option>
                                                </select>
                                            </div>
                                            <div class="form-group req">
                                                <label class="compare-label"> سال ساخت :  </label>
                                                <span class="req-star"> * </span>
                                                <select class="compare-input form-control " id="ProductionYears-select" NameInObj="ProductionYearId">
                                                    <option value="00" > انتخاب کنید  </option>
                                                </select>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header last-head">
                                    <h5 class="mb-0 card-header-box" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseOne">
                                        <button class="btn " >
                                            اطلاعات بیمه نامه
                                        </button>
                                        <span class="fa fa-angle-down"></span>
                                    </h5>
                                </div>
                                <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                    <div class="card-body">
                                        <form class="form">
                                            <div class="form-group req">
                                                <label class="compare-label"> بیمه نامه قبلی  :  </label>
                                                <span class="req-star"> * </span>
                                                <select class="compare-input form-control "  id="InsuranceStatuses-select" NameInObj="PreviousInsuranceStatusId">
                                                    <option value="00" > انتخاب کنید  </option>
                                                </select>
                                            </div>
                                            <div class="form-group conditional-number" >
                                                <label class="compare-label">تاریخ شماره گذاری  :  </label>
                                                <span class="req-star"> * </span>
                                                <input type="date" class="compare-input form-control " id="Number-date" value=" " NameInObj="ReleaseDate">
                                            </div>
                                            <div class="conditional-box dn">
                                                <div class="form-group">
                                                    <label class="compare-label"> بیمه گر قبلی  :  </label>
                                                    <span class="req-star"> * </span>
                                                    <select class="compare-input form-control " id="Companies-select" NameInObj="PreviousCompanyId">
                                                        <option value="00" > انتخاب کنید  </option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="compare-label"> نوع بیمه نامه قبلی  : </label>
                                                    <span class="req-star"> * </span>
                                                    <select class="compare-input form-control " id="Durations-select"  NameInObj="PreviousDurationId">
                                                        <option value="00" > انتخاب کنید  </option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="compare-label"> تاریخ انقضای بیمه نامه قبلی  : </label>
                                                    <span class="req-star"> * </span>
                                                    <input type="date" class="compare-input form-control " id="End-date" value=" " NameInObj="PreviousExpirationDate">
                                                </div>
                                                <div class="form-group">
                                                    <label class="compare-label"> درصد تخفیف حوادث بیمه‌نامه قبلی (راننده)  : </label>
                                                    <span class="req-star"> * </span>
                                                    <select class="compare-input form-control  " id="DriverDiscounts-select" NameInObj="DriverDiscountId">
                                                        <option value="00" > انتخاب کنید  </option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="compare-label"> درصد تخفیف حوادث بیمه‌نامه قبلی (شخص ثالث) : </label>
                                                    <span class="req-star"> * </span>
                                                    <select class="compare-input form-control  " id="ThirdPartyDiscounts-select" NameInObj="ThirdPartyDiscountId">
                                                        <option value="00" > انتخاب کنید  </option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="compare-label"> وضعیت خسارت از بیمه نامه قبلی: </label>
                                                    <span class="req-star"> * </span>
                                                    <select class="compare-input form-control  " id="Lossstatus-select">
                                                        <option value="0"> ندارم  </option>
                                                        <option value="1"> دارم  </option>
                                                    </select>
                                                </div>
                                                <div class="conditional-loss dn">
                                                    <div class="form-group">
                                                        <label class="compare-label"> تعداد خسارت های مالی : </label>
                                                        <span class="req-star"> * </span>
                                                        <select class="compare-input form-control  " id="PropertyLosses-select"  NameInObj="PropertyLossId">
                                                            <option value="00" > انتخاب کنید  </option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="compare-label">  تعداد خسارت های بیمه جانی : </label>
                                                        <span class="req-star"> * </span>
                                                        <select class="compare-input form-control " id="LifeLosses-select" NameInObj="LifeLossId">
                                                            <option value="00" > انتخاب کنید  </option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="compare-label">  تعداد خسارت حوادث راننده  : </label>
                                                        <span class="req-star"> * </span>
                                                        <select class="compare-input form-control " id="DriverLosses-select"  NameInObj="DriverLossId">
                                                            <option value="00" > انتخاب کنید  </option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="compare-label"> پوشش مالی :  </label>
                                                <select class="compare-input form-control "  id="CoverageTypes-select"  NameInObj="CoverageTypeId">
                                                    <option value="00" > انتخاب کنید  </option>
                                                </select>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-12">
                    <div class="result-list">

                    </div>
                </div>
            </div>
    </div>
    </div>
    <div class="loading-box dn">
        <div class="loading-sq"></div>
    </div>
</section>

@include('layouts.footer')
<script type="text/javascript">
    $(".menu-box .menu-list .menu-list-item").removeClass("active");
    $(".menu-box .menu-list #m1").addClass("active");
</script>
<script src="/js/fetchServerPure.js" ></script>
<script src="/js/third-party-compare.js"></script>

</body>

</html>
