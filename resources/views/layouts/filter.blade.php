<!--  filter -->
<div class="content">
    <section>
        <div class="search-section">
            <div class="aboutus">
                <span>بی می تو </span>
                مقایسه، مشاوره و خرید آنلاین بیمه
            </div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs nav-filter" >
                <li class="nav-item active" id="p1">
                    <a class="nav-link " data-toggle="tab" href="#part1">
                        <i class="fas fa-user-shield icon fa-1x">
                        </i>
                        <span>شخص ثالث  </span>
                    </a>
                </li>
                <li class="nav-item "  id="p2">
                    <a class="nav-link" data-toggle="tab" href="#part2">
                        <i class="fas fa-motorcycle icon fa-1x">
                        </i>
                        <span> موتور سیکلت  </span>
                    </a>
                </li>
                <li class="nav-item"  id="p3">
                    <a class="nav-link" data-toggle="tab" href="#part3">
                        <i class="fas fa-car icon fa-1x">
                        </i>
                        <span>بدنه  </span>
                    </a>
                </li>
                <li class="nav-item"  id="p4">
                    <a class="nav-link" data-toggle="tab" href="#part4">
                        <i class="fas fa-suitcase-rolling icon fa-1x">
                        </i>
                        <span> مسافرتی </span>
                    </a>
                </li>
                <li class="nav-item "  id="p5">
                    <a class="nav-link" data-toggle="tab" href="#part5">
                        <i class="fas fa-suitcase-rolling icon fa-1x">
                        </i>
                        <span> مسافرتی ویژه </span>
                    </a>
                </li>
                <li class="nav-item "  id="p6">
                    <a class="nav-link" data-toggle="tab" href="#part6">
                        <i class="fas fa-clinic-medical icon fa-1x">
                        </i>
                        <span> درمان تکمیلی  </span>
                    </a>
                </li>
                <li class="nav-item"  id="p7">
                    <a class="nav-link" data-toggle="tab" href="#part7">
                        <i class="fas fa-fire icon fa-1x">
                        </i>
                        <span> زلزله و آتش سوزی  </span>
                    </a>
                </li>
                <li class="nav-item lastmenu"  id="p8">
                    <a class="nav-link" data-toggle="tab" href="#part8">
                        <i class="fas fa-mobile-alt icon fa-1x">
                        </i>
                        <span>  تجهیزات الکترونیک </span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade active" id="part1">
                    <form id="wizard1" class="wizard slider-form" >
                        <div class="wizard-content">
                            <div  class="wizard-step">
                                <div class="col-sm-10 col-xs-12 content-step">
                                    <div class="col-lg-3 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> نام  وسیله نقلیه : </label>
                                            <select class="slider-input form-control " id="Brands-select" >
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> مدل وسیله نقلیه : </label>
                                            <select class="slider-input form-control " id="Model-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label">  نوع کاربری : </label>
                                            <select class="slider-input form-control" id="UsingTypes-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> سال ساخت :  </label>
                                            <select class="slider-input form-control " id="ProductionYears-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-12 btns-step next-par ">
                                    <button type="button" class="wizard-btn wizard-next btn press-btn">
                                        <i class="fas fa-caret-left"></i>
                                        <span> بعد  </span>
                                    </button>
                                </div>
                            </div>
                            <div  class="wizard-step ">
                                <div class="col-sm-2 col-xs-12 btns-step prev-par ">
                                    <button type="button" class="wizard-btn wizard-prev btn press-btn">
                                        <i class="fas fa-caret-right"></i>
                                        <span> قبلی  </span>
                                    </button>
                                </div>
                                <div class="col-sm-8 col-xs-12 content-step">
                                    <div class="col-lg-6 col-md-12 tab-col ">
                                        <div class="form-group">
                                            <label class="slider-label"> بیمه نامه قبلی  :  </label>
                                            <select class="slider-input form-control "  id="InsuranceStatuses-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 tab-col number-condition">
                                        <div class="form-group">
                                            <label class="slider-label">تاریخ شماره گذاری  :  </label>
                                            <input type="date" class="slider-input form-control " id="Number-date" value=" ">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-12 btns-step next-par reg-par conditional-btn-reg">
                                    <button type="submit" class="wizard-finish wizard-reg btn bimi-btns press-btn">
                                        <span> مقایسه  </span>
                                    </button>
                                </div>
                            </div>
                            <div  class="wizard-step wizard-conditional dn">
                                <div class="col-sm-2 col-xs-12 btns-step prev-par ">
                                    <button type="button" class="wizard-btn wizard-prev btn press-btn ">
                                        <i class="fas fa-caret-right"></i>
                                        <span> قبلی  </span>
                                    </button>
                                </div>
                                <div class="col-sm-8 col-xs-12  content-step">
                                    <div class="col-lg-4 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> بیمه گر قبلی  :  </label>
                                            <select class="slider-input form-control " id="Companies-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> نوع بیمه نامه قبلی  : </label>
                                            <select class="slider-input form-control " id="Durations-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> تاریخ انقضای بیمه نامه قبلی  : </label>
                                            <input type="date" class="slider-input form-control " id="End-date" value=" ">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-12 btns-step next-par ">
                                    <button type="button" class="wizard-btn wizard-next btn press-btn">
                                        <i class="fas fa-caret-left"></i>
                                        <span> بعد  </span>
                                    </button>
                                </div>
                            </div>
                            <div  class="wizard-step wizard-conditional dn">
                                <div class="col-sm-2 col-xs-12 btns-step prev-par">
                                    <button type="button" class="wizard-btn wizard-prev btn press-btn ">
                                        <i class="fas fa-caret-right"></i>
                                        <span> قبلی  </span>
                                    </button>
                                </div>
                                <div class=" col-sm-8 col-xs-12 content-step">
                                    <div class="col-lg-6 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> درصد تخفیف حوادث بیمه‌نامه قبلی (راننده)  : </label>
                                            <select class="slider-input form-control  " id="DriverDiscounts-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> درصد تخفیف حوادث بیمه‌نامه قبلی (شخص ثالث) : </label>
                                            <select class="slider-input form-control  " id="ThirdPartyDiscounts-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-12 btns-step next-par">
                                    <button type="button" class="wizard-btn wizard-next btn press-btn">
                                        <i class="fas fa-caret-left"></i>
                                        <span> بعد  </span>
                                    </button>
                                </div>
                            </div>
                            <div  class="wizard-step wizard-conditional dn">
                                <div class="col-sm-2 col-xs-12 btns-step prev-par">
                                    <button type="button" class="wizard-btn wizard-prev btn press-btn ">
                                        <i class="fas fa-caret-right"></i>
                                        <span> قبلی  </span>
                                    </button>
                                </div>
                                <div class=" col-sm-8 col-xs-12 content-step">
                                    <div class="col-lg-3 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> وضعیت خسارت از بیمه نامه قبلی : </label>
                                            <select class="slider-input form-control  " id="Lossstatus-select">
                                                <option value="0"> ندارم  </option>
                                                <option value="1"> دارم  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-12 tab-col loss-condition dn">
                                        <div class="form-group">
                                            <label class="slider-label"> تعداد خسارت های مالی : </label>
                                            <select class="slider-input form-control  " id="PropertyLosses-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-12 tab-col loss-condition dn">
                                        <div class="form-group">
                                            <label class="slider-label">  تعداد خسارت های  جانی  : </label>
                                            <select class="slider-input form-control " id="LifeLosses-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-12 tab-col loss-condition dn">
                                        <div class="form-group">
                                            <label class="slider-label">  تعداد خسارت حوادث راننده  : </label>
                                            <select class="slider-input form-control " id="DriverLosses-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-12 btns-step next-par reg-par">
                                    <button type="submit" class="wizard-finish wizard-reg btn bimi-btns press-btn">
                                        <span> مقایسه  </span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="wizard-header">
                            <ul class="nav nav-tabs">
                                <li role="presentation" class="wizard-step-indicator "><a href="#!">
                                        <span class="badge slider-counter"></span>
                                    </a></li>
                                <li role="presentation" class="wizard-step-indicator "><a href="#!">
                                        <span class="badge slider-counter"></span>
                                    </a></li>
                                <li role="presentation" class="wizard-step-indicator wizard-conditional dn"><a href="#!">
                                        <span class="badge slider-counter"></span>
                                    </a></li>
                                <li role="presentation" class="wizard-step-indicator wizard-conditional dn"><a href="#!">
                                        <span class="badge slider-counter"></span>
                                    </a></li>
                                <li role="presentation" class="wizard-step-indicator wizard-conditional dn"><a href="#!">
                                        <span class="badge slider-counter"></span>
                                    </a></li>
                            </ul>
                            <div class="slider-line"></div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane fade " id="part2">

                    <form id="wizard2" class="wizard slider-form" >
                        <div class="wizard-content">
                            <div  class="wizard-step">
                                <div class="col-sm-10 col-xs-12 content-step">

                                    <div class="col-lg-6 col-sm-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> نوع موتور : </label>
                                            <select class="slider-input form-control " id="MotorModel-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> سال ساخت :  </label>
                                            <select class="slider-input form-control " id="MotorProductionYears-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-12 btns-step next-par">
                                    <button type="button" class="wizard-btn wizard-next btn press-btn">
                                        <i class="fas fa-caret-left"></i>
                                        <span> بعد  </span>
                                    </button>
                                </div>
                            </div>
                            <div  class="wizard-step ">
                                <div class="col-sm-2 col-xs-12 btns-step prev-par">
                                    <button type="button" class="wizard-btn wizard-prev btn press-btn">
                                        <i class="fas fa-caret-right"></i>
                                        <span> قبلی  </span>
                                    </button>
                                </div>
                                <div class="col-sm-8 col-xs-12 content-step">
                                    <div class="col-lg-6 col-md-12 tab-col ">
                                        <div class="form-group">
                                            <label class="slider-label"> بیمه نامه قبلی  :  </label>
                                            <select class="slider-input form-control "  id="MotorInsuranceStatuses-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-2 col-xs-12 btns-step next-par dn conditional-btn-next">
                                    <button type="button" class="wizard-btn wizard-next btn press-btn">
                                        <i class="fas fa-caret-left"></i>
                                        <span> بعد  </span>
                                    </button>
                                </div>
                                <div class="col-sm-2 col-xs-12 btns-step next-par reg-par conditional-btn-reg">
                                    <button type="submit" class="wizard-finish wizard-reg btn bimi-btns press-btn">
                                        <span> مقایسه  </span>
                                    </button>
                                </div>
                            </div>
                            <div  class="wizard-step wizard-conditional dn">
                                <div class="col-sm-2 col-xs-12 btns-step prev-par">
                                    <button type="button" class="wizard-btn wizard-prev btn press-btn ">
                                        <i class="fas fa-caret-right"></i>
                                        <span> قبلی  </span>
                                    </button>
                                </div>
                                <div class="col-sm-8 col-xs-12  content-step">
                                    <div class="col-lg-4 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> بیمه گر قبلی  :  </label>
                                            <select class="slider-input form-control " id="MotorCompanies-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> نوع بیمه نامه قبلی  : </label>
                                            <select class="slider-input form-control " id="MotorDurations-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> تاریخ انقضای بیمه نامه قبلی  : </label>
                                            <input type="date" class="slider-input form-control " id="MotorEnd-date" value=" ">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-12 btns-step next-par">
                                    <button type="button" class="wizard-btn wizard-next btn press-btn">
                                        <i class="fas fa-caret-left"></i>
                                        <span> بعد  </span>
                                    </button>
                                </div>
                            </div>
                            <div  class="wizard-step wizard-conditional dn">
                                <div class="col-sm-2 col-xs-12 btns-step prev-par">
                                    <button type="button" class="wizard-btn wizard-prev btn press-btn ">
                                        <i class="fas fa-caret-right"></i>
                                        <span> قبلی  </span>
                                    </button>
                                </div>
                                <div class=" col-sm-8 col-xs-12 content-step">
                                    <div class="col-lg-6 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> درصد تخفیف حوادث بیمه‌نامه قبلی (راننده)  : </label>
                                            <select class="slider-input form-control  " id="MotorDriverDiscounts-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> درصد تخفیف حوادث بیمه‌نامه قبلی (شخص ثالث) : </label>
                                            <select class="slider-input form-control  " id="MotorThirdPartyDiscounts-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-12 btns-step next-par">
                                    <button type="button" class="wizard-btn wizard-next btn press-btn">
                                        <i class="fas fa-caret-left"></i>
                                        <span> بعد  </span>
                                    </button>
                                </div>
                            </div>
                            <div  class="wizard-step wizard-conditional dn">
                                <div class="col-sm-2 col-xs-12 btns-step prev-par">
                                    <button type="button" class="wizard-btn wizard-prev btn press-btn ">
                                        <i class="fas fa-caret-right"></i>
                                        <span> قبلی  </span>
                                    </button>
                                </div>
                                <div class=" col-sm-8 col-xs-12 content-step">
                                    <div class="col-lg-3 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> وضعیت خسارت از بیمه نامه قبلی : </label>
                                            <select class="slider-input form-control  " id="MotorLossstatus-select">
                                                <option value="0"> ندارم  </option>
                                                <option value="1"> دارم  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-12 tab-col loss-condition dn">
                                        <div class="form-group">
                                            <label class="slider-label"> تعداد خسارت های مالی : </label>
                                            <select class="slider-input form-control  " id="MotorPropertyLosses-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-12 tab-col loss-condition dn">
                                        <div class="form-group">
                                            <label class="slider-label">  تعداد خسارت های  جانی  : </label>
                                            <select class="slider-input form-control " id="MotorLifeLosses-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-12 tab-col loss-condition dn">
                                        <div class="form-group">
                                            <label class="slider-label">  تعداد خسارت حوادث راننده  : </label>
                                            <select class="slider-input form-control " id="MotorDriverLosses-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-12 btns-step next-par reg-par">
                                    <button type="submit" class="wizard-finish wizard-reg btn bimi-btns press-btn">
                                        <span> مقایسه  </span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="wizard-header">
                            <ul class="nav nav-tabs">
                                <li role="presentation" class="wizard-step-indicator "><a href="#!">
                                        <span class="badge slider-counter"></span>
                                    </a></li>
                                <li role="presentation" class="wizard-step-indicator "><a href="#!">
                                        <span class="badge slider-counter"></span>
                                    </a></li>
                                <li role="presentation" class="wizard-step-indicator wizard-conditional dn"><a href="#!">
                                        <span class="badge slider-counter"></span>
                                    </a></li>
                                <li role="presentation" class="wizard-step-indicator wizard-conditional dn"><a href="#!">
                                        <span class="badge slider-counter"></span>
                                    </a></li>
                                <li role="presentation" class="wizard-step-indicator wizard-conditional dn"><a href="#!">
                                        <span class="badge slider-counter"></span>
                                    </a></li>
                            </ul>
                            <div class="slider-line"></div>
                        </div>
                    </form>

                </div>
                <div class="tab-pane  fade" id="part3">

                    <form id="wizard3" class="wizard slider-form" >
                        <div class="wizard-content">
                            <div  id="step1" class="wizard-step">
                                <div class="col-sm-10 col-xs-12 content-step">
                                    <div class="col-lg-3 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> نام  وسیله نقلیه : </label>
                                            <select class="slider-input form-control " id="AutoBrands-select" >
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> مدل وسیله نقلیه : </label>
                                            <select class="slider-input form-control " id="AutoModel-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label">  نوع کاربری : </label>
                                            <select class="slider-input form-control" id="AutoUsingTypes-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> سال ساخت :  </label>
                                            <select class="slider-input form-control " id="AutoProductionYears-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-12 btns-step next-par">
                                    <button type="button" class="wizard-btn wizard-next btn press-btn">
                                        <i class="fas fa-caret-left"></i>
                                        <span> بعد  </span>
                                    </button>
                                </div>
                            </div>
                            <div  id="step2" class="wizard-step ">
                                <div class="col-sm-2 col-xs-12 btns-step prev-par">
                                    <button type="button" class="wizard-btn wizard-prev btn press-btn">
                                        <i class="fas fa-caret-right"></i>
                                        <span> قبلی  </span>
                                    </button>
                                </div>
                                <div class="col-sm-8 col-xs-12 content-step">
                                    <div class="col-lg-4 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label">عمر خودرو (ماه) : </label>
                                            <input type="number" class="slider-input form-control " id="AutoLife-select" value="00">
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> ارزش خودرو : </label>
                                            <input type="number"class="slider-input form-control " id="AutoPrice-select" value="00">
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label">  وارداتی ؟ : </label>
                                            <select class="slider-input form-control" id="AutoVaredat-select">
                                                <option value="00" > انتخاب کنید  </option>
                                                <option value="false" > داخلی  </option>
                                                <option value="true" > وارداتی  </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-12 btns-step next-par">
                                    <button type="button" class="wizard-btn wizard-next btn press-btn">
                                        <i class="fas fa-caret-left"></i>
                                        <span> بعد  </span>
                                    </button>
                                </div>
                            </div>
                            <div  id="step3" class="wizard-step ">
                                <div class="col-sm-2 col-xs-12 btns-step prev-par">
                                    <button type="button" class="wizard-btn wizard-prev btn press-btn">
                                        <i class="fas fa-caret-right"></i>
                                        <span> قبلی  </span>
                                    </button>
                                </div>
                                <div class="col-sm-8 col-xs-12 content-step">
                                    <div class="col-lg-6 col-md-12 tab-col ">
                                        <div class="form-group">
                                            <label class="slider-label"> بیمه نامه قبلی  :  </label>
                                            <select class="slider-input form-control "  id="AutoInsuranceStatuses-select">
                                                <option value="0" > ندارم  </option>
                                                <option value="1" > دارم  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 tab-col AutoYear-condition dn">
                                        <div class="form-group">
                                            <label class="slider-label"> تعداد سال های تخفیف عدم خسارت بیمه بدنه  : </label>
                                            <select class="slider-input form-control " id="AutoNoDisYear-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-12 btns-step next-par reg-par">
                                    <button type="submit" class="wizard-finish wizard-reg btn bimi-btns press-btn">
                                        <span> مقایسه  </span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="wizard-header">
                            <ul class="nav nav-tabs">
                                <li role="presentation" class="wizard-step-indicator "><a href="#!">
                                        <span class="badge slider-counter">1</span>
                                    </a></li>
                                <li role="presentation" class="wizard-step-indicator "><a href="#!">
                                        <span class="badge slider-counter">2</span>
                                    </a></li>
                                <li role="presentation" class="wizard-step-indicator "><a href="#!">
                                        <span class="badge slider-counter">3</span>
                                    </a></li>
                            </ul>
                            <div class="slider-line"></div>
                        </div>
                    </form>

                </div>
                <div class="tab-pane fade " id="part4">

                    <form id="wizard4" class="wizard slider-form" >
                        <div class="wizard-content">
                            <div id="step1" class="wizard-step">
                                <div class="col-sm-10 col-xs-12  content-step">
                                    <div class="col-lg-4 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> کشور (های) مقصد :  </label>
                                            <select class="slider-input form-control" id="TravCountries-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> مدت سفر :  </label>
                                            <select class="slider-input form-control " id="TravDurations-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> تاریخ تولد :  </label>
                                            <input type="date" class="slider-input form-control " id="TravBirthDate-select" value=" ">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-12 btns-step next-par reg-par">
                                    <button type="button" class="wizard-finish wizard-reg btn bimi-btns press-btn">
                                        <span> مقایسه  </span>
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="country-listt clist">

                        </div>
                        <div class="wizard-header">
                            <div class="slider-line"></div>
                        </div>
                    </form>

                </div>
                <div class="tab-pane fade " id="part5">

                    <form id="wizard5" class="wizard slider-form" >
                        <div class="wizard-content">
                            <div id="step1" class="wizard-step">
                                <div class="col-sm-10 col-xs-12  content-step">
                                    <div class="col-lg-4 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> مقصد سفر  :  </label>
                                            <select class="slider-input form-control" id="Countries-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> مدت اقامت :  </label>
                                            <select class="slider-input form-control " id="TravelDurations-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label">  طرح ها :  </label>
                                            <select class="slider-input form-control " id="Plans-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-2 col-xs-12  btns-step next-par">
                                    <button type="button" class="wizard-btn wizard-next btn press-btn">
                                        <i class="fas fa-caret-left"></i>
                                        <span> بعد  </span>
                                    </button>
                                </div>
                            </div>
                            <div id="step2" class="wizard-step">
                                <div class="col-sm-2 col-xs-12 btns-step prev-par">
                                    <button type="button" class="wizard-btn wizard-prev btn press-btn">
                                        <i class="fas fa-caret-right"></i>
                                        <span> قبلی  </span>
                                    </button>
                                </div>
                                <div class="col-sm-8 col-xs-12 content-step">
                                    <div class="col-lg-6 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> نوع ویزا :  </label>
                                            <select class="slider-input form-control " id="VisaTypes-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> تاریخ تولد :  </label>
                                            <input type="date" class="slider-input form-control " id="BirthDate-select" value=" ">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-12 btns-step next-par reg-par">
                                    <button type="button" class="wizard-finish wizard-reg btn bimi-btns press-btn">
                                        <span> مقایسه  </span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="wizard-header">
                            <ul class="nav nav-tabs">
                                <li role="presentation" class="wizard-step-indicator"><a href="#!">
                                        <span class="badge slider-counter">1</span>
                                    </a></li>
                                <li role="presentation" class="wizard-step-indicator"><a href="#!">
                                        <span class="badge slider-counter">2</span>
                                    </a></li>
                            </ul>
                            <div class="slider-line"></div>
                        </div>
                    </form>

                </div>
                <div class="tab-pane  fade" id="part6">

                    <form id="wizard6" class="wizard slider-form" >
                        <div class="wizard-content">
                            <div  id="step1" class="wizard-step ">
                                <div class="col-sm-10 col-xs-12 content-step">
                                    <div class="col-lg-6 col-md-12 tab-col ">
                                        <div class="form-group">
                                            <label class="slider-label"> تاریخ تولد  : </label>
                                            <input type="date" class="slider-input form-control " id="HealthDate-input" value=" ">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 tab-col ">
                                        <div class="form-group">
                                            <label class="slider-label"> بیمه گر پایه  : </label>
                                            <select class="slider-input form-control " id="HealthBasicInsurers-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-12 btns-step next-par reg-par">
                                    <button type="submit" class="wizard-finish wizard-reg btn bimi-btns press-btn">
                                        <span> مقایسه  </span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="wizard-header">
                            <ul class="nav nav-tabs">
                            </ul>
                            <div class="slider-line"></div>
                        </div>
                    </form>

                </div>
                <div class="tab-pane fade " id="part7">

                    <form id="wizard7" class="wizard slider-form" >
                        <div class="wizard-content">
                            <div id="step1" class="wizard-step">
                                <div class="col-sm-10 col-xs-12  content-step">
                                    <div class="col-lg-6 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> نوع ملک  :  </label>
                                            <select class="slider-input form-control" id="EstateTypes-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-12 tab-col unit-condition dn">
                                        <div class="form-group">
                                            <label class="slider-label">  تعداد واحد : </label>
                                            <input type="number" class="slider-input form-control " id="Unit-input" value="00" >
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label">  نوع سازه  : </label>
                                            <select class="slider-input form-control " id="StructureTypes-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-12  btns-step next-par">
                                    <button type="button" class="wizard-btn wizard-next btn press-btn">
                                        <i class="fas fa-caret-left"></i>
                                        <span> بعد  </span>
                                    </button>
                                </div>
                            </div>
                            <div id="step2" class="wizard-step">
                                <div class="col-sm-2 col-xs-12 btns-step prev-par">
                                    <button type="button" class="wizard-btn wizard-prev btn press-btn">
                                        <i class="fas fa-caret-right"></i>
                                        <span> قبلی  </span>
                                    </button>
                                </div>
                                <div class="col-sm-8 col-xs-12 content-step">
                                    <div class="col-lg-4 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> قیمت هر متر مربع (تومان)  </label>
                                            <select class="slider-input form-control " id="AreaUnitPrices-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> متراژ مورد نظر (مترمربع) :  </label>
                                            <input type="number" class="slider-input form-control " id="Meter-input" value="00">
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> ارزش لوازم منزل (ریال)  : </label>
                                            <input type="text" class="slider-input form-control " id="HomeCost-input" value="00">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-12 btns-step next-par reg-par">
                                    <button type="button" class="wizard-finish wizard-reg btn bimi-btns press-btn">
                                        <span> مقایسه  </span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="wizard-header">
                            <ul class="nav nav-tabs">
                                <li role="presentation" class="wizard-step-indicator"><a href="#!">
                                        <span class="badge slider-counter">1</span>
                                    </a></li>
                                <li role="presentation" class="wizard-step-indicator"><a href="#!">
                                        <span class="badge slider-counter">2</span>
                                    </a></li>
                            </ul>
                            <div class="slider-line"></div>
                        </div>
                    </form>

                </div>
                <div class="tab-pane fade " id="part8">

                    <form id="wizard8" class="wizard slider-form" >
                        <div class="wizard-content">
                            <div id="step1" class="wizard-step">
                                <div class="col-sm-10 col-xs-12 content-step">
                                    <div class="col-lg-4 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> نوع دستگاه : </label>
                                            <select class="slider-input form-control " id="Device-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> برند : </label>
                                            <select class="slider-input form-control " id="DeviceBrand-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-12 tab-col">
                                        <div class="form-group">
                                            <label class="slider-label"> مدل : </label>
                                            <select class="slider-input form-control " id="DeviceModel-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-12 btns-step next-par">
                                    <button type="button" class="wizard-btn wizard-next btn press-btn">
                                        <i class="fas fa-caret-left"></i>
                                        <span> بعد  </span>
                                    </button>
                                </div>
                            </div>
                            <div id="step2" class="wizard-step ">
                                <div class="col-sm-2 col-xs-12 btns-step prev-par">
                                    <button type="button" class="wizard-btn wizard-prev btn press-btn">
                                        <i class="fas fa-caret-right"></i>
                                        <span> قبلی  </span>
                                    </button>
                                </div>
                                <div class="col-sm-8 col-xs-12 content-step">
                                    <div class="col-lg-6 col-md-12 tab-col ">
                                        <div class="form-group">
                                            <label class="slider-label"> فرانشیز  :  </label>
                                            <select class="slider-input form-control "  id="DeviceFran-select">
                                                <option value="00" > انتخاب کنید  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 tab-col ">
                                        <div class="form-group">
                                            <label class="slider-label"> قیمت دستگاه (ریال)  :  </label>
                                            <input type="number" class="slider-input form-control " id="DevicePrice-input" value="00">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2 col-xs-12 btns-step next-par reg-par">
                                    <button type="button" class="wizard-finish wizard-reg btn bimi-btns press-btn">
                                        <span> مقایسه  </span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="wizard-header">
                            <ul class="nav nav-tabs">
                                <li role="presentation" class="wizard-step-indicator "><a href="#!">
                                        <span class="badge slider-counter">1</span>
                                    </a></li>
                                <li role="presentation" class="wizard-step-indicator "><a href="#!">
                                        <span class="badge slider-counter">2</span>
                                    </a></li>
                            </ul>
                            <div class="slider-line"></div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </section>
</div>
