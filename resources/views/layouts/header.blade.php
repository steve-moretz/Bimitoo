<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS Ver 4.3.1 -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-rtl.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-reboot.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-grid.min.css')}}">

    <!-- Font awesome Css 5.8.2 -->
    <link rel="stylesheet" href="{{asset('css/fontawesome.min.css')}}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">


    <!--  slick slider for insurance list -->
    <link rel="stylesheet" href="{{asset('css/slick.css')}}">
    <link rel="stylesheet" href="{{asset('css/slick-theme.css')}}">

    <!--  Main Style -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/resp.css')}}">
    <!-- login Style -->
    <link rel="stylesheet" href="{{asset('css/login.css')}}">
    <!-- blog Style -->
    <link rel="stylesheet" href="{{asset('css/blog.css')}}">
    <!--job Style -->
    <link rel="stylesheet" href="{{asset('css/job.css')}}">
    <!-- insurance Style -->
    <link rel="stylesheet" href="{{asset('css/third-insurance.css')}}">

    <title> سایت بی می تو </title>

</head>
<body>

<section>
    <div class="header-box">
        <div class="header-menu fixed-menu">
            <div class="logo-box">
                <div class="brand-box">
                    <a href="/index" class="logo-img">
                        <img src="/images/bimito.png">
                    </a>
                    <a href="/index" class="logo-txt"> Bimitoo.com </a>
                </div>
                <div class="close-box">
                    <button class="btn close-btn">
                        <i class="fa fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="menu-box">
                <ul class="menu-list">
                    <li class="menu-list-item text-center active" id="m1">
                        <a class="bincleft menu-list-link" href="/third-party-insurance">
                            <i class="fas fa-user-shield fa-1x"></i>
                            بیمه شخص ثالث
                        </a>
                    </li>
                    <li class="menu-list-item text-center" id="m2">
                        <a class="bincleft menu-list-link " href="/motor-third-party-insurance">
                            <i class="fas fa-motorcycle fa-1x"></i>
                            بیمه موتورسیکلت
                        </a>
                    </li>
                    <li class="menu-list-item text-center " id="m3">
                        <a class="bincleft menu-list-link" href="/auto-body-insurance">
                            <i class="fas fa-car fa-1x"></i>
                            بیمه بدنه
                        </a>
                    </li>
                    <li class="menu-list-item text-center" id="m4">
                        <a class="bincleft menu-list-link " href="/life-insurance">
                            <i class="fas fa-fire fa-1x"></i>
                            بیمه مسافرتی
                        </a>
                    </li>
                    <li class="menu-list-item text-center" id="m5">
                        <a class="bincleft menu-list-link" href="/travel-insurance">
                            <i class="fas fa-suitcase-rolling fa-1x"></i>
                            بیمه مسافرتی ویژه
                        </a>
                    </li>
                    <li class="menu-list-item text-center" id="m6">
                        <a class="bincleft menu-list-link " href="/supplementary-health-insurance">
                            <i class="fas fa-clinic-medical fa-1x"></i>
                            بیمه تکمیلی
                        </a>
                    </li>
                    <li class="menu-list-item text-center" id="m7">
                        <a class="bincleft menu-list-link " href="/fire-insurance">
                            <i class="fas fa-fire fa-1x"></i>
                            بیمه زلزله و آتش سوزی
                        </a>
                    </li>
                    <li class="menu-list-item text-center" id="m8">
                        <a class="bincleft menu-list-link " href="/other-insurance">
                            <i class="fas fa-mobile-alt fa-1x"></i>
                            بیمه تجهیزات الکترونیک
                        </a>
                    </li>
                </ul>
            </div>
            <div class="login-btn-box">
                <div class="btn-group ">
                    @guest
                        <a href="/login" type="button" class="btn btn-login log">
                            ورود
                        </a>
                        <a href="/register" type="button" class="btn btn-login reg">
                            ثبت نام
                        </a>
                    @elseauth
                        <form action="/logout" method="post">
                            @csrf
                            <a href="/profile" type="button" class="btn btn-login reg">
                                حساب کاربری
                            </a>
                        </form>
                    @endauth
                </div>
            </div>
        </div>
        <div class="logo-boxx">
            <div class="brand-box">
                <a href="/index" class="logo-img">
                    <img src="/images/bimito.png">
                </a>
                <a href="/index" class="logo-txt"> Bimitoo.com </a>
            </div>
            <div class="hamberger-box">
                <div class="login-btn-box">
                    <div class="btn-group ">
                        @guest
                            <a href="/login" type="button" class="btn btn-login log">
                                ورود
                            </a>
                            <a href="/register" type="button" class="btn btn-login reg">
                                ثبت نام
                            </a>
                        @elseauth
                            <form action="/logout" method="post">
                                @csrf
                                <a href="/profile" type="button" class="btn btn-login reg">
                                    حساب کاربری
                                </a>
                            </form>
                        @endauth
                    </div>
                </div>
                <button class="btn hamberger-btn">
                    <i class="fa fa-bars"></i>
                </button>
            </div>
        </div>
    </div>
    <div class="header-box-dark"></div>

</section>
