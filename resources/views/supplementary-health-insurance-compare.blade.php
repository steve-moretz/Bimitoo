@include('layouts.header')


<!-- content session -->
<section id="Health-insurance-compare" class="compare-sec">
    <div class="compare-header">
        <span class="bread-crump"> بی می تو / بیمه درمان تکمیلی </span>
        <ul class="advertisment">
        </ul>
    </div>
    <div class="compare-cont" id="wizard-compare6">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-12">
                    <div class="form-box">
                        <div id="accordion">
                            <div class="card">
                                <div class="card-header first-head" >
                                    <h5 class="mb-0 card-header-box"  data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <button class="btn ">
                                            اطلاعات بیمه ای شخص اول
                                        </button>
                                        <span class="fa fa-angle-down"></span>
                                    </h5>
                                </div>
                                <div id="collapseOne" class="collapse show"  data-parent="#accordion">
                                    <div class="card-body">
                                        <form class="form">
                                            <div class="form-group req">
                                                <label class="compare-label"> تاریخ تولد  : </label>
                                                <span class="req-star"> * </span>
                                                <input type="date" class="compare-input form-control " id="HealthDate-input" value=" "  NameInObj="BirthDate">
                                            </div>
                                            <div class="form-group req">
                                                <label class="compare-label">  بیمه گر پایه :  </label>
                                                <span class="req-star"> * </span>
                                                <select class="compare-input form-control " id="HealthBasicInsurers-select" NameInObj="BasicInsuranceId">
                                                    <option value="00" > انتخاب کنید  </option>
                                                </select>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header last-head">
                                    <h5 class="mb-0 card-header-box" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseOne">
                                        <button class="btn " >
                                             افراد تحت تکفل
                                        </button>
                                        <span class="fa fa-angle-down"></span>
                                    </h5>
                                </div>
                                <div id="collapseTwo" class="collapse"  data-parent="#accordion">
                                    <div class="card-body">

                                        <div class="takafolbtn-row">
                                            <button class="btn takfoladd-btn">   اضافه کردن </button>
                                            <button class="btn takfolest-btn">  استعلام جدید </button>
                                        </div>

                                        <form class="form takafol-box">

                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-12">
                    <div class="result-list">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="loading-box dn">
        <div class="loading-sq"></div>
    </div>
</section>

@include('layouts.footer')
<script type="text/javascript">

    $(".menu-box .menu-list .menu-list-item").removeClass("active");
    $(".menu-box .menu-list #m6").addClass("active");

</script>

<script src="/js/fetchServerPure.js" ></script>
<script src="/js/health-compare.js"></script>

</body>

</html>
