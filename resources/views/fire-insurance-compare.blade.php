@include('layouts.header')

<!-- content session -->
<section id="fire-insurance-compare" class="compare-sec">
    <div class="compare-header">
        <span class="bread-crump"> بی می تو / بیمه آتش سوزی و زلزله </span>
        <ul class="advertisment">
        </ul>
    </div>
    <div class="compare-cont" id="wizard-compare7">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-12">
                    <div class="form-box">
                        <div id="accordion">
                            <div class="card">
                                <div class="card-header first-head" >
                                    <h5 class="mb-0 card-header-box"  data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <button class="btn ">
                                            اطلاعات بیمه
                                        </button>
                                        <span class="fa fa-angle-down"></span>
                                    </h5>
                                </div>
                                <div id="collapseOne" class="collapse show"  data-parent="#accordion">
                                    <div class="card-body">
                                        <form class="form">
                                            <div class="form-group req">
                                                <label class="compare-label"> نوع ملک  :  </label>
                                                <span class="req-star"> * </span>
                                                <select class="compare-input form-control" id="EstateTypes-select" NameInObj="EstateTypeId">
                                                    <option value="00" > انتخاب کنید  </option>
                                                </select>
                                            </div>
                                            <div class="unit-condition dn">
                                                <div class="form-group">
                                                    <label class="compare-label">  تعداد واحد : </label>
                                                    <span class="req-star"> * </span>
                                                    <input type="number" class="compare-input form-control " id="Unit-input" value="00" NameInObj="ApartmentUnitCount">
                                                </div>
                                            </div>
                                            <div class="form-group req">
                                                    <label class="compare-label">  نوع سازه  : </label>
                                                    <span class="req-star"> * </span>
                                                    <select class="compare-input form-control " id="StructureTypes-select" NameInObj="StructureTypeId">
                                                        <option value="00" > انتخاب کنید  </option>
                                                    </select>
                                            </div>
                                            <div class="form-group req">
                                                <label class="compare-label"> قیمت هر متر مربع (تومان)  </label>
                                                <span class="req-star"> * </span>
                                                <select class="compare-input form-control " id="AreaUnitPrices-select" NameInObj="AreaUnitPriceId">
                                                    <option value="00" > انتخاب کنید  </option>
                                                </select>
                                            </div>
                                            <div class="form-group req">
                                                    <label class="compare-label"> متراژ مورد نظر (مترمربع) :  </label>
                                                    <span class="req-star"> * </span>
                                                    <input type="number" class="compare-input form-control " id="Meter-input" value="00" NameInObj="TotalArea">
                                            </div>
                                            <div class="form-group req">
                                                    <label class="compare-label"> ارزش لوازم منزل (ریال)  : </label>
                                                    <span class="req-star"> * </span>
                                                    <input type="text" class="compare-input form-control " id="HomeCost-input" value="00" NameInObj="AppliancesValue">
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header last-head">
                                    <h5 class="mb-0 card-header-box" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseOne">
                                        <button class="btn " >
                                            پوشش های اضافی
                                        </button>
                                        <span class="fa fa-angle-down"></span>
                                    </h5>
                                </div>
                                <div id="collapseTwo" class="collapse"  data-parent="#accordion">
                                    <div class="card-body">
                                        <form class="form">
                                            <div class="form-group">
                                                <div id="Coverages-box" class="ExtraCheck-box" NameInObj="CoverageIds">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-12">
                    <div class="result-list">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="loading-box dn">
        <div class="loading-sq"></div>
    </div>
</section>

@include('layouts.footer')
<script type="text/javascript">


    $(".menu-box .menu-list .menu-list-item").removeClass("active");
    $(".menu-box .menu-list #m7").addClass("active");

</script>
<script src="/js/fetchServerPure.js" ></script>
<script src="/js/fire-compare.js"></script>

</body>

</html>
