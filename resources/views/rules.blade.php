@include('layouts.header')
<section style="margin-bottom: 30px;margin-top: 122px;" id="third-party-insurance-content">
    <div class="container">
        <div class="row ">
            <div class="col-md-12 shadow " >
                <div class="content">
                    <div class="row" style="margin-right:42%; ">
                        <div class="third-title  col-md-4">
                            <h3>
                                شرایط و قوانین
                            </h3>
                        </div>
                    </div>
                    <div class="third-content">
                        <p class="paragraph mt-md-4">
                            کاربر گرامی استفاده شما از وب سایت بی می تو بیانگر موافقت شما با شرایط و قوانین سایت خواهد بود،
                            خواهشمند است پیش از هرگونه استفاده از سایت به مطالعه دقیق این قوانین بپردازید.
                        </p>
                        <p class="paragraph">
                            شرایط و قوانین بی می تو برگرفته و با تبعیت ازقوانین جمهوری اسلامی ایران، قوانین تجارت الکترونیک،
                            قوانین حمایت از مصرف کننده و بیمه مرکزی جمهوری اسلامی ایران می باشد. بنابراین عمل به قوانین سایت
                            به عنوان یک قرارداد الزامی است.
                        </p>
                        <h3 class="title">کاربر</h3>
                        <p class="paragraph">
                            تمام اشخاص حقیقی و حقوقی که با استفاده از اطلاعات شخصی خود نسبت به ثبت سفارش یا هر گونه استفاده
                            از خدمات از کیـ اقدام میکنند کاربر سایت شناخته می شوند، و ملزم به رعایت تمام قوانین بی می تو
                            می باشند.
                        </p>
                        <h3 class="title">پروفایل</h3>
                        <p class="paragraph">
                            تمام اشخاص حقیقی و حقوقی که با استفاده از اطلاعات شخصی خود نسبت به ثبت سفارش یا هر گونه استفاده از خدمات از کیـ اقدام میکنند کاربر سایت شناخته می شوند، و ملزم به رعایت تمام قوانین بی می تو می باشند.
                        </p>
                        <h3 class="title">قوانین صدور بیمه نامه</h3>
                        <p class="paragraph">
                            در صورت بروز هر گونه قصور، تقلب و عدم همکاری در ارائه ی اصل بیمه نامه ی سال گذشته و یا عدم ارائه ی آن به این شرکت در زمان دریافت بیمه نامه جدید، ، کلیه مسئولیت حاصل از امر فوق بر عهده ی بیمه گذار بوده و وی میبایست ما به التفاوت حق بیمه ی الحاقیه جدید مربوطه را که به دلیل لغو اعمال تخفیفات عدم خسارت سال های گذشته به وجود آمده است، پرداخت نماید
                        </p>
                        <h3 class="title">حریم شخصی</h3>
                        <p class="paragraph">
                            فعالیت بی می تو مبتنی بر قوانین جمهوری اسلامی ایران و قوانین تجارت الکترونیک می باشد، و بی می تو متعهد است از حریم شخصی اطلاعات شما محافظت نماید و از جدیدترین تکنولوژی های موجود برای حفظ اطلاعات شما استفاده نماید.
                        </p>
                        <h3 class="title">حقوق مالکیت معنوی</h3>
                        <p class="paragraph">
                            تمام مطالب موجود در سایت بی می تو ، مانند لوگو، متون، آیکون ها، تصاویر، علائم تجاری، کلیه محتوای تولید شده توسط بی می تو، اسامی خدمات قابل ارائه، کلیه موارد قابل دانلود و کپی، جزئی از اموال بی می تو محسوب می&rlm; شود و حق استفاده و نشر تمامی مطالب موجود در انحصار بی می تو است و هرگونه استفاده بدون کسب مجوز کتبی، نقض حق مالکیت معنوی محسوب شده و حق پیگرد قانونی را برای بی می تو محفوظ می&rlm; دارد.
                        </p>
                        <p class="paragraph">
                            در صورت استفاده از هر یک از خدمات بی می تو ، کاربران مسئول حفظ محرمانه بودن حساب کاربری و رمز عبور خود هستند و تمامی مسئولیت فعالیت &rlm;هایی که تحت حساب کاربری و یا رمز ورود انجام می&rlm; پذیرد به عهده کاربران است.
                        </p>
                        <p class="paragraph">
                            تنها مرجع رسمی مورد تایید بی می تو برای ارتباط با کاربران، پایگاه رسمی این سایت یعنی Bimitoo.com  است. ما تنها از طریق ارسال ایمیل و یا از طریق راه &rlm; های رسمی و تایید شده در سایت، با شما تماس می &rlm;گیریم و البته شما از طریق شبکه های اجتماعی بی می تو از قبیل فیسبوک، اینستاگرام، تلگرام، آپارات، توییتر، گوگل پلاس، یوتیوب، لینکدین، لنزور و پینترست نیز می توانید از فعالیت ها و خدمات بی می تو مطلع شوید.
                        </p>
                        <h3 class="title">تماس با بی می تو</h3>
                        <p class="paragraph">
                            راه های ارتباطی رسمی بی می تو ازطریق شماره تماس، سامانه پیامکی، ایمیل و یا شبکه های اجتماعی که در قسمت تماس با بی می تو درج شده، می باشد. و بی می تو فقط از طریق این سامانه ها به ارتباط با مشتری می پردازد و هر گونه تماس از راه های دیگر سوء استفاده از نام بی می تو محسوب می شود و تحت پیگرد قانونی قرار می گیرد.
                        </p>
                        <h3 class="title">ارسال سفارش</h3>
                        <p class="paragraph">
                            روز کاری به معنی شنبه تا چهارشنبه هر هفته از ساعت ٨ تا ١٦ و پنج شنبه از ساعت ٨ تا ١٢ به استثنای تعطیلات رسمی در جمهوری اسلامی ایران می باشد کلیه سفارشات و درخواست های کاربران در طول اولین روز کاری پس از تعطیلات بررسی و در دست اقدام قرار می گیرند. کاربران می توانند در تمامی ساعات شبانه روز در هفته و به صورت ۲۴ ساعته اقدام به ارسال درخواست نمایند.
                        </p>
                        <h3 class="title">زمان صدور بیمه نامه</h3>
                        <p class="paragraph">
                            کلیه درخواست های کاربران در اولین روز کاری، بعد از ثبت درخواست مشتری در دست اقدام قرار می گیرند. با توجه به اینکه بیمه از ساعت ۲۴:۰۰ روز صدور اعتبار پیدا می کند، لازم است حداقل ۲ روز پیش از اتمام بیمه نامه خود، اقدام به سفارش بیمه نمایند. در غیر این صورت مسئولیت مشکلات احتمالی به عهده کاربر می باشد. بی می تو تمام تلاش خود را خواهد نمود تا بیمه های با محدودیت زمانی را در اولویت قرار داده و در زمان کوتاه تری پیگیری نماید.
                        </p>
                        <p class="paragraph">
                            چنانچه درخواست مشتری به هر عنوان امکان عملی شدن را نداشته باشد، بی می تو ملزم بوده در اولین روز کاری بعد از ارسال درخواست توسط کاربر او را مطلع سازد و در اولین فرصت ممکن، نسبت به عودت وجه پرداختی کاربر اقدام می نماید.
                        </p>
                        <p class="paragraph">
                            کاربر موظف است در هنگام ثبت درخواست خرید با آگاهی کامل و اطلاع از شرایط بیمه  نامه درخواستی خود اطلاعات مورد نیاز جهت خرید بیمه  نامه جدید را به دقت وارد نماید و در هنگام تحویل بیمه  نامه جدید نسبت به تحویل بیمه  نامه قبلی اقدام نماید. چنانچه اطلاعات ارسالی توسط کاربر دارای هر گونه ایرادی باشد یا در هنگام تحویل بیمه  نامه جدید، بیمه  نامه قبلی تحویل نگردد، درخواست خرید کاربر غیر معتبر بوده و بی می تو حق پیگیری جهت جبران ضرر و زیان احتمالی را برای خود محفوظ می داند. بدیهی است، در صورت نبود شرایط ذکر شده، بیمه  نامه تحویل داده نمی شود.
                        </p>
                        <p class="paragraph">
                            کاربران می توانند نام، آدرس و تلفن شخص دیگری را برای دریافت سفارش خود وارد نمایند و در این صورت تحویل گیرنده هنگام دریافت می بایست کارت شناسایی ارائه نماید.
                        </p>
                        <p class="paragraph">
                            تمامی بیمه نامه های صادر شده در بی می تو، کاملاً مطابق با درخواست مشتری صادر می شود. تمام مواردی که مشتری در هنگام خرید در سیستم های بی می تو، اعم از وبسایت و اپلیکیشن وارد می نماید، از جمله سابقه ی عدم خسارت، سطح پوشش های مالی و جانی مورد درخواست، پوشش های جانبی تکمیلی بیمه نامه و دیگر موارد انتخابی، به عنوان درخواست قطعی مشتری تلقی شده و تمام مسئولیت ناشی از انتخاب های انجام شده بر عهده ی مشتری می باشد.
                        </p>
                        <p class="paragraph">
                            کاربر موظف است جهت نهایی نمودن سفارش، مبلغ بیمه  نامه شخص ثالث را از طریق درگاه پرداخت درون سایت  www.Bimitoo.com  به صورت آنلاین پرداخت نماید. چنانچه امکان پرداخت در محل وجود داشته باشد، در هنگام ثبت درخواست به اطلاع مشتری می رسد و در این صورت نهایی نمودن درخواست منوط به پرداخت آنلاین نمی باشد.
                        </p>
                        <p class="paragraph">
                            تحویل سفارش در مکان هایی که دارای آدرس قابل استناد نمی باشد، همانند اماکن عمومی، امکان پذیر نخواهد بود. به استثناء افرادی که به صورت ثابت در آن محل مشغول فعالیت رسمی باشند. (به عنوان مثال کارکنان فرودگاه ها)
                        </p>
                        <h3 class="title">انصراف از خرید</h3>
                        <p class="paragraph">
                            کاربر می تواند در هر مرحله قبل از پرداخت مبلغ بیمه  نامه، از خرید خود انصراف دهد. چنانچه درخواست انصراف پس از پرداخت مبلغ بیمه از طرف مشتری اعلام شود و بیمه درخواستی مشتری هنوز صادر نشده باشد، بی می تو موظف است کلیه مبالغ پرداختی را به مشتری مسترد کند. در صورتی که بیمه درخواستی مشتری صادر شده باشد، پرداخت وجه منوط به قبول شرکت بیمه برای ابطال بیمه خواهد بود، در غیر این صورت استرداد وجه مقدور نمی باشد.
                        </p>
                        <h3 class="title">مدت قرارداد</h3>
                        <p class="paragraph">
                            از لحظه ورود کاربر با نام کاربری اختصاصی خود، می تواند از خدمات بی می تو استفاده نماید. ورود کاربر به معنی قبول همکاری با بی می تو می باشد. اما خروج کاربر به معنای قطع همکاری نیست، و کاربران برای پایان همکاری باید از طریق اعلام به بی می تو به همکاری خود پایان دهند.
                        </p>
                        <p class="paragraph">
                            در موارد زیر بی می تو می تواند بدون توضیح علت، در صورت تشخیص، عضویت کاربران را پایان دهد:
                            <br>
                            ۱. تخلف از قوانین جمهوری اسلامی ایران و قوانین سایت
                            <br>
                            ۲. جعل هویت اشخاص
                        </p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
@include('layouts.footer')


</body>

</html>
