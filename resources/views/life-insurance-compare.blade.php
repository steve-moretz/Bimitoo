@include('layouts.header')

<!-- content session -->
<section id="trav-insurance-compare" class="compare-sec">
    <div class="compare-header">
        <span class="bread-crump"> بی می تو / بیمه مسافرتی </span>
        <ul class="advertisment">
        </ul>
    </div>
    <div class="compare-cont" id="wizard-compare4">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-12">
                    <div class="form-box">
                        <div id="accordion">
                            <div class="card">
                                <div class="card-header first-head" >
                                    <h5 class="mb-0 card-header-box"  data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <button class="btn ">
                                            اطلاعات بیمه
                                        </button>
                                        <span class="fa fa-angle-down"></span>
                                    </h5>
                                </div>
                                <div id="collapseOne" class="collapse show"  data-parent="#accordion">
                                    <div class="card-body">
                                        <form class="form">
                                            <div class="form-group req">
                                                <label class="compare-label"> کشور (های) مقصد : </label>
                                                <span class="req-star"> * </span>
                                                <select class="compare-input form-control " id="TCountries-select" NameInObj="CountryIds">
                                                    <option value="00" > انتخاب کنید  </option>
                                                </select>
                                            </div>
                                            <div class="country-list clist">

                                            </div>
                                            <div class="form-group req">
                                                <label class="compare-label"> مدت سفر : </label>
                                                <span class="req-star"> * </span>
                                                <select class="compare-input form-control " id="TDurations-select" NameInObj="DurationId">
                                                    <option value="00" > انتخاب کنید  </option>
                                                </select>
                                            </div>
                                            <div class="form-group req">
                                                <label class="compare-label"> تاریخ تولد : </label>
                                                <span class="req-star"> * </span>
                                                <input type="date" class="compare-input form-control " id="TBirthDate-select" value=" " NameInObj="BirthDate">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-12">
                    <div class="result-list">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="loading-box dn">
        <div class="loading-sq"></div>
    </div>
</section>

@include('layouts.footer')

<script type="text/javascript">


    $(".menu-box .menu-list .menu-list-item").removeClass("active");
    $(".menu-box .menu-list #m4").addClass("active");

</script>
<script src="/js/fetchServerPure.js" ></script>
<script src="/js/trav-compare.js"></script>

</body>

</html>
