<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string|null $email
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class User extends \Eloquent {}
}

namespace App{
/**
 * App\TempModel
 *
 * @property int $id
 * @property mixed $json
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TempModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TempModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TempModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TempModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TempModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TempModel whereJson($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TempModel whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class TempModel extends \Eloquent {}
}

namespace App{
/**
 * App\Receipt
 *
 * @property int $id
 * @property int $user_id
 * @property int $price
 * @property int $duration
 * @property mixed $data
 * @property \Illuminate\Support\Carbon $expires_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Receipt newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Receipt newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Receipt query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Receipt whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Receipt whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Receipt whereDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Receipt whereExpiresAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Receipt whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Receipt wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Receipt whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Receipt whereUserId($value)
 * @mixin \Eloquent
 */
	class Receipt extends \Eloquent {}
}

